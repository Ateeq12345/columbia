-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2020 at 07:25 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_login`
--

CREATE TABLE `cms_login` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(130) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_login`
--

INSERT INTO `cms_login` (`id`, `user_id`, `email`, `password`, `user_type`, `status`, `created`) VALUES
(1, 0, 'farziali2000@gmail.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 1, 1, '2020-02-01 00:00:00'),
(2, 1, 'demo@demo.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 2, 1, '0000-00-00 00:00:00'),
(3, 1, 'some@some.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 2, 1, '0000-00-00 00:00:00'),
(4, 2, 'demosome@gmail.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 1, 1, '0000-00-00 00:00:00'),
(5, 2, 'teacher@gmail.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 2, 1, '0000-00-00 00:00:00'),
(6, 5, 'student@gmail.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 3, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_schdule`
--

CREATE TABLE `cms_schdule` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_schdule`
--

INSERT INTO `cms_schdule` (`id`, `subject_id`, `teacher_id`, `created`) VALUES
(1, 1, 1, '2020-02-01 09:57:41'),
(3, 1, 1, '2020-02-16 23:45:30'),
(4, 1, 1, '2020-02-16 23:50:30'),
(6, 1, 1, '2020-02-18 20:28:48'),
(7, 1, 1, '2020-02-18 20:29:21'),
(8, 1, 2, '2020-02-21 08:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `cms_schdule_date`
--

CREATE TABLE `cms_schdule_date` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `schdule_date_id` int(11) NOT NULL,
  `dates` varchar(20) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_schdule_date`
--

INSERT INTO `cms_schdule_date` (`id`, `subject_id`, `teacher_id`, `schdule_date_id`, `dates`, `created`) VALUES
(3, 1, 1, 1, '02/01/2020', '2020-02-01 09:57:41'),
(4, 1, 1, 1, '02/02/2020', '2020-02-01 09:57:41'),
(6, 1, 1, 3, '02/17/2020', '2020-02-16 23:45:30'),
(7, 1, 1, 4, '02/17/2020', '2020-02-16 23:50:30'),
(8, 1, 1, 5, '02/16/2020', '2020-02-16 23:57:22'),
(9, 1, 1, 6, '02/19/2020', '2020-02-18 20:28:48'),
(10, 1, 1, 7, '02/19/2020', '2020-02-18 20:29:21'),
(11, 1, 2, 8, '02/21/2020', '2020-02-21 08:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `cms_schdule_time`
--

CREATE TABLE `cms_schdule_time` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `schdule_id` int(11) NOT NULL,
  `date_id` int(11) NOT NULL,
  `schdule_date` varchar(50) NOT NULL,
  `timepickerfrom` varchar(50) NOT NULL,
  `timepickerto` varchar(50) NOT NULL,
  `noofstudent` varchar(20) NOT NULL,
  `expiry_time` time NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_schdule_time`
--

INSERT INTO `cms_schdule_time` (`id`, `subject_id`, `teacher_id`, `schdule_id`, `date_id`, `schdule_date`, `timepickerfrom`, `timepickerto`, `noofstudent`, `expiry_time`, `status`, `created`) VALUES
(3, 1, 1, 1, 3, '02/01/2020', '01:51 PM', '02:51 PM', '1', '14:51:00', 0, '2020-02-01 09:57:41'),
(4, 1, 1, 1, 4, '02/02/2020', '10:00 AM', '01:00 PM', '3', '13:00:00', 0, '2020-02-01 09:57:41'),
(7, 1, 1, 3, 6, '02/17/2020', '03:45 PM', '', '', '03:45:00', 0, '2020-02-16 23:45:30'),
(8, 1, 1, 4, 7, '02/17/2020', '03:50 PM', '', '', '03:50:00', 0, '2020-02-16 23:50:30'),
(10, 1, 1, 6, 9, '02/19/2020', '12:28 PM', '', '', '12:28:00', 0, '2020-02-18 20:28:48'),
(11, 1, 1, 7, 10, '02/19/2020', '12:29 PM', '', '', '12:29:00', 0, '2020-02-18 20:29:21'),
(12, 1, 2, 8, 11, '02/21/2020', '01:02 PM', '', '', '01:02:00', 0, '2020-02-21 08:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `cms_students`
--

CREATE TABLE `cms_students` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `fathername` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(130) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `homephone` varchar(20) NOT NULL,
  `fimg` varchar(50) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `admission` tinyint(4) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_students`
--

INSERT INTO `cms_students` (`id`, `fname`, `lname`, `fathername`, `email`, `password`, `address`, `phone`, `homephone`, `fimg`, `active`, `admission`, `created`) VALUES
(1, 'Some 1', 'Some', '', 'some@some.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 'demo', '324342', '234234', '', 1, 1, '2020-02-01 18:36:33'),
(2, 'Cool', 'Some', '', 'some@some.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 'demo', '324342', '234234', '', 1, 1, '2020-02-01 18:36:33'),
(3, 'Hand', 'Some', '', 'some@some3.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 'demo', '324342', '234234', '', 1, 1, '2020-02-01 18:36:33'),
(4, 'Dear', 'Some', '', 'some@some.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 'demo', '324342', '234234', '', 1, 1, '2020-02-01 18:36:33'),
(5, 'hello', 'some', '', 'student@gmail.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 'demo', '3423432', '234234', '', 1, 1, '2020-02-21 08:14:47');

-- --------------------------------------------------------

--
-- Table structure for table `cms_student_booking`
--

CREATE TABLE `cms_student_booking` (
  `booking_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `slotdate` varchar(20) NOT NULL,
  `slotid` int(11) NOT NULL,
  `slottype` varchar(20) NOT NULL,
  `timeslot` int(11) NOT NULL,
  `timefrom` time NOT NULL,
  `timeto` time NOT NULL,
  `slotstatus` tinyint(4) NOT NULL,
  `class_status` tinyint(4) NOT NULL,
  `class_notes` text NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `video_link` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_student_booking`
--

INSERT INTO `cms_student_booking` (`booking_id`, `student_id`, `subject_id`, `teacher_id`, `slotdate`, `slotid`, `slottype`, `timeslot`, `timefrom`, `timeto`, `slotstatus`, `class_status`, `class_notes`, `video_title`, `video_link`, `created`) VALUES
(1, 1, 1, 1, '2020-02-19', 5, 'booking', 6, '15:30:00', '01:00:00', 1, 0, '', '', '', '2020-02-16 23:31:37'),
(2, 3, 1, 1, '2020-02-01', 3, 'booking', 3, '13:51:00', '00:00:00', 1, 0, '', '', '', '2020-02-16 23:42:55'),
(3, 1, 1, 1, '2020-02-17', 6, 'booking', 7, '15:45:00', '00:00:00', 1, 0, '', '', '', '2020-02-16 23:45:38'),
(4, 3, 1, 1, '2020-02-17', 6, 'booking', 7, '15:45:00', '00:00:00', 1, 0, '', '', '', '2020-02-16 23:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `cms_subjects`
--

CREATE TABLE `cms_subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_subjects`
--

INSERT INTO `cms_subjects` (`id`, `name`, `created`) VALUES
(1, 'English', '2020-02-01 09:39:58');

-- --------------------------------------------------------

--
-- Table structure for table `cms_teachers`
--

CREATE TABLE `cms_teachers` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(130) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `fimg` varchar(50) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_teachers`
--

INSERT INTO `cms_teachers` (`id`, `fname`, `lname`, `email`, `password`, `address`, `phone`, `fimg`, `active`, `created`) VALUES
(2, 'Some ', 'teacher', 'teacher@gmail.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', 'demo', '234234', '', 1, '2020-02-21 07:57:48');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(130) DEFAULT NULL,
  `address` text,
  `phone` varchar(15) DEFAULT NULL,
  `active` enum('0','1') NOT NULL,
  `created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `profile_id`, `fname`, `lname`, `email`, `password`, `address`, `phone`, `active`, `created`) VALUES
(1, 2, 'Farzand', 'Ali', 'demo@demo.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', '', '321312', '1', '2019-12-19'),
(2, 2, 'Ali', 'Ali', 'demosome@gmail.com', 'f70e71f17885336fadc28d99d9942d6f93ccb5c029961a73178489c3f5ca795a1a006918fad3a846a00f56bd63e317a29a1922eb3c402b019dcaec9710d62380', NULL, NULL, '1', '2020-02-21');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_permission`
--

CREATE TABLE `cms_users_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `pright` varchar(200) NOT NULL,
  `pid` int(11) NOT NULL,
  `addp` enum('0','1') NOT NULL,
  `editp` enum('0','1') NOT NULL,
  `view` enum('0','1') NOT NULL,
  `deletep` enum('0','1') NOT NULL,
  `allview` enum('0','1') NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_users_permission`
--

INSERT INTO `cms_users_permission` (`id`, `name`, `pright`, `pid`, `addp`, `editp`, `view`, `deletep`, `allview`, `created`) VALUES
(3, NULL, 'profile', 2, '1', '1', '1', '1', '1', '2020-02-23'),
(4, NULL, 'adminsetting', 2, '1', '1', '1', '1', '1', '2020-02-23'),
(5, NULL, 'adminuser', 2, '1', '1', '1', '1', '1', '2020-02-23'),
(6, NULL, 'slass_schdule', 2, '1', '1', '1', '1', '1', '2020-02-23'),
(7, NULL, 'permission', 2, '1', '1', '1', '1', '1', '2020-02-23'),
(8, NULL, 'students', 2, '1', '1', '1', '1', '1', '2020-02-23'),
(9, NULL, 'subjects', 2, '1', '1', '1', '1', '1', '2020-02-23'),
(10, NULL, 'teachers', 2, '1', '1', '1', '1', '1', '2020-02-23');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_profile`
--

CREATE TABLE `cms_users_profile` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_users_profile`
--

INSERT INTO `cms_users_profile` (`id`, `name`, `created`) VALUES
(2, 'Admin', '2016-05-29');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_right`
--

CREATE TABLE `cms_users_right` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `rights` varchar(200) DEFAULT NULL,
  `created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_users_right`
--

INSERT INTO `cms_users_right` (`id`, `name`, `rights`, `created`) VALUES
(9, 'Admin User', 'adminuser', '2014-10-19'),
(10, 'Admin Profile', 'profile', '2014-10-19'),
(11, 'Admin Setting', 'adminsetting', '2014-10-19'),
(12, 'Permission', 'permission', '2014-10-19'),
(13, 'Students', 'students', '2020-02-23'),
(14, 'Teachers', 'teachers', '2020-02-23'),
(15, 'Subjects', 'subjects', '2020-02-23'),
(16, 'Class Schdule', 'slass_schdule', '2020-02-23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_login`
--
ALTER TABLE `cms_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_schdule`
--
ALTER TABLE `cms_schdule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_schdule_date`
--
ALTER TABLE `cms_schdule_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_schdule_time`
--
ALTER TABLE `cms_schdule_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_students`
--
ALTER TABLE `cms_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_student_booking`
--
ALTER TABLE `cms_student_booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `cms_subjects`
--
ALTER TABLE `cms_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_teachers`
--
ALTER TABLE `cms_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users_permission`
--
ALTER TABLE `cms_users_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users_profile`
--
ALTER TABLE `cms_users_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users_right`
--
ALTER TABLE `cms_users_right`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_login`
--
ALTER TABLE `cms_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cms_schdule`
--
ALTER TABLE `cms_schdule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cms_schdule_date`
--
ALTER TABLE `cms_schdule_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cms_schdule_time`
--
ALTER TABLE `cms_schdule_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `cms_students`
--
ALTER TABLE `cms_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cms_student_booking`
--
ALTER TABLE `cms_student_booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_subjects`
--
ALTER TABLE `cms_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_teachers`
--
ALTER TABLE `cms_teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_users_permission`
--
ALTER TABLE `cms_users_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cms_users_profile`
--
ALTER TABLE `cms_users_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_users_right`
--
ALTER TABLE `cms_users_right`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
