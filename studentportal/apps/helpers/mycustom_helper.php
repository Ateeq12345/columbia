<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function flash_message(){
	// get flash message from CI instance
	$ci =& get_instance();
	$flashmsg = $ci->session->flashdata('message');

	$html = '';
	if (is_array($flashmsg))
	{
		if($flashmsg['title'] == 'error'){
			$html = '<div id="flashmessage" role="alert" class="alert alert-error alert-danger">
						<button type="button" class="close" data-dismiss="alert"></button>
						'. $flashmsg['content'] . '
						<strong></strong>
					</div>';
		}elseif($flashmsg['title'] == 'success'){
			$html = '<div id="flashmessage" role="alert" class="alert alert-block alert-success">
							<button type="button" class="close" data-dismiss="alert"></button>
						'. $flashmsg['content'] . '
							<strong></strong>
					</div>';
		}
	}
	return $html;
}

function encode_url($string, $key="", $url_safe=TRUE)
{
    return strtr(base64_encode($string), '+/=', '._-');
}
  function decode_url($string, $key="")
{
    return base64_decode(strtr($string, '._-', '+/='));
}