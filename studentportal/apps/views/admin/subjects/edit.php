<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Edit Subject</h6>
                                    <?php print flash_message();?>
									<div class="row">
										<div class="col-sm-6">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/update';?>" id="formValidate" enctype="multipart/form-data">
									<?php 
										if($user->num_rows() > 0){
											$row = $user->row();
									?>			
													<div class="form-group">
														<label for=""> Subject Name</label>
														<input type="text" name="name" class="form-control" data-error="Please enter Subject Name." placeholder="Enter Subject Name" required="required" value="<?php print $row->name;?>"/>
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
													<input type="hidden" name="id" value="<?php print $row->id;?>"/>
										<?php } ?>		
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	