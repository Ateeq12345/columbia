<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin Students</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-6">
												<a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add Students</a>
											</div>
                                            <div class="col-sm-6">
                                                <form method="get" name="post" action="<?php print base_url().$this->uri->segment(1).'/search'?>" class="form-inline justify-content-sm-end">
                                                    <input class="form-control form-control-sm rounded bright" placeholder="Search" name="q" type="text">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                    <th>Admission</th>
                                                    <th>Created</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
												if($users->num_rows() > 0){
													foreach($users->result() as $row){
											?>    
												
												<tr>
													<td>
													<?php if(!$row->fimg == ""){?>
														<div class="user-avatar-w">
															<div class="user-avatar">
																<img alt="" src="<?php print base_url().'upload/teacher/thumb/thumb_'.$row->fimg;?>" width="50">
															</div>
														</div>
													<?php } ?>
													</td>
                                                    <td><?php print ucfirst($row->fname . ' ' . $row->lname);?></td>
                                                    <td><?php print $row->email;?></td>
                                                    <td><?php if($row->active == 1){ print 'Active'; } else { print 'Block';}?></td>
                                                    <td><?php if($row->admission == 1){ print 'Trial Class'; } else { print 'Confirm Admission';}?></td>
                                                    <td><?php print $row->created;?></td>
                                                    <td class="row-actions">
														<a data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="<?php print base_url().$this->uri->segment(1).'/edit/'.$row->id;?>"><i class="os-icon os-icon-ui-49"></i></a>
														<!--<a href="#"><i class="os-icon os-icon-grid-10"></i></a>-->
														<a data-placement="top" data-toggle="tooltip" data-original-title="Delete" class="danger" onclick="return confirm('Are your Sure to delete this?');" href="<?php print base_url().$this->uri->segment(1).'/delete/'.$row->id;?>"><i class="os-icon os-icon-ui-15"></i></a>
													</td>
                                                </tr>
                                            <?php 
													}
												}
											?>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	