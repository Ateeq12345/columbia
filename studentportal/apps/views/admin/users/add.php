<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin User</h6>
                                    <?php print flash_message();?>
									<div class="row">
										<div class="col-sm-6">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/insert';?>" id="formValidate">
													<div class="form-group">
														<label for=""> Firstname</label>
														<input type="text" name="fname" class="form-control" data-error="Please enter Firstname." placeholder="Enter Firstname" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Lastname</label>
														<input type="text" name="lname" class="form-control" data-error="Please enter Lastname." placeholder="Enter Lastname" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Email address</label>
														<input type="email" name="email" class="form-control" data-error="Your email address is invalid" placeholder="Enter email" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Password</label>
														<input type="password" name="password" class="form-control" data-minlength="6" placeholder="Password" required="required">
														<div class="help-block form-text text-muted form-control-feedback">Minimum of 6 characters</div>
													</div>
													<div class="form-group">
														<label for=""> Status</label>
														<select name="active" class="form-control">
															<option value="1">Active</option>
															<option value="0">Block</option>
														</select>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
											
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	