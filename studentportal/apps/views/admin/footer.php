					</div>
				</div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
<style type="text/css">
	.lds-roller {
	  display: inline-block;
	  position: relative;
	  width: 80px;
	  height: 80px;
	}
	.lds-roller div {
	  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
	  transform-origin: 40px 40px;
	}
	.lds-roller div:after {
	  content: " ";
	  display: block;
	  position: absolute;
	  width: 7px;
	  height: 7px;
	  border-radius: 50%;
	  background: #0a7cf8;
	  margin: -4px 0 0 -4px;
	}
	.lds-roller div:nth-child(1) {
	  animation-delay: -0.036s;
	}
	.lds-roller div:nth-child(1):after {
	  top: 63px;
	  left: 63px;
	}
	.lds-roller div:nth-child(2) {
	  animation-delay: -0.072s;
	}
	.lds-roller div:nth-child(2):after {
	  top: 68px;
	  left: 56px;
	}
	.lds-roller div:nth-child(3) {
	  animation-delay: -0.108s;
	}
	.lds-roller div:nth-child(3):after {
	  top: 71px;
	  left: 48px;
	}
	.lds-roller div:nth-child(4) {
	  animation-delay: -0.144s;
	}
	.lds-roller div:nth-child(4):after {
	  top: 72px;
	  left: 40px;
	}
	.lds-roller div:nth-child(5) {
	  animation-delay: -0.18s;
	}
	.lds-roller div:nth-child(5):after {
	  top: 71px;
	  left: 32px;
	}
	.lds-roller div:nth-child(6) {
	  animation-delay: -0.216s;
	}
	.lds-roller div:nth-child(6):after {
	  top: 68px;
	  left: 24px;
	}
	.lds-roller div:nth-child(7) {
	  animation-delay: -0.252s;
	}
	.lds-roller div:nth-child(7):after {
	  top: 63px;
	  left: 17px;
	}
	.lds-roller div:nth-child(8) {
	  animation-delay: -0.288s;
	}
	.lds-roller div:nth-child(8):after {
	  top: 56px;
	  left: 12px;
	}
	@keyframes lds-roller {
	  0% {
	    transform: rotate(0deg);
	  }
	  100% {
	    transform: rotate(360deg);
	  }
	}
#loading{
	position: absolute;
	top: 40%;
	right: 0;
	left: 0;
	bottom: 0;
	z-index: 1000;
	text-align: center;
	display: none;
}
</style>	
<div id="loading">
	<div class="lds-roller">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>
    <script src="<?php print base_url();?>admin_theme/bower_components/moment/moment.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/demo_customizerce5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/maince5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/timepicki.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/datepicker.js"></script>
  <link rel="stylesheet" href="<?php print base_url();?>admin_theme/css/easy-autocomplete.min.css">
	<script src="<?php print base_url();?>admin_theme/js/jquery.easy-autocomplete.min.js"></script>
	

    <script>
		$('.timepicker1').timepicki();
    </script>
    
	<script>
$(function(){
		
    if ($('#ckeditor1_1').length) {
        CKEDITOR.replace('ckeditor1_1');
    }
	if ($("#fullCalendar_class").length) {
	    var calendar;
	    calendar = $("#fullCalendar_class").fullCalendar({
	      header: {
	        left: "prev,next today",
	        center: "title",
	        right: "month,agendaWeek,agendaDay"
	      },
	      selectable: true,
	      selectHelper: true,
	      editable: true,
	      events: [<?php 
	      			$cal = $this->custom->getAllSlot();
	      		if($cal->num_rows() > 0){
	      			foreach($cal->result() as $row){?>{
	        title: "<?php print $this->custom->TotalcountSlot($row->timeslot);?> Student",
	        start: '<?php print $row->slotdate;?>T<?php print $row->timefrom;?>',
	        end: '<?php print $row->slotdate;?>T<?php print $row->timeto;?>'
	      },<?php 
	      			}
	      		}
	      ?>]
	    });
	  }
});
</script>

<script>

function addStudent(ids)
{
	$('#studentModal').modal('show');
	$('#class_ids').val(ids);
}

$(function(){
	// $('.basicAutoComplete').autoComplete();

	var options = {
	  url: "<?php print base_url().$this->uri->segment(1).'/searchstudent';?>",
	  getValue: "name",
	  list: {
	    onSelectItemEvent: function() {
			var value = $("#student_id").getSelectedItemData().code;
			$("#student_ids").val(value).trigger("change");
		},
		maxNumberOfElements: 1000,
	     match: {
	        enabled: true
	    }
	  },
	  theme: "square"
	};
	$("#student_id").easyAutocomplete(options);
	
	$('#addClassStudent').click(function(){
		var student_ids = $('#student_ids').val();
		var class_ids = $('#class_ids').val();
		// var schdule_date = $('#schdule_date').val();
		// var timepickerfrom = $('#timepickerfrom').val();
		$.post( '<?php print base_url();?>schdule/poststudent' , { 'student_ids': student_ids, 'class_ids': class_ids }, function(resp){
            if(resp > 0){
            	location.reload(true);
            }
		} , 'JSON');
	});


	$('#add_class').click(function(){
		$('#classModal').modal('show');
	});

	$('#newaddClass').click(function(){
		var subject_id = $('#subject_id').val();
		var teacher_id = $('#teacher_id').val();
		var schdule_date = $('#schdule_date').val();
		var timepickerfrom = $('#timepickerfrom').val();
		$.post( '<?php print base_url();?>schdule/addclass' , { 'subject_id': subject_id, 'teacher_id': teacher_id, 'schdule_date': schdule_date, 'timepickerfrom': timepickerfrom }, function(resp){
            if(resp > 0){
            	location.reload(true);
            }
		} , 'JSON');
		// $.ajax({
		// 	url: '<?php print base_url();?>schdule/addclass',
		// 	type: 'POST',
		// 	datatype: 'JSON',
		// 	data: { 'subject_id': subject_id, 'teacher_id': teacher_id, 'schdule_date': schdule_date, 'timepickerfrom': timepickerfrom },
		// 	success: function(resp){

		// 	}
		// });
	});

	$('input.datepicker').datepicker({
        autoHide: true,
        zIndex: 2048,
    });

	var count=2;
	$("#addItem").click(function(){
		var html = '<div id="div'+count+'">'+
						'<div class="row">'+
							'<div class="col-sm-3">'+
								'<div class="form-group">'+
									'<label for=""> Class Date</label>'+
									'<div class="date-input">'+
										'<input class="datepicker form-control" placeholder="Class Date" type="text" value="" name="schdule_date[]">'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-3">'+
								'<div class="form-group">'+
									'<label for=""> Class Time From</label>'+
									'<div class="date-input">'+
										'<input class="form-control timepicker1" type="text" name="timepickerfrom[]"/>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-3">'+
								'<div class="form-group">'+
									'<label for=""> Class Time To</label>'+
									'<div class="date-input">'+
										'<input class="form-control timepicker1" type="text" name="timepickerto[]"/>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-2">'+
								'<div class="form-group">'+
									'<label for=""> No of Student</label>'+
									'<div class="date-inputss">'+
										'<input type="tel" name="noofstudent[]" class="form-control" value="" />'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-1">'+
								'<label></label>'+
								'<button onclick="foo('+count+')" type="button" class="btn btn-info btn-xs">Remove</button>'+
							'</div>'+
						'</div>'+
					'</div>';
		$("#datetimerow").append(html);
		count++;
		$('.timepicker1').timepicki();
		$('input.datepicker').datepicker();
	});
});
function foo(which){
	$("#div"+which).remove();
}
function fooDiv(which){
	$("#divCanopy"+which).remove();
}
</script>	
</body>
</html>