<?php $this->load->view('admin/header');?>    

        
  <div class="row">
        <div class="col-sm-12">
            <div class="element-wrapper">
                <h6 class="element-header">Admin Permission</h6>
				<?php print flash_message();?>
				<div class="controls-above-table">
			
            <div class="row">
				<div class="col-sm-12">
				<!-- general form elements disabled -->
				  <div class="box box-warning">
					<div class="box-header with-border">
					  <h3 class="box-title">Edit Permission</h3>
					</div>
						<?php print flash_message();?>
               <form method="post" class="validate nice custom" action="<?php print base_url();?>profile/permission_update">
							<input type="hidden" name="hid" value="<?php print $this->uri->segment(3);?>"/>
							<div class="box_c_content">
								<table id="sample-table-1" class="table table-striped table-bordered table-hover">
									<thead class="block-fluid">
										<tr>
											<th width="48%">Name</th>
											<th width="10%">Add</th>
											<th width="10%">Edit</th>
											<th width="10%">View</th>
											<th width="10%">Delete</th>
											<th width="12%">View All</th>
										</tr>
									</thead>
									<tbody class="block-fluid">
				<?php 
						if($query->num_rows() > 0){
							foreach($query->result_array() as $row){ 			
								$insert = 0;
								$view = 0;
								$edit = 0;
								$delete = 0; 
								$viewall = 0; 
								if($allroles->num_rows() > 0){
									foreach($allroles->result_array() as $roles_row){ 
										foreach($roletype as $rows){
											if($rows['pright'] ==  $roles_row['rights']){
												$insert = $rows['addp'];
												$edit = $rows['editp'];
												$view = $rows['view'];
												$delete = $rows['deletep'];
												$viewall = $rows['allview'];
											}
										}
				?>	
										<tr class="row-form">
											<td><?php echo $roles_row['name'];?></td>
											<td>
												<label>
													<input type="checkbox" name="<?php echo $roles_row['rights'];?>[addp]" value="1" <?php if($insert == 1){?> checked="checked" <?php } ?> class="ace-checkbox-2"/>
													<span class="lbl"></span>
												</label>
											</td>
											<td>
												<label>
													<input type="checkbox" name="<?php echo $roles_row['rights'];?>[editp]" value="1" <?php if($edit == 1){?> checked="checked" <?php } ?> class="ace-checkbox-2"/>
													<span class="lbl"></span>
												</label>
											</td>
											<td>
												<label>
													<input type="checkbox" name="<?php echo $roles_row['rights'];?>[view]" value="1" <?php if($view == 1){?> checked="checked" <?php } ?> class="ace-checkbox-2"/>
													<span class="lbl"></span>
												</label>
											</td>
											<td>
												<label>
													<input type="checkbox" name="<?php echo $roles_row['rights'];?>[deletep]" value="1" <?php if($delete == 1){?> checked="checked" <?php } ?> class="ace-checkbox-2"/>
													<span class="lbl"></span>
												</label>
											</td>
											<td>
												<label>
													<input type="checkbox" name="<?php echo $roles_row['rights'];?>[allview]" value="1" <?php if($viewall == 1){?> checked="checked" <?php } ?> class="ace-checkbox-2"/>
													<span class="lbl"></span>
												</label>
											</td>
                        				</tr>
								
				<?php		
										$insert = 0;
										$view = 0;
										$edit = 0;
										$delete = 0; 
										$viewall = 0; 
									}
								}
							}
						}
				?>	
            					
									</tbody>
								</table>
							</div>
							
							<div class="box-footer">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>	
				</form>
					<!-- /.box-body -->
				  </div>
				  <!-- /.box -->
				
		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		
<?php $this->load->view('admin/footer');?>    