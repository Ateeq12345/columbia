<?php $this->load->view('admin/header');?>      
<?php $this->load->view('admin/nav');?>   
        
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Profile</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Profile</li>
			</ol>
		</section>
		
		<!-- Main content -->
		<section class="content">
			
            <div class="row">
				<div class="col-xs-12">
				<!-- general form elements disabled -->
				  <div class="box box-warning">
					<div class="box-header with-border">
					  <h3 class="box-title">Edit Profile</h3>
					</div>
        <form  role="form" method="post" name="postform" action="<?php print base_url().$this->uri->segment(1);?>/profile/update<?php print $this->config->item('url_suffix');?>" enctype="multipart/form-data">   
		<?php 
			if($sql->num_rows() > 0){
				$row = $sql->row();
		?>
                <input type="hidden" name="id" value="<?php print $row->id;?>"/>
					<!-- /.box-header -->
					<div class="box-body">
						<?php print flash_message();?>
						<!-- text input -->
						<div class="form-group">
						  <label>Profile Name</label>
						  <input type="text" class="form-control" name="name" value="<?php print $row->name;?>" placeholder="Profile Name ..." required>
						</div>
					</div>	
					<div class="box-footer">
						<button class="btn btn-primary" type="submit">Submit</button>
					</div>
        <?php }?>
		</form>
					<!-- /.box-body -->
				  </div>
				  <!-- /.box -->
				
		
				</div>
			</div>
				
		</section>
	
	</div>	
		
<?php $this->load->view('admin/footer');?>    