<?php $this->load->view('admin/header');?>
<style type="text/css">
    .student_block{
        width: auto;
        display: inline-block;
        /*padding: 5px 10px;*/
        background: #bfd6f9;
    }
    .student_block span{
        display: inline-block;
        padding: 5px 10px;
    }
    .student_del{
        cursor: pointer;
        background: #e65252;
        color: #FFFFFF;
        font-size: 10px;
        /*padding: 7px 10px;*/
    }
</style>			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Class Schdule</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-4">
												<div class="btn btn-sm btn-success" id="add_class">Add Class</div>
                                                <!-- <a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add Class</a> -->
											</div>
                                            <div class="col-sm-4">
                                                Today <?php print $date;?>
                                            </div>
                                            <div class="col-sm-4">
                                                <form class="form-inline justify-content-sm-end" action="<?php print base_url().$this->uri->segment(1);?>" method="get">
                                                    <input class="datepicker form-control form-control-sm rounded bright" placeholder="Search" name="d" type="text">
                                                    <button type="submit" class="btn btn-sm btn-default">Search</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
													<th>Class Time</th>
                                                    <th>Class Detail</th>
													<th>Class Student</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
												if($users->num_rows() > 0){
													foreach($users->result() as $row){
											?>    
												<tr>
													<td style="width: 120px;"><?php print $row->timepickerfrom;?></td>
                                                    <td style="width: 200px;"><?php print $this->teachers_model->getNameById($row->teacher_id);?><br/>
                                                        <b><?php print $this->subjects_model->getNameById($row->subject_id);?></b>
                                                    </td>
                                                    <td><?php 
                                                      $stu = $this->custom->getStudentCountByClass($row->date_id);
                                                      if($stu->num_rows() > 0){
                                                         foreach($stu->result() as $st){
                                                    ?>
                                                    <div class="student_block">
                                                        <span class="student_name">
                                                    <?php
                                                            print $this->students_model->getNameById($st->student_id);
                                                    ?>
                                                        </span>
                                                        <span class="student_del" onclick="delete_student(<?php print $st->booking_id;?>);">Delete</span>
                                                    </div>
                                                    <?php     
                                                         }
                                                      }  
                                                    ?></td>
                                                    <td class="row-actions" style="width: 300px;">
                                                        <div class="btn btn-sm btn-success" onclick="addStudent(<?php print $row->id;?>);">Add Student</div>
														<!-- <a href="<?php print base_url().$this->uri->segment(1).'/addstudent/'.$row->id.'?slotdate='.date('Y-m-d', strtotime($row->schdule_date)).'&slotid='.$row->date_id.'&type=booking';?>"><i class="os-icon os-icon-grid-10" title="Add Student"></i></a>
                                                        <a href="<?php print base_url().$this->uri->segment(1).'/edit/'.$row->id;?>"><i class="os-icon os-icon-ui-49"></i></a> -->
														<a class="btn btn-sm btn-danger" onclick="return confirm('Are your Sure to delete this?');" href="<?php print base_url().$this->uri->segment(1).'/delete/'.$row->id.'?date_id='.$row->date_id.'&schdule_id='.$row->schdule_id;?>"><i class="os-icon os-icon-ui-15"></i> Delete</a>
													</td>
                                                </tr>
                                            <?php 
													}
												}
											?>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>

<div class="modal" id="classModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Class</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for=""> Subject</label>
                <select name="subject_id" id="subject_id" class="form-control">
                    <?php 
                        if($subjects->num_rows() > 0){
                            foreach($subjects->result() as $srow){
                    ?>
                    <option value="<?php print $srow->id;?>"><?php print ucfirst($srow->name);?></option>
                    <?php
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for=""> Teacher</label>
                <select name="teacher_id" id="teacher_id" class="form-control">
                    <?php 
                        if($teacher->num_rows() > 0){
                            foreach($teacher->result() as $srow){
                    ?>
                    <option value="<?php print $srow->id;?>"><?php print ucfirst($srow->fname . ' ' . $srow->lname);?></option>
                    <?php
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for=""> Class Date</label>
                <div class="date-input">
                    <input id="schdule_date" class="datepicker form-control" placeholder="Class Date" type="text" value="" name="schdule_date">
                </div>
            </div>
            <div class="form-group">
                <label for=""> Class Time</label>
                <div class="date-input">
                    <input class="form-control timepicker1" type="text" id="timepickerfrom" name="timepickerfrom"/>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="newaddClass">Add</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>    

<div class="modal" id="studentModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Student</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for=""> Search Student</label>
                <input type="text" id="student_id" name="student_id" class="form-controls" style="width: 500px;" />
                <input type="hidden" id="student_ids" name="student_ids" value="">
                <input type="hidden" id="class_ids" name="class_ids" value="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addClassStudent">Add</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>    

<script type="text/javascript">
    function delete_student(ids)
    {
        $.get( "<?php print base_url().$this->uri->segment(1).'/deletestudent';?>", { student_id: ids}, function( data ) {
            location.reload(true);
        });
    }
</script>
<?php $this->load->view('admin/footer');?>