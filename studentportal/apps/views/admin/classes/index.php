<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Class Schdule</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-6">
												<a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add Schdule</a>
											</div>
                                            <div class="col-sm-6">
                                                <form class="form-inline justify-content-sm-end">
                                                    <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                    <?php if($sql->num_rows() > 0){ ?>        
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Teacher</th>
                                                    <th>Subject</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Students</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                    foreach($sql->result() as $row){
                                            ?>    
                                                <tr>
                                                    <td><?php print $this->teachers_model->getNameById($row->teacher_id);?></td>
                                                    <td><?php print $this->subjects_model->getNameById($row->subject_id);?></td>
                                                    <td><?php print $row->slotdate;?></td>
                                                    <td><?php print date('h:i A', strtotime($row->timefrom));?> / <?php print date('h:i A', strtotime($row->timeto));?></td>
                                                    <td><?php print $this->custom->getStudentCountByTeacher($row->timeslot);?></td>
                                                    <td><?php 
                                                        if ( $row->slotstatus == 1 ) {
                                                            print 'Next Session';
                                                        } elseif ( $row->slotstatus == 2 ) {
                                                            print 'session Closed';
                                                        } else {
                                                            
                                                        }
                                                    ?></td>
                                                    <td class="row-actions">
                                                        <a href="<?php print base_url().$this->uri->segment(1).'/edit/'.$row->timeslot;?>"><i class="os-icon os-icon-ui-49"></i></a>
                                                    </td>
                                                </tr>
                                            <?php 
                                                    }
                                            ?>    
                                            </tbody>
                                        </table>
                                <?php } else { ?>
                                    <div class="alert alert-danger">No result found.</div>
                                <?php } ?>
                                
                                    </div>
                                </div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	