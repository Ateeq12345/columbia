<?php $this->load->view('admin/header');?>
			<?php if($class->num_rows() > 0){ ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Classes</h6>
                                    
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Add Student</h6>
                                            <div class="element-box-tp">
                                            
                        <form method="post" name="postform" action="#">
                            <input type="hidden" name="class_id" value="<?php print $this->uri->segment(3);?>" />
                            <div class="form-group">
                                <input type="text" name="student_add" value="" />
                            </div>
                        </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="element-box">
                                                <div class="element-wrapper">
                            <h6 class="element-header">Student Attendance</h6>
                            <div class="element-box-tp">
                                <div class="table-responsive">
                        <form method="post" name="postform" action="<?php print base_url().'classes/update';?>">
                            <input type="hidden" name="class_id" value="<?php print $this->uri->segment(3);?>" />
                                    <table class="table table-padded">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Student</th>
                                                <th class="text-right">Select</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                                foreach($class->result() as $time){
                                        ?>
                                            <tr>
                                                <td class="nowrap"><span class="status-pill smaller green"></span><span>Available</span></td>
                                                <td><?php print $this->students_model->getNameById($time->student_id);?></td>
                                                <td class="text-right bolder nowrap">
                                                    <input type="checkbox" name="student[]" value="<?php print $time->student_id.'_'.$time->booking_id;?>"/>
                                                </td>
                                            </tr>
                                        <?php
                                                }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">
                                                    <button type="submit" class="btn btn-success">Start Class</button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                        </form>
                                </div>
                            </div>
                        </div>
                                        
                                            </div>

                                </div>
                            </div>
                        </div>
            <?php } ?>

            <?php if($class_document->num_rows() > 0){ ?>
                    <?php $cl = $class_document->row();?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Classes</h6>
                                    <div class="row">
                                        <div class="col-sm-7">
                            <div class="element-box">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Class Notes and Videos</h6>
                                    <div class="element-box-tp">
                        <form method="post" name="postform" action="<?php print base_url().'classes/updatenotes';?>" id="formValidate">
                            <input type="hidden" name="class_id" value="<?php print $this->uri->segment(3);?>" />
                            
                            <div class="form-group">
                                <label>Class Notes</label>
                                <textarea cols="80" id="ckeditor1_1" name="class_notes" rows="10" style="visibility: hidden;"><?php print html_entity_decode($cl->class_notes);?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Video Title</label>
                                <input type="text" name="video_title" class="form-control" value="<?php print $cl->video_title;?>" />
                            </div>
                            <div class="form-group">
                                <label>Video Link</label>
                                <textarea name="video_link" class="form-control" rows="5"><?php print html_entity_decode($cl->video_link);?></textarea>
                            </div>
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>            
                                    </div>
                                </div>
                            </div>                

                                        </div>
                                        <div class="col-sm-5">
                                            
                                    <div class="element-box">
                                                <div class="element-wrapper">
                            <h6 class="element-header">Student Attendance</h6>
                            <div class="element-box-tp">
                                <div class="table-responsive">
                                    <table class="table table-padded">
                                        <tbody>
                                        <?php 
                                                foreach($class_document->result() as $time){
                                        ?>
                                            <tr>
                                                <td class="nowrap">
                                                <?php if ( $time->class_status == 1 ) { ?>    
                                                    <span class="status-pill smaller green"></span>
                                                    <span>Available</span>
                                                <?php } else { ?>
                                                    <span class="status-pill smaller red"></span>
                                                    <span>Not Available</span>
                                                <?php } ?>
                                                </td>
                                                <td><?php print $this->students_model->getNameById($time->student_id);?></td>
                                            </tr>
                                        <?php
                                                }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                                        </div>
                                    </div>
                                        
                                            </div>

                                </div>
                            </div>
                        </div>
            
            <?php } ?>
<?php $this->load->view('admin/footer');?>	