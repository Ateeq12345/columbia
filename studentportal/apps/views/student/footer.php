					</div>
				</div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
	
    <script src="<?php print base_url();?>admin_theme/bower_components/moment/moment.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/demo_customizerce5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/maince5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/timepicki.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/datepicker.js"></script>
    <script>
		$('.timepicker1').timepicki();
    </script>
    
    <script>
$(function(){
	if ($("#booking_fullCalendar_class").length) {
	   var calendar;
	     calendar = $("#booking_fullCalendar_class").fullCalendar({
	      header: {
	        left: "prev,next today",
	        center: "title",
	        right: "month"
	      },
	      selectable: true,
	      selectHelper: true,
	      editable: true,
	      events: [<?php 
	      			$cal = $this->custom->getBookingDate();
	      		if($cal->num_rows() > 0){
	      			foreach($cal->result() as $row){?>{
	        title: "Available",
	        start: '<?php print date('Y-m-d', strtotime($row->dates));?>',
	        url : '<?php print base_url().'student/booking/timeslot?date='.date('Y-m-d', strtotime($row->dates)).'&booking='.$row->id.'&type=booking';?>'
	      },<?php 
	      			}
	      		}
	      ?>]
	    });
	 }

	if ($("#fullCalendar_class").length) {
	    var calendar;
	    calendar = $("#fullCalendar_class").fullCalendar({
	      header: {
	        left: "prev,next today",
	        center: "title",
	        right: "month,agendaWeek,agendaDay"
	      },
	      selectable: true,
	      selectHelper: true,
	      editable: false,
	      events: [<?php 
	      			$cal = $this->custom->getStudentBookingDate();
	      		if($cal->num_rows() > 0){
	      			foreach($cal->result() as $row){?>{
	        title: "Time Slot Booked",
	        start: '<?php print $row->slotdate;?>T<?php print $row->timefrom;?>',
	        end: '<?php print $row->slotdate;?>T<?php print $row->timeto;?>'
	      },<?php 
	      			}
	      		}
	      ?>]
	    });
	  }
});

</script>	
</body>
</html>