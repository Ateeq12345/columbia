<?php $this->load->view('student/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Slot Booking</h6>
									
                                    <div class="element-box">
												<div class="element-wrapper">
                            <h6 class="element-header">Select Time</h6>
                            <div class="element-box-tp">
                                <div class="table-responsive">
                        <form method="post" name="postform" action="<?php print base_url().'student/booking/slotbook';?>">
                            <input type="hidden" name="slotdate" value="<?php print $this->input->get('date');?>" />
                            <input type="hidden" name="slotid" value="<?php print $this->input->get('booking');?>" />
                            <input type="hidden" name="slottype" value="<?php print $this->input->get('type');?>" />
                                    <table class="table table-padded">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Time</th>
                                                <th class="text-center">Category</th>
                                                <th class="text-right">Select</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        	if($schdule_time->num_rows() > 0){
                                        		foreach($schdule_time->result() as $time){
                                        ?>
                                            <tr>
                                                <td class="nowrap"><span class="status-pill smaller green"></span><span>Available</span></td>
                                                <td><?php print $time->timepickerfrom .' / ' . $time->timepickerto;?></td>
                                                <td class="text-center">Book Now</a></td>
                                                <td class="text-right bolder nowrap">
                                                	<input type="checkbox" name="timeslot[]" value="<?php print $time->id;?>"/>
                                                </td>
                                            </tr>
                                        <?php
                                        		}
                                        	}
                                        ?>
                                        </tbody>
                                    	<tfoot>
                                    		<tr>
                                    			<td colspan="4">
                                    				<button type="submit" class="btn btn-success">Book Now</button>
                                    			</td>
                                    		</tr>
                                    	</tfoot>
                                    </table>

                        </form>
                                </div>
                            </div>
                        </div>
                        				
											</div>

								</div>
							</div>
						</div>
			
<?php $this->load->view('student/footer');?>	