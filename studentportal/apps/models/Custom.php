<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Custom extends CI_Model {
	
	private function filter_post($post){
		$data = trim($post);
		$data = strip_tags($post);
		$data = htmlentities($post);
		return $data;
	}
	
	public function filter_all(){
		$get  = array();
		$post = array();		
		// filter get vay
		foreach($_GET as $key => $value){
			$get[$key] = $this->filter_post($value);
		}
		// filter post var
		foreach($_POST as $key => $value){
			$post[$key] = $this->filter_post($value);	
		}
		return array('get' => $get , 'post' => $post);
	}

	
	public function logo_admin(){
		
	}
	
	public function channelUrl(){
		
	}
		
	public function user_id(){
		$session_id = $this->session->userdata('logged_in');
		return $session_id['user_id'];
	}
	
	public function user_id_ip(){
		$session_id = $this->session->userdata('logged_in');
		return $session_id['ip'];
	}
	
	public function user_red(){
		$session_id = $this->session->userdata('red_url');
		return $session_id['red_url_user'];
	}
	
	public function student_user_id(){
		$session_id = $this->session->userdata('student_logged_in');
		return $session_id['login_user_id'];
	}
	
	public function teacher_user_id(){
		$session_id = $this->session->userdata('teacher_logged_in');
		return $session_id['login_user_id'];
	}
	
	public function teacher_login_user_id(){
		$session_id = $this->session->userdata('teacher_logged_in');
		return $session_id['login_user_id'];
	}
	
	public function getAdminUser()
	{
		$user_id = $this->user_id();
		$sql = $this->db->where('id',$user_id)
						->get('users');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname; 
		}
		return false;
	}
	
	public function getUserProfile(){
		$id = $this->user_id();
		$sql = $this->db->where('id',$id)
						->limit(1)
						->get('users');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->profile_id;
		}
		return false;		
	}

	public function getStudentUser()
	{
		$user_id = $this->student_user_id();
		$sql = $this->db->where('id',$user_id)
						->get('students');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname; 
		}
		return false;
	}
	
	public function getTeacherUser()
	{
		$user_id = $this->teacher_login_user_id();
		$sql = $this->db->where('id',$user_id)
						->get('teachers');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname; 
		}
		return false;
	}

	public function upload_image($field,$path){
		$dir = chmod($path,0777);
		$config['image_library'] = 'gd2';
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		$config['maintain_ratio'] = TRUE;
		$config['encrypt_name'] = TRUE;
		$config['max_size']    = '1024000';
		$this->load->library('upload');
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($field))
		{
			$error = array('error' => $this->upload->display_errors());
		} else {
			$image_data =  $this->upload->data();
            $config['source_image'] = $image_data['full_path'];
			$config['create_thumb'] = false;
            $config['new_image'] = $path.'thumb/thumb_'.$image_data['file_name'];
            $config['width'] = 200;
            $config['height'] = 200;
            $config['maintain_ratio'] = TRUE;
			$this->load->library('image_lib');
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			return  $image_data;
		}
	}

	public function hash_password($password){
		$salt = sha1('@#$DSFT%T%YY%^U&U&@#$C');
		$pass = hash('sha512',$salt.$password);
		return $pass;	
	}
	
	public function checkloginemail($email)
	{
		$sql = $this->db->where('email',$email)
						->get('login');
		if($sql->num_rows() > 0){
			return true;
		}
		return false;
	}

	public function login($email,$password)
	{
		// $pass = $this->hash_password($password);
		// print_r($password);
		// die;
		$pass = $password;
		$sql = $this->db->where('email',$email)
						->where('password',$pass)
						->where('status',1)
						->get('login');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			if( $row->user_type == 1 ) {
				$this->session->set_userdata('logged_in', array('user_id' => $row->id,'login_user_id' => $row->user_id ,'ip'=>$this->input->ip_address(),'user_type'=>$row->user_type));
				$this->session->set_userdata('red_url', array('red_url_user' => 1));
			} elseif ( $row->user_type == 2 ) {
				$this->session->set_userdata('teacher_logged_in', array('user_id' => $row->id,'login_user_id' => $row->user_id ,'ip'=>$this->input->ip_address(),'user_type'=>$row->user_type));
				$this->session->set_userdata('red_url', array('red_url_user' => 2));
			} else {
				$this->session->set_userdata('student_logged_in', array('user_id' => $row->id,'login_user_id' => $row->user_id ,'ip'=>$this->input->ip_address(),'user_type'=>$row->user_type));
				$this->session->set_userdata('red_url', array('red_url_user' => 3));
			}
			return true;
		}
		return false;
	}


	function getDate($user_id)
	{
		$sql = $this->db->select('*')
						->from('schdule_date')
						->join('schdule_time','schdule_time.date_id=schdule_date.id')
						->where('schdule_date.teacher_id',$user_id)
						->get();
		return $sql;
	}

	function getBookingDate()
	{
		$today = date('m/d/Y');
		$sql = $this->db->select('*')
						->from('schdule_date')
						->where('dates >=',$today)
						->get();
		return $sql;
	}

	function getStudentBookingDate()
	{
		$user_id = $this->student_user_id();
		$sql = $this->db->select('*')
						->from('student_booking')
						->where('student_id',$user_id)
						->get();
		return $sql;
	}

	function countSlot($slot_id)
	{
		return $this->db->select('*')
						->from('student_booking')
						->where('timeslot',$slot_id)
						->count_all_results();
	}

	function TotalcountSlot($slot_id)
	{
		return $this->db->select('*')
						->from('student_booking')
						->where('timeslot',$slot_id)
						->count_all_results();
	}

	function getAllSlot()
	{
		$sql = $this->db->select('*')
						->from('student_booking')
						->get();
		return $sql;
	}

	function getSubjectById($slot_id)
	{
		$sql = $this->db->select('*')
						->from('schdule_date')
						->where('id',$slot_id)
						->get();
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->subject_id;
		}
		return false;
	}

	function getTeacherById($slot_id)
	{
		$sql = $this->db->select('*')
						->from('schdule_date')
						->where('id',$slot_id)
						->get();
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->teacher_id;
		}
		return false;
	}

	function getStudentCountByTeacher($timeslot)
	{
		$sql = $this->db->select('*')
							->from('student_booking')
							->where('timeslot',$timeslot)
							->count_all_results();
		return $sql;
	}

	function getStudentCountByClass($timeslot)
	{
		$sql = $this->db->select('*')
						->from('student_booking')
						// ->join('schdule_time','schdule_time.id=student_booking.timeslot')
						->where('student_booking.slotid',$timeslot)
						->get();
		return $sql;
	}

	

}