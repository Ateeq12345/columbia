<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schdule extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('subjects_model');
		$this->load->model('schdule_model');
		$this->load->model('teachers_model');
		$this->load->model('students_model');
	}
	
	public function index()
	{
		$d = isset( $_GET['d']) ? $_GET['d'] : '';
		if($d){
			$data['date'] = urldecode($d);
			$data['subjects'] = $this->subjects_model->get();
			$data['teacher'] = $this->teachers_model->get();
			$data['users'] = $this->db->where('schdule_date',$data['date'])
									  ->order_by('id','desc')
									  ->get('schdule_time');
			$this->load->view('admin/schdule/index',$data);
		} else {
			$data['date'] = date('m/d/Y');
			$data['subjects'] = $this->subjects_model->get();
			$data['teacher'] = $this->teachers_model->get();
			$data['users'] = $this->db->where('schdule_date',$data['date'])
									  ->order_by('id','desc')
									  ->get('schdule_time');
			$this->load->view('admin/schdule/index',$data);
		}
	}
	
	public function addclass()
	{
		$filter = $this->custom->filter_all();
		$subject_id = $filter['post']['subject_id'];
		$teacher_id = $filter['post']['teacher_id'];
		$schdule_date = $filter['post']['schdule_date'];
		$timepickerfrom = $filter['post']['timepickerfrom'];
		$post = array('subject_id'=>$subject_id,'teacher_id'=>$teacher_id);
		$this->schdule_model->add($post);
		$ids = $this->db->insert_id();
		
		$this->db->insert('schdule_date', array(
													'schdule_date_id'=>$ids,
													'subject_id' => $subject_id,
													'teacher_id' => $teacher_id,
													'dates'=>$schdule_date,
													'created'=>date('Y-m-d H;i:s'
												)
											));
		$date_id = $this->db->insert_id();
		
		$data = array(
						'subject_id' => $subject_id,
						'teacher_id' => $teacher_id,
						'schdule_id' => $ids,
						'date_id' => $date_id,
						'schdule_date' => $schdule_date,
						'timepickerfrom' => $timepickerfrom,
						'expiry_time' => date('h:i', strtotime($timepickerfrom)),
						'created' => date('Y-m-d H:i:s')
					 );
		$this->db->insert('schdule_time',$data);
		print $this->db->insert_id();
	}

	public function add()
	{
		$data['subjects'] = $this->subjects_model->get();
		$data['teacher'] = $this->teachers_model->get();
		$this->load->view('admin/schdule/add',$data);
	}
	
	public function insert()
	{
		$subject_id = trim(strip_tags($this->input->post('subject_id')));
		$teacher_id = trim(strip_tags($this->input->post('teacher_id')));
		$schdule_date = $this->input->post('schdule_date');
		$timepickerfrom = $this->input->post('timepickerfrom');
		$timepickerto = $this->input->post('timepickerto');
		$noofstudent = $this->input->post('noofstudent');
		$post = array('subject_id'=>$subject_id,'teacher_id'=>$teacher_id);
		$this->schdule_model->add($post);
		$ids = $this->db->insert_id();
		for($i=0;$i < count($schdule_date); $i++ ){
			if( $this->getDate($schdule_date[$i],$ids) > 0){
				$date_id = $this->getDate($schdule_date[$i],$ids);
			} else {
				$this->db->insert('schdule_date', array(
															'schdule_date_id'=>$ids,
															'subject_id' => $subject_id,
															'teacher_id' => $teacher_id,
															'dates'=>$schdule_date[$i],
															'created'=>date('Y-m-d H;i:s'
														)
													));
				$date_id = $this->db->insert_id();
			}
			$data = array(
							'subject_id' => $subject_id,
							'teacher_id' => $teacher_id,
							'schdule_id' => $ids,
							'date_id' => $date_id,
							'schdule_date' => $schdule_date[$i],
							'timepickerfrom' => $timepickerfrom[$i],
							'timepickerto' => $timepickerto[$i],
							'noofstudent' => $noofstudent[$i],
							'expiry_time' => date('h:i', strtotime($timepickerto[$i])),
							'created' => date('Y-m-d H:i:s')
						 );
			$this->db->insert('schdule_time',$data);
		}
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Schdule Successfully added.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function edit()
	{
		$data['user'] = $this->schdule_model->get(array('id'=>$this->uri->segment(3)));
		$data['subjects'] = $this->subjects_model->get();
		$data['teacher'] = $this->teachers_model->get();
		$this->load->view('admin/schdule/edit',$data);
	}
	
	public function update()
	{
		$id = trim(strip_tags($this->input->post('id')));
		$subject_id = trim(strip_tags($this->input->post('subject_id')));
		$teacher_id = trim(strip_tags($this->input->post('teacher_id')));
		$schdule_date = $this->input->post('schdule_date');
		$timepickerfrom = $this->input->post('timepickerfrom');
		$timepickerto = $this->input->post('timepickerto');
		$noofstudent = $this->input->post('noofstudent');
		$post = array('id'=>$id,'subject_id'=>$subject_id,'teacher_id'=>$teacher_id);
		$this->schdule_model->update($post);
		$ids = $id;
		$this->db->where('schdule_id',$ids)->delete('schdule_time');
		$this->db->where('subject_id',$ids)->delete('schdule_date');
		for($i=0;$i < count($schdule_date); $i++ ){
			if( $this->getDate($schdule_date[$i],$ids) > 0){
				$date_id = $this->getDate($schdule_date[$i],$ids);
			} else {
				$this->db->insert('schdule_date', array(
															'schdule_date_id'=>$ids,
															'subject_id' => $subject_id,
															'teacher_id' => $teacher_id,
															'dates'=>$schdule_date[$i],
															'created'=>date('Y-m-d H;i:s'
														)
													));
				$date_id = $this->db->insert_id();
			}
			$data = array(
							'subject_id' => $subject_id,
							'teacher_id' => $teacher_id,
							'schdule_id' => $ids,
							'date_id' => $date_id,
							'schdule_date' => $schdule_date[$i],
							'timepickerfrom' => $timepickerfrom[$i],
							'timepickerto' => $timepickerto[$i],
							'noofstudent' => $noofstudent[$i],
							'expiry_time' => date('H:i A', strtotime($timepickerto[$i])),
							'created' => date('Y-m-d H:i:s')
						 );
			$this->db->insert('schdule_time',$data);
		}
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Schdule Successfully added.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function delete()
	{
		$this->db->where('id',$this->uri->segment(3))
				 ->delete('schdule_time');
		$this->db->where('id',$this->input->get('date_id'))
				 ->delete('schdule_date');
		$this->schdule_model->delete($this->input->get('schdule_id'));
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully deleted.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	private function getDate($date,$subject_id)
	{
		$sql = $this->db->where('dates',$date)
						->where('subject_id',$subject_id)
						->get('schdule_date');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->id;
		}
		return false;
	}

	public function addstudent()
	{
		$data[] = '';
		$this->load->view('admin/schdule/addstudent',$data);
	}

	public function searchstudent()
	{
		$q = $this->input->get('term');
		$result = array();
		$sql = $this->db->like('fname',$q)
						->or_like('phone',$q)
						->where('admission',2)
						->get('students');
		if($sql->num_rows() > 0){
			foreach($sql->result() as $row){
				$result[] = array('code'=>$row->id,'name'=>$row->fname .' '. $row->lname);
			}
			print json_encode($result);
		}
	}

	public function searchstudentnew()
	{
		$q = trim(strip_tags($this->input->post('student')));
		$result = array();
		$sql = $this->db->like('fname',$q)
						->or_like('phone',$q)
						->where('admission',2)
						->get('students');
		if($sql->num_rows() > 0){
			foreach($sql->result() as $row){
				$result[] = array('value'=>$row->id,'label' => $row->fname .' '. $row->lname, 'phone'=>$row->phone);
			}
			print json_encode($result);
		}	
	}

	public function poststudent()
	{
		$filter = $this->custom->filter_all();
		$sql = $this->db->like('id',$filter['post']['class_ids'])
						->get('schdule_time');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			$data = array(
							'student_id' => $filter['post']['student_ids'],
							'slotdate' => date('Y-m-d', strtotime($row->schdule_date)),
							'slotid' => $row->date_id,
							'subject_id' => $row->subject_id,
							'teacher_id' => $row->teacher_id,
							'slottype' => 'booking',
							'timeslot' => $row->id,
							'timefrom' => date('H:i:s', strtotime($row->timepickerfrom)),
							'slotstatus' => 1,
							'created' => date('Y-m-d H:i:s')
						);
			$this->db->insert('student_booking',$data);
			print $this->db->insert_id();
		}
	}

	public function deletestudent()
	{
		$student_id = $this->input->get('student_id');	
		$this->db->where('booking_id',$student_id)
				 ->delete('student_booking');
		return true;
	}

}
