<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('users_model');
	}
	
	public function index()
	{
		$data['users'] = $this->users_model->get();
		$this->load->view('admin/users/index',$data);
	}
	
	public function add()
	{
		$this->load->view('admin/users/add');
	}
	
	public function insert()
	{
		$filter = $this->custom->filter_all();
		if( $this->custom->checkloginemail($filter['post']['email']) == true ) {
			$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Email already taken. please choose another email.', 'type' => 'message' )); 
			redirect($this->uri->segment(1).'/add');
		} else {		
			$this->users_model->add($filter['post']);
			$ids = $this->db->insert_id();
			$login = array(
							'user_id' => $ids,
							'email' => $filter['post']['email'],
							'password' => $this->custom->hash_password($filter['post']['password']),
							'user_type' => 1,
							'status' => $filter['post']['active']
						);
			$this->db->insert('login',$login);
			$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'New User Successfully added.', 'type' => 'message' )); 
			redirect($this->uri->segment(1));
		}
	}
	
	public function edit()
	{
		$data['user'] = $this->users_model->get(array('id'=>$this->uri->segment(3)));
		$this->load->view('admin/users/edit',$data);
	}
	
	public function update()
	{
		$filter = $this->custom->filter_all();
		if ( $filter['post']['password'] == "" ) {
			$data = array(
							'id' => $filter['post']['id'],
							'fname' => $filter['post']['fname'],
							'lname' => $filter['post']['lname'],
							'active' => $filter['post']['active']
						);
			$this->users_model->update($data);
			$login = array(
							'user_type' => 1,
							'status' => $filter['post']['active']
						);
			$this->db->where('user_id',$filter['post']['id'])->update('login',$login);
		} else {
			$data = array(
							'id' => $filter['post']['id'],
							'fname' => $filter['post']['fname'],
							'lname' => $filter['post']['lname'],
							'active' => $filter['post']['active'],
							'password' => $filter['post']['password']
						);
			$this->users_model->update($data);
			$login = array(
							'password' => $this->custom->hash_password($filter['post']['password']),
							'user_type' => 1,
							'status' => $filter['post']['active']
						);
			$this->db->where('user_id',$filter['post']['id'])->update('login',$login);
		}
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'User Successfully updated.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function delete()
	{
		$this->users_model->delete($this->uri->segment(3));
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'User Successfully deleted.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
}
