<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('subjects_model');
		$this->load->model('schdule_model');
		$this->load->model('teachers_model');
	}
	
	public function index()
	{
		$this->load->view('admin/booking/index');
	}
}
