<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('subjects_model');
		$this->load->model('teachers_model');
		$this->load->model('students_model');
	}
	
	public function index()
	{
		$data['sql'] = $this->db->group_by('timeslot')
								->order_by('slotstatus','asc')
								->get('student_booking');
		$this->load->view('admin/classes/index',$data);
	}

	public function edit()
	{
		$data['class_id'] = $this->uri->segment(3);
		$data['class'] = $this->db->where('timeslot',$data['class_id'])
								  ->where('slotstatus',1)
								  ->get('student_booking');
		$data['class_document'] = $this->db->where('timeslot',$data['class_id'])
								  		   ->where('slotstatus',2)
								  		   ->get('student_booking');
		$this->load->view('admin/classes/edit',$data);
	}

	public function update()
	{
		$class_id = trim(strip_tags($this->input->post('class_id')));
		$student =  $this->input->post('student');
		for($i=0; $i<count($student);$i++){
			$single = explode('_',$student[$i]);
			$this->db->where('timeslot',$class_id)
					 ->where('student_id',$single[0])
					 ->where('booking_id',$single[1])
					 ->update('student_booking', array('class_status'=>1));
		}
			$this->db->where('timeslot',$class_id)
					 ->update('student_booking', array('slotstatus'=>2));
		redirect(base_url().'classes/edit/'.$class_id);
	}

	public function updatenotes()
	{
		$class_id = trim(strip_tags($this->input->post('class_id')));
		$class_notes = trim(htmlentities($this->input->post('class_notes')));
		$video_title = trim(strip_tags($this->input->post('video_title')));
		$video_link = trim(strip_tags($this->input->post('video_link')));
		$this->db->where('timeslot',$class_id)
				 ->update('student_booking', array(
				 									'class_notes'=>$class_notes,
				 									'video_title'=>$video_title,
				 									'video_link'=>$video_link
				 								)
				);
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Class Successfully Updated.', 'type' => 'message' )); 
		redirect(base_url().'classes');
	}

	
}
