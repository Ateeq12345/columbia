<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('email');
		$this->load->model('users_model');
	}
	
	public function index()
	{
		$this->load->view('admin/login');
	}
	
	public function auth()
	{
		$filter = $this->custom->filter_all();
		if ( $filter['post']['user_email'] == "" || $filter['post']['user_password'] == "" ) {
				$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Please fill all fields.', 'type' => 'message' )); 
				redirect('login');
		} elseif ( !valid_email($filter['post']['user_email']) ) {
				$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Please enter a valid email.', 'type' => 'message' )); 
				redirect('login');
		} else {
			$login = $this->custom->login($filter['post']['user_email'],$filter['post']['user_password']);
			if($login > 0){
				switch ($this->custom->user_red()) {
					case 1:
							redirect(base_url());
						break;
					case 2:
							redirect(base_url().'teacher');
						break;
					default:
							redirect(base_url().'student');
						break;
				}
			} else {
				$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Invalid Username / Password.', 'type' => 'message' )); 
				redirect('login');
			}
		}
	}
	
}
