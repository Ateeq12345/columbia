<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('teachers_model');
	}
	
	public function index()
	{
		$data['users'] = $this->teachers_model->get();
		$this->load->view('admin/teachers/index',$data);
	}
	
	public function search()
	{
		$data['users'] = $this->teachers_model->getlike(array('fname'=>$this->input->get('q')));
		$this->load->view('admin/teachers/index',$data);
	}
	
	public function add()
	{
		$this->load->view('admin/teachers/add');
	}
	
	public function insert()
	{
		$filter = $this->custom->filter_all();
		if( $this->custom->checkloginemail($filter['post']['email']) == true ) {
			$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Email already taken. please choose another email.', 'type' => 'message' )); 
			redirect($this->uri->segment(1).'/add');
		} else {
			$this->teachers_model->add($filter['post']);
			$ids = $this->db->insert_id();
			$login = array(
							'user_id' => $ids,
							'email' => $filter['post']['email'],
							'password' => $this->custom->hash_password($filter['post']['password']),
							'user_type' => 2,
							'status' => $filter['post']['active']
						);
			$this->db->insert('login',$login);
			if($_FILES['fimg']['size'] != 0){	
				$path = 'upload';
				if(is_dir($path)){
					
				} else {
					mkdir($path,0755);
					mkdir($path.'/teacher',0755);
					mkdir($path.'/teacher/thumb',0755);
				}
				$pathdir = './upload/teacher/'; 
				$file_type = $_FILES['fimg']['type'];
				$allowed = array("image/png", "image/gif" ,"image/jpg" ,"image/jpeg");
				if(!in_array($file_type, $allowed)) {
					$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Only gif | jpg | png | jpeg Files Allow.', 'type' => 'message' )); 
					redirect($this->uri->segment(1).'/'.$this->uri->segment(2).'/add');
				}else{					
					$image_data = $this->custom->upload_image('fimg',$pathdir);
					$upload = $image_data['file_name'];
					$this->teachers_model->update(array('id'=>$ids,'fimg'=>$upload));
				}
			}
			$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'New User Successfully added.', 'type' => 'message' )); 
			redirect($this->uri->segment(1));
		}
	}
	
	public function edit()
	{
		$data['user'] = $this->teachers_model->get(array('id'=>$this->uri->segment(3)));
		$this->load->view('admin/teachers/edit',$data);
	}
	
	public function update()
	{
		$filter = $this->custom->filter_all();
		if ( $filter['post']['password'] == "" ) {
			$data = array(
							'id' => $filter['post']['id'],
							'fname' => $filter['post']['fname'],
							'lname' => $filter['post']['lname'],
							'active' => $filter['post']['active']
						);
			$this->teachers_model->update($data);
			$login = array(
							'user_type' => 2,
							'status' => $filter['post']['active']
						);
			$this->db->where('user_id',$filter['post']['id'])->update('login',$login);
		} else {
			$data = array(
							'id' => $filter['post']['id'],
							'fname' => $filter['post']['fname'],
							'lname' => $filter['post']['lname'],
							'active' => $filter['post']['active'],
							'password' => $filter['post']['password']
						);
			$this->teachers_model->update($data);
			$login = array(
							'password' => $this->custom->hash_password($filter['post']['password']),
							'user_type' => 2,
							'status' => $filter['post']['active']
						);
			$this->db->where('user_id',$filter['post']['id'])->update('login',$login);
		}
		if($_FILES['fimg']['size'] != 0){	
			$path = 'upload';
			if(is_dir($path)){
				
			} else {
				mkdir($path,0755);
				mkdir($path.'/teacher',0755);
				mkdir($path.'/teacher/thumb',0755);
			}
			$pathdir = './upload/teacher/'; 
			$file_type = $_FILES['fimg']['type'];
			$allowed = array("image/png", "image/gif" ,"image/jpg" ,"image/jpeg");
			if(!in_array($file_type, $allowed)) {
				$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Only gif | jpg | png | jpeg Files Allow.', 'type' => 'message' )); 
				redirect($this->uri->segment(1).'/edit/'.$filter['post']['id']);
			}else{						
				$image_data = $this->custom->upload_image('fimg',$pathdir);
				$upload = $image_data['file_name'];
				$this->teachers_model->update(array('id'=>$filter['post']['id'],'fimg'=>$upload));
			}
		}
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'User Successfully updated.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function delete()
	{
		$this->teachers_model->delete($this->uri->segment(3));
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'User Successfully deleted.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
}
