<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('subjects_model');
	}
	
	public function index()
	{
		$data['users'] = $this->subjects_model->get();
		$this->load->view('admin/subjects/index',$data);
	}
	
	public function add()
	{
		$this->load->view('admin/subjects/add');
	}
	
	public function insert()
	{
		$filter = $this->custom->filter_all();
		$this->subjects_model->add($filter['post']);
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully added.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function edit()
	{
		$data['user'] = $this->subjects_model->get(array('id'=>$this->uri->segment(3)));
		$this->load->view('admin/subjects/edit',$data);
	}
	
	public function update()
	{
		$filter = $this->custom->filter_all();
		$data = array(
						'id' => $filter['post']['id'],
						'name' => $filter['post']['name']
					);
		$this->subjects_model->update($data);
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully updated.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function delete()
	{
		$this->subjects_model->delete($this->uri->segment(3));
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully deleted.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
}
