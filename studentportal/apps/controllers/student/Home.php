<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->student_user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('schdule_model');
	}
	
	public function index()
	{
		$data['calendar'] = $this->db->limit(31)
									 ->order_by('id','desc')
									 ->get('schdule_date');
		$this->load->view('student/index',$data);
	}
}
