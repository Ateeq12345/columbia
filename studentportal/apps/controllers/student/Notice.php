<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notice extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->student_user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('schdule_model');
		$this->load->model('subjects_model');
	}
	
	public function index()
	{
		$data['student_id'] = $this->custom->student_user_id();
		$data['sql'] = $this->db->where('student_id',$data['student_id'])
							    ->order_by('booking_id','desc')
							    ->get('student_booking');
		$this->load->view('student/notice/index',$data);
	}

	public function edit()
	{
		$data['student_id'] = $this->custom->student_user_id();
		$data['class_id'] = $this->uri->segment(4);
		$data['class_document'] = $this->db->where('timeslot',$data['class_id'])
								  		   ->where('student_id',$data['student_id'])
										   ->where('slotstatus',2)
								  		   ->get('student_booking');
		$this->load->view('student/notice/edit',$data);
	}


}
