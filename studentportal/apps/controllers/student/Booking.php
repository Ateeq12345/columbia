<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->student_user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('schdule_model');
	}
	
	public function index()
	{
		$data['calendar'] = $this->db->limit(7)
									 ->get('schdule_time');
		$this->load->view('student/booking/index',$data);
	}

	public function timeslot()
	{
		$slotdate = $this->input->get('booking');
		$data['schdule_time'] = $this->db->where('date_id',$slotdate)
						   				 ->get('schdule_time');
		$this->load->view('student/booking/timeslot',$data);
	}

	public function slotbook()
	{
		$student_id = $this->custom->student_user_id();
		$timeslot = $this->input->post('timeslot');
		$slottype = trim(strip_tags($this->input->post('slottype')));
		$slotid = trim(strip_tags($this->input->post('slotid')));
		$slotdate = trim(strip_tags($this->input->post('slotdate')));
		for($i=0;$i < count($timeslot); $i++ ){
			$data = array(
							'student_id' => $student_id,
							'slotdate' => $slotdate,
							'slotid' => $slotid,
							'subject_id' => $this->custom->getSubjectById($slotid),
							'teacher_id' => $this->custom->getTeacherById($slotid),
							'slottype' => $slottype,
							'timeslot' => $timeslot[$i],
							'timefrom' => $this->getFromtime($timeslot[$i],$slotid),
							'timeto' => $this->getTotime($timeslot[$i],$slotid),
							'slotstatus' => 1,
							'created' => date('Y-m-d H:i:s')
						);
			$this->db->insert('student_booking',$data);
		}
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Timeslot successfully booked.', 'type' => 'message' )); 
		redirect(base_url().'student');
	}

	private function getFromtime($id,$date_id)
	{
		$sql = $this->db->select('*')
						->from('schdule_time')
						->where('id',$id)
						->where('date_id',$date_id)
						->limit(1)
						->get();
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return date('H:i:s', strtotime($row->timepickerfrom));
		}
		return false;
	}

	private function getTotime($id,$date_id)
	{
		$sql = $this->db->select('*')
						->from('schdule_time')
						->where('id',$id)
						->where('date_id',$date_id)
						->limit(1)
						->get();
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return date('H:i:s', strtotime($row->timepickerto));
		}
		return false;
	}

}
