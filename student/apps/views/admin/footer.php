					</div>
				</div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
<style type="text/css">
	.lds-roller {
	  display: inline-block;
	  position: relative;
	  width: 80px;
	  height: 80px;
	}
	.lds-roller div {
	  animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
	  transform-origin: 40px 40px;
	}
	.lds-roller div:after {
	  content: " ";
	  display: block;
	  position: absolute;
	  width: 7px;
	  height: 7px;
	  border-radius: 50%;
	  background: #0a7cf8;
	  margin: -4px 0 0 -4px;
	}
	.lds-roller div:nth-child(1) {
	  animation-delay: -0.036s;
	}
	.lds-roller div:nth-child(1):after {
	  top: 63px;
	  left: 63px;
	}
	.lds-roller div:nth-child(2) {
	  animation-delay: -0.072s;
	}
	.lds-roller div:nth-child(2):after {
	  top: 68px;
	  left: 56px;
	}
	.lds-roller div:nth-child(3) {
	  animation-delay: -0.108s;
	}
	.lds-roller div:nth-child(3):after {
	  top: 71px;
	  left: 48px;
	}
	.lds-roller div:nth-child(4) {
	  animation-delay: -0.144s;
	}
	.lds-roller div:nth-child(4):after {
	  top: 72px;
	  left: 40px;
	}
	.lds-roller div:nth-child(5) {
	  animation-delay: -0.18s;
	}
	.lds-roller div:nth-child(5):after {
	  top: 71px;
	  left: 32px;
	}
	.lds-roller div:nth-child(6) {
	  animation-delay: -0.216s;
	}
	.lds-roller div:nth-child(6):after {
	  top: 68px;
	  left: 24px;
	}
	.lds-roller div:nth-child(7) {
	  animation-delay: -0.252s;
	}
	.lds-roller div:nth-child(7):after {
	  top: 63px;
	  left: 17px;
	}
	.lds-roller div:nth-child(8) {
	  animation-delay: -0.288s;
	}
	.lds-roller div:nth-child(8):after {
	  top: 56px;
	  left: 12px;
	}
	@keyframes lds-roller {
	  0% {
	    transform: rotate(0deg);
	  }
	  100% {
	    transform: rotate(360deg);
	  }
	}
#loading{
	position: absolute;
	top: 40%;
	right: 0;
	left: 0;
	bottom: 0;
	z-index: 1000;
	text-align: center;
	display: none;
}
.form-check-label{
	cursor: pointer;
}
.bold_student{
	color:#000000;
	font-weight:600;
}

</style>	
<div id="loading">
	<div class="lds-roller">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>
    <script src="<?php print base_url();?>admin_theme/bower_components/moment/moment.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/demo_customizerce5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/maince5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/timepicki.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/datepicker.js"></script>
  	<link rel="stylesheet" href="<?php print base_url();?>admin_theme/css/easy-autocomplete.min.css">
	<script src="<?php print base_url();?>admin_theme/js/jquery.easy-autocomplete.min.js"></script>
	
  	<!-- <link rel="stylesheet" href="<?php print base_url();?>admin_theme/css/spectrum.css">
	<script src="<?php print base_url();?>admin_theme/js/spectrum.js"></script> -->
	<script type="text/javascript" src="<?php print base_url();?>admin_theme/tiny_mce/jquery.tinymce.js"></script>

    <script>
		$('.timepicker1').timepicki();
	 
	 $(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '<?php print base_url();?>admin_theme/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "jbimages,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "jbimages,|,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			  // ===========================================
			  // Set RELATIVE_URLS to FALSE (This is required for images to display properly)
			  // ===========================================
			 
			relative_urls: false,
			remove_script_host: false,
		});
	});
    </script>
    
	<script>
$(function(){
		
	$('.langselection').click(function(){
		var langval = $('.langselection input:checked').val();
		$.ajax({
			url: '<?php print base_url().'language/lang';?>',
			type: 'POST',
			data: { lang :langval },
			success: function(){
				location.reload();
			}
		});
	});	

    if ($('#ckeditor1_1').length) {
        CKEDITOR.replace('ckeditor1_1');
    }
	
});
</script>

<script>

$(function(){
	// $('.basicAutoComplete').autoComplete();

	var booking_subject_ids = $("#booking_subject_ids").val();
	var options = {
	  url: "<?php print base_url().$this->uri->segment(1).'/searchstudent/';?>" + booking_subject_ids,
	  getValue: "name",
	  list: {
	    onSelectItemEvent: function() {
			var value = $("#student_id").getSelectedItemData().code;
			$("#student_ids").val(value).trigger("change");
		},
		maxNumberOfElements: 1000,
	     match: {
	        enabled: true
	    }
	  },
	  theme: "square"
	};
	$("#student_id").easyAutocomplete(options);
	
	$('#addClassStudent').click(function(){
		var student_ids = $('#student_ids').val();
		var class_ids = $('#class_ids').val();
		// var schdule_date = $('#schdule_date').val();
		// var timepickerfrom = $('#timepickerfrom').val();
		$.post( '<?php print base_url();?>schdule/poststudent' , { 'student_ids': student_ids, 'class_ids': class_ids }, function(resp){
            if(resp > 0){
            	location.reload(true);
            }
		} , 'JSON');
	});


	$('#add_class').click(function(){
		$('#classModal').modal('show');
	});

	$('#newaddClass').click(function(){
		var subject_id = $('#subject_id').val();
		var teacher_id = $('#teacher_id').val();
		var school_id = $('#school_id').val();
		var schdule_date = $('#schdule_date').val();
		var timepickerfrom = $('#timepickerfrom').val();
		$.post( '<?php print base_url();?>schdule/addclass' , { 'subject_id': subject_id, 'teacher_id': teacher_id, 'schdule_date': schdule_date, 'timepickerfrom': timepickerfrom, 'school_id' : school_id }, function(resp){
            if(resp > 0){
            	location.reload(true);
            }
		} , 'JSON');
	});

	$('input.datepicker').datepicker({
        autoHide: true,
        zIndex: 2048,
    });

	var count=2;
	$("#addItem").click(function(){
		var html = '<div id="div'+count+'">'+
						'<div class="row">'+
							'<div class="col-sm-3">'+
								'<div class="form-group">'+
									'<label for=""> Class Date</label>'+
									'<div class="date-input">'+
										'<input autocomplete="off" class="datepicker form-control" placeholder="Class Date" type="text" value="" name="schdule_date[]">'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-3">'+
								'<div class="form-group">'+
									'<label for=""> Class Time From</label>'+
									'<div class="date-input">'+
										'<input class="form-control timepicker1" type="text" name="timepickerfrom[]"/>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-3">'+
								'<div class="form-group">'+
									'<label for=""> Class Time To</label>'+
									'<div class="date-input">'+
										'<input class="form-control timepicker1" type="text" name="timepickerto[]"/>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-2">'+
								'<div class="form-group">'+
									'<label for=""> No of Student</label>'+
									'<div class="date-inputss">'+
										'<input type="tel" name="noofstudent[]" class="form-control" value="" />'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="col-sm-1">'+
								'<label></label>'+
								'<button onclick="foo('+count+')" type="button" class="btn btn-info btn-xs">Remove</button>'+
							'</div>'+
						'</div>'+
					'</div>';
		$("#datetimerow").append(html);
		count++;
		$('.timepicker1').timepicki();
		$('input.datepicker').datepicker();
	});
});
function foo(which){
	$("#div"+which).remove();
}
function fooDiv(which){
	$("#divCanopy"+which).remove();
}
</script>	
<!-- <script type="text/javascript">
	$(document).ready(function(){
    $( ".tInfoModal" ).hover(function() {
           $('.modal').modal({
        show: true
    });
  });  
});
</script>
<script type="text/javascript">
	$(document).ready(function(){
    $( ".sInfoModal" ).hover(function() {
           $('.modal').modal({
        show: true
    });
  });  
});
</script> -->
</body>
</html>