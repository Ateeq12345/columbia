<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin Student</h6>
									<div class="row">
										<div class="col-sm-6">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/insert';?>" id="formValidate" enctype="multipart/form-data">
													
													<div class="form-group">
														<label for=""> Subject</label>
														<select name="admission" class="form-control" required>
															<option value="">Select Subject</option>
												<?php 
													if($subject->num_rows() > 0){
														foreach($subject->result() as $r){
												?>		
															<option value="<?php print $r->id;?>"><?php print ucfirst($r->name);?></option>
												<?php 
														}
													}
												?>		
														</select>
													</div>
													<div class="form-group">
														<label for=""> Teacher</label>
														<select name="active" class="form-control"></select>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
											
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	