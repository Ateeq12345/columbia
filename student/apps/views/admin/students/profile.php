<?php $this->load->view('admin/header');?>
<style type="text/css">
	.table-lg td {
    	padding: 5px;
	}
</style>			
	<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/profileadd';?>" id="formValidate" enctype="multipart/form-data">
		<input type="hidden" name="student_id" value="<?php print $this->uri->segment(3);?>">
	<?php 
		if($profile->num_rows() > 0){
			$r = $profile->row();
	?>	
		<input type="hidden" name="profile_id" value="<?php print $r->profile_id;?>">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="element-wrapper">
                                    <h6 class="element-header"><?php print $this->custom->getLang(8);?></h6>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
													
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(1);?></label>
														<input type="text" name="fullname" class="form-control" value="<?php print $r->fullname;?>" />
													</div>
													<div class="form-group">
														<label for=""> Email</label>
														<input type="text" name="email" class="form-control" value="<?php print $r->email;?>" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(2);?></label>
														<input type="text" name="streetaddredd" class="form-control" value="<?php print $r->streetaddredd;?>" />
													</div>
													<div class="form-group row">
														<label class="col-sm-4"> <?php print $this->custom->getLang(3);?></label>
														<div class="col-sm-8">
															<div class="form-check">
																<label class="form-check-label">
																	<input <?php if($r->gender == 'Male'){?>checked=""<?php } ?> class="form-check-input" name="gender" type="radio" value="<?php print $this->custom->getLang(4);?>"> <?php print $this->custom->getLang(4);?>
																</label>
															</div>
															<div class="form-check">
																<label class="form-check-label">
																	<input <?php if($r->gender == 'Female'){?>checked=""<?php } ?> class="form-check-input" name="gender" type="radio" value="<?php print $this->custom->getLang(5);?>"> <?php print $this->custom->getLang(5);?>
																</label>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(6);?></label>
														<input type="text" name="phonenumber" class="form-control" value="<?php print $r->phonenumber;?>"/>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(7);?></label>
														<input autocomplete="off" type="text" name="birthday" class="form-control datepicker" value="<?php print $r->birthday;?>"/>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(11);?></label>
														<textarea name="noteofattending" class="form-control"><?php print $r->noteofattending;?></textarea>
													</div>
											
											</div>
										</div>
                                    </div>
								</div>
							</div>


                            <div class="col-sm-6">
                                <div class="element-wrapper">
                                    <h6 class="element-header"><?php print $this->custom->getLang(9);?></h6>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
													
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(10);?></label>
														<input autocomplete="off" type="text" name="expirydate" class="form-control datepicker"  value="<?php print $r->expirydate;?>"/>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(12);?></label>
														<input type="text" name="coursename" class="form-control"  value="<?php print $r->coursename;?>"/>
													</div>

													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(27);?></label>
														<input type="text" name="startdate" autocomplete="off" class="form-control datepicker"  value="<?php print $r->startdate;?>"/>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(28);?></label>
														<input type="text" name="numberofcourses" class="form-control"  value="<?php print $r->numberofcourses;?>"/>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(29);?></label>
														<input type="text" name="instructionform" class="form-control"  value="<?php print $r->instructionform;?>"/>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(30);?></label>
														<textarea name="contracttype" class="form-control"><?php print $r->contracttype;?></textarea>
													</div>
													
											
											</div>
										</div>
                                    </div>
								</div>
							</div>

						</div>

						<div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header"><?php print $this->custom->getLang(11);?></h6>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
												<div class="form-desc"><?php print $this->custom->getLang(31);?></div>	
							<?php 
								if($enrollment->num_rows() > 0){
									$enroll = $enrollment->row();
							?>					
												<table class="table table-lg">
													<tr>
														<td style="width: 200px;"><?php print $this->custom->getLang(32);?></td>
														<td>
															<table class="table table-bordered table-lg">
																<tr>
																	<th style="width: 150px;"><?php print $this->custom->getLang(33);?></th>
																	<th><?php print $this->custom->getLang(34);?></th>
																	<th><?php print $this->custom->getLang(35);?></th>
																	<th><?php print $this->custom->getLang(36);?></th>
																	<th><?php print $this->custom->getLang(37);?></th>
																	<th><?php print $this->custom->getLang(38);?></th>
																</tr>
																<tr>
																	<td><?php print $this->custom->getLang(39);?></td>
																	<td>
																		<input type="text" name="enrollmentfee_listproce" class="form-control fee_total" value="<?php print $enroll->enrollmentfee_listproce;?>">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_discount" class="form-control dis_total" value="<?php print $enroll->enrollmentfee_discount;?>">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_subtotal" class="form-control sub_total" value="<?php print $enroll->enrollmentfee_subtotal;?>">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_saletax" class="form-control sale_total" value="<?php print $enroll->enrollmentfee_saletax;?>">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_meter" class="form-control meter_total" value="<?php print $enroll->enrollmentfee_meter;?>">
																	</td>
																</tr>
																<tr>
																	<td><?php print $this->custom->getLang(40);?></td>
																	<td>
																		<input type="text" name="tuitionfee_listproce" class="form-control fee_total_1" value="<?php print $enroll->tuitionfee_listproce;?>">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_discount" class="form-control dis_total_1" value="<?php print $enroll->tuitionfee_discount;?>">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_subtotal" class="form-control sub_total_1" value="<?php print $enroll->tuitionfee_subtotal;?>">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_saletax" class="form-control sale_total_1" value="<?php print $enroll->tuitionfee_saletax;?>">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_meter" class="form-control meter_total_1" value="<?php print $enroll->tuitionfee_meter;?>">
																	</td>
																</tr>
																<tr>
																	<td><?php print $this->custom->getLang(41);?></td>
																	<td>
																		<input type="text" name="curriculumfee_listproce" class="form-control fee_total_2" value="<?php print $enroll->curriculumfee_listproce;?>">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_discount" class="form-control dis_total_2" value="<?php print $enroll->curriculumfee_discount;?>">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_subtotal" class="form-control sub_total_2" value="<?php print $enroll->curriculumfee_subtotal;?>">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_saletax" class="form-control sale_total_2" value="<?php print $enroll->curriculumfee_saletax;?>">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_meter" class="form-control meter_total_2" value="<?php print $enroll->curriculumfee_meter;?>">
																	</td>
																</tr>
																<tr>
																	<td colspan="4"></td>
																	<td align="center"><?php print $this->custom->getLang(42);?></td>
																	<td>
																		<input type="text" name="curriculumfee_meter_total" class="form-control" value="<?php print $enroll->curriculumfee_meter_total;?>">
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>	
							<?php } ?>	
							<?php 
								if($plan->num_rows() > 0){
									$p = $plan->result();
							?>					
												<table class="table table-lg">
													<tr>
														<td style="width: 200px;"><?php print $this->custom->getLang(43);?></td>
														<td>
															<table class="table table-bordered table-lg">
																<tr>
																	<th style="width: 150px;"><?php print $this->custom->getLang(44);?></th>
																	<th><?php print $this->custom->getLang(45);?></th>
																	<th><?php print $this->custom->getLang(46);?></th>
																	<th><?php print $this->custom->getLang(47);?></th>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker" value="<?php print $p[0]->paymentplan_date;?>"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control" value="<?php print $p[0]->paymentplan_method;?>">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control" value="<?php print $p[0]->paymentplan_payment;?>">
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker" value="<?php print $p[1]->paymentplan_date;?>"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control" value="<?php print $p[1]->paymentplan_method;?>">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control" value="<?php print $p[1]->paymentplan_payment;?>">
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker" value="<?php print $p[2]->paymentplan_date;?>"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control" value="<?php print $p[2]->paymentplan_method;?>">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control" value="<?php print $p[2]->paymentplan_payment;?>">
																	</td>
																	<td><?php print $this->custom->getLang(48);?></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker" value="<?php print $p[3]->paymentplan_date;?>"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control" value="<?php print $p[3]->paymentplan_method;?>">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control" value="<?php print $p[3]->paymentplan_payment;?>">
																	</td>
																	<td><input autocomplete="off" type="text" name="paymentplan_datepicker" class="form-control datepicker" value="<?php print $p[3]->paymentplan_datepicker;?>"></td>
																</tr>
																<tr>
																	<td><input type="text" autocomplete="off" name="paymentplan_date[]" class="form-control datepicker" value="<?php print $p[4]->paymentplan_date;?>"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control" value="<?php print $p[4]->paymentplan_method;?>">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control" value="<?php print $p[4]->paymentplan_payment;?>">
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td><input type="text" autocomplete="off" name="paymentplan_date[]" class="form-control datepicker" value="<?php print $p[5]->paymentplan_date;?>"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control" value="<?php print $p[5]->paymentplan_method;?>">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control" value="<?php print $p[5]->paymentplan_payment;?>">
																	</td>
																	<td></td>
																</tr>

															</table>
														</td>
													</tr>
												</table>	
							<?php } ?>						
											
											
											</div>
										</div>
                                    </div>
								</div>
							</div>

						</div>


	<?php } else { ?>
						<div class="row">
                            <div class="col-sm-6">
                                <div class="element-wrapper">
                                    <h6 class="element-header"><?php print $this->custom->getLang(8);?></h6>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
													
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(1);?></label>
														<input type="text" name="fullname" class="form-control" />
													</div>
													<div class="form-group">
														<label for=""> Email</label>
														<input type="text" name="email" class="form-control" value="" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(2);?></label>
														<input type="text" name="streetaddredd" class="form-control" />
													</div>
													<div class="form-group row">
														<label class="col-sm-4"> <?php print $this->custom->getLang(3);?></label>
														<div class="col-sm-8">
															<div class="form-check">
																<label class="form-check-label">
																	<input checked="" class="form-check-input" name="gender" type="radio" value="<?php print $this->custom->getLang(4);?>"> <?php print $this->custom->getLang(4);?>
																</label>
															</div>
															<div class="form-check">
																<label class="form-check-label">
																	<input class="form-check-input" name="gender" type="radio" value="<?php print $this->custom->getLang(5);?>"> <?php print $this->custom->getLang(5);?>
																</label>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(6);?></label>
														<input type="text" name="phonenumber" class="form-control" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(7);?></label>
														<input autocomplete="off" type="text" name="birthday" class="form-control datepicker" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(11);?></label>
														<textarea name="noteofattending" class="form-control"></textarea>
													</div>
											
											</div>
										</div>
                                    </div>
								</div>
							</div>


                            <div class="col-sm-6">
                                <div class="element-wrapper">
                                    <h6 class="element-header"><?php print $this->custom->getLang(9);?></h6>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
													
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(10);?></label>
														<input autocomplete="off" type="text" name="expirydate" class="form-control datepicker" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(12);?></label>
														<input type="text" name="coursename" class="form-control" />
													</div>

													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(27);?></label>
														<input autocomplete="off" type="text" name="startdate" class="form-control datepicker" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(28);?></label>
														<input type="text" name="numberofcourses" class="form-control" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(29);?></label>
														<input type="text" name="instructionform" class="form-control" />
													</div>
													<div class="form-group">
														<label for=""> <?php print $this->custom->getLang(30);?></label>
														<textarea name="contracttype" class="form-control"></textarea>
													</div>
													
											
											</div>
										</div>
                                    </div>
								</div>
							</div>

						</div>

						<div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header"><?php print $this->custom->getLang(11);?></h6>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
												<div class="form-desc"><?php print $this->custom->getLang(31);?></div>	
												<table class="table table-lg">
													<tr>
														<td style="width: 200px;"><?php print $this->custom->getLang(32);?></td>
														<td>
															<table class="table table-bordered table-lg">
																<tr>
																	<th style="width: 150px;"><?php print $this->custom->getLang(33);?></th>
																	<th><?php print $this->custom->getLang(34);?></th>
																	<th><?php print $this->custom->getLang(35);?></th>
																	<th><?php print $this->custom->getLang(36);?></th>
																	<th><?php print $this->custom->getLang(37);?></th>
																	<th><?php print $this->custom->getLang(38);?></th>
																</tr>
																<tr>
																	<td><?php print $this->custom->getLang(39);?></td>
																	<td>
																		<input type="text" name="enrollmentfee_listproce" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_discount" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_subtotal" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_saletax" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="enrollmentfee_meter" class="form-control">
																	</td>
																</tr>
																<tr>
																	<td><?php print $this->custom->getLang(40);?></td>
																	<td>
																		<input type="text" name="tuitionfee_listproce" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_discount" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_subtotal" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_saletax" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="tuitionfee_meter" class="form-control">
																	</td>
																</tr>
																<tr>
																	<td><?php print $this->custom->getLang(41);?></td>
																	<td>
																		<input type="text" name="curriculumfee_listproce" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_discount" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_subtotal" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_saletax" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="curriculumfee_meter" class="form-control">
																	</td>
																</tr>
																<tr>
																	<td colspan="4"></td>
																	<td align="center"><?php print $this->custom->getLang(42);?></td>
																	<td>
																		<input type="text" name="curriculumfee_meter_total" class="form-control">
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>	
													
												<table class="table table-lg">
													<tr>
														<td style="width: 200px;"><?php print $this->custom->getLang(43);?></td>
														<td>
															<table class="table table-bordered table-lg">
																<tr>
																	<th style="width: 150px;"><?php print $this->custom->getLang(44);?></th>
																	<th><?php print $this->custom->getLang(45);?></th>
																	<th><?php print $this->custom->getLang(46);?></th>
																	<th><?php print $this->custom->getLang(47);?></th>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control">
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control">
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control">
																	</td>
																	<td><?php print $this->custom->getLang(48);?></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control">
																	</td>
																	<td><input autocomplete="off" type="text" name="paymentplan_datepicker" class="form-control datepicker"></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control">
																	</td>
																	<td></td>
																</tr>
																<tr>
																	<td><input autocomplete="off" type="text" name="paymentplan_date[]" class="form-control datepicker"></td>
																	<td>
																		<input type="text" name="paymentplan_method[]" class="form-control">
																	</td>
																	<td>
																		<input type="text" name="paymentplan_payment[]" class="form-control">
																	</td>
																	<td></td>
																</tr>
																
															</table>
														</td>
													</tr>
												</table>	
													
											
											
											</div>
										</div>
                                    </div>
								</div>
							</div>

						</div>
	<?php } ?>				

						<div class="row">

                            <div class="col-sm-6">
                                <div class="form-buttons-w">
									<button class="btn btn-primary" type="submit"> Submit</button>
								</div>
							</div>

						</div>								
	</form>
			
<?php $this->load->view('admin/footer');?>	