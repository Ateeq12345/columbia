<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Edit Student</h6>
                                    <?php print flash_message();?>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/update';?>" id="formValidate" enctype="multipart/form-data">
									<?php 
										if($user->num_rows() > 0){
											$row = $user->row();
									?>			
													<div class="form-group">
														<label for=""> First name</label>
														<input type="text" name="fname" class="form-control" data-error="Please enter Firstname." placeholder="Enter Firstname" required="required" value="<?php print $row->fname;?>"/>
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Lastname</label>
														<input type="text" name="lname" class="form-control" data-error="Please enter Lastname." placeholder="Enter Lastname" required="required" value="<?php print $row->lname;?>">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Email address</label>
														<input type="email" name="email" class="form-control" data-error="Your email address is invalid" placeholder="Enter email" required="required" value="<?php print $row->email;?>">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Address</label>
														<input type="text" name="address" class="form-control" data-error="Please enter Address." placeholder="Enter Address" value="<?php print $row->address;?>" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Phone</label>
														<input type="tel" name="phone" class="form-control" data-error="Please enter Phone." placeholder="Enter Phone" value="<?php print $row->phone;?>" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Home Phone</label>
														<input type="tel" name="homephone" class="form-control" placeholder="Enter Home Phone" value="<?php print $row->homephone;?>" />
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Counsellor</label>
														<input type="text" name="counsellor" class="form-control" placeholder="Enter Counsellor" value="<?php print $row->counsellor;?>"/>
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Subject</label>
														<select name="subject_id" class="form-control">
										<?php 
											if($subject->num_rows() > 0){
												foreach($subject->result() as $sub){
										?>
															<option value="<?php print $sub->id;?>"<?php if($row->subject_id == $sub->id){?> selected<?php }?>><?php print $sub->name;?></option>
										<?php
												}
											}
										?>
														</select>
													</div>
													<div class="form-group">
														<label for=""> Student Picture</label>
														<input type="file" name="fimg" class="form-control"/>
													<?php if(!empty($row->fimg)){?>	
														<img src="<?php print base_url().'upload/student/'.$row->fimg;?>" alt="" width="150"/>
													<?php } ?>	
													</div>
													<div class="form-group">
														<label for=""> Student Info</label>
														<input type="text" name="student_info" class="form-control" placeholder="Student Info" value="<?php print $row->student_info;?>">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>

                                            <div class="form-group">
                                                <label for="">Select Demo Date</label>
                                                <input data-error="Please enter Demo Date." type="text" value="<?php print $row->demo_date;?>" name="demo_date" class="form-control datepicker" placeholder="Please Enter Demo Date" autocomplete="off">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Select Demo Time</label>
                                                <input  type="text" name="demo_time" value="<?php print $row->demo_time;?>"  class="time_element form-control" placeholder="Please Enter Demo Time" autocomplete="off"/>
                                             </div>
											<div class="form-group">
												<label for="">Free Classes</label>
												<input type="text" name="total_free" class="form-control" placeholder="Free Classes" value="<?php print $row->total_free;?>">
												<div class="help-block form-text with-errors form-control-feedback"></div>
											</div>
											<div class="form-group">
												<label for=""> Total Classes</label>
												<input type="text" name="total_class" class="form-control" placeholder="Total Classes" value="<?php print $row->total_class;?>" >
												<div class="help-block form-text with-errors form-control-feedback"></div>
											</div>
                                            <div class="form-group">
                                                <label for="">Joining Date</label>
                                                <input data-error="Please enter Joining Date."  value="<?php print $row->joining_date;?>" type="text" name="joining_date" class="form-control datepicker" placeholder="Please Enter Joining Date" autocomplete="off">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Contract Expiry Date</label>
                                                <input data-error="Please enter Contract Expiry Date."  value="<?php print $row->contract_expiry_date;?>" type="text" name="contract_expiry_date" class="form-control datepicker" placeholder="Please Enter Contract Expiry Date" autocomplete="off">
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
													<div class="form-group">
														<label for=""> Password</label>
														<input type="password" name="password" class="form-control" data-minlength="6" placeholder="Password" />
														<div class="help-block form-text text-muted form-control-feedback">Minimum of 6 characters</div>
													</div>
													<div class="form-group">
														<label for=""> Admission</label>
														<select name="admission" class="form-control">
															<option value="1"<?php if($row->admission == '1'){?> selected<?php }?>>Trial Class</option>
															<option value="0"<?php if($row->admission == '0'){?> selected<?php }?>>Confirm Admission</option>
														</select>
													</div>
													<div class="form-group">
														<label for=""> Student Comments</label>
														<textarea name="student_comments" class="form-control tinymce"><?php print html_entity_decode($row->student_comments);?></textarea>
													</div>

                                            <div class="form-group">
                                                <label for=""> Source</label>
                                                <div class="box-bordered form-group mt_5 custom-input-check source-bar">
                                                    <div>
                                                        <input id="1" name="source" <?php if($row->source == 'google'){?> checked <?php }?> value="google" type="radio">
                                                        <label class="no-margin" for="1">
                                                            <b>Google </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="2" name="source"  <?php if($row->source == 'billboard'){?> checked <?php }?> value="billboard" type="radio">
                                                        <label class="no-margin" for="2">
                                                            <b>BillBoard </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="3" name="source"  <?php if($row->source == 'walkin'){?> checked <?php }?> value="walkin" type="radio">
                                                        <label class="no-margin" for="3">
                                                            <b>Walk-in </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="4" name="source"  <?php if($row->source == 'student_introduction'){?> checked <?php }?> value="student_introduction" type="radio">
                                                        <label class="no-margin" for="4">
                                                            <b>Student Introduction </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="5" name="source" <?php if($row->source == 'counsellor'){?> checked <?php }?> value="counsellor" type="radio">
                                                        <label class="no-margin" for="5">
                                                            <b>Counsellor </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="6" name="source" <?php if($row->source == 'pamphlet'){?> checked <?php }?> value="pamphlet" type="radio">
                                                        <label class="no-margin" for="6">
                                                            <b>Pamphlet </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="7" name="source" <?php if($row->source == 'others'){?> checked <?php }?> value="others" type="radio">
                                                        <label class="no-margin" for="7">
                                                            <b>Others </b><br>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for=""> Add Levels of Sudent</label>
                                                <div class="box-bordered form-group mt_5 custom-input-check source-bar">
                                                    <div>
                                                        <input id="8" name="level" <?php if($row->level == 'level_1'){?> checked <?php }?>  value="level_1" type="radio">
                                                        <label class="no-margin" for="8">
                                                            <b>Level 1 </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="9" name="level" <?php if($row->level == 'level_2'){?> checked <?php }?>  value="level_2" type="radio">
                                                        <label class="no-margin" for="9">
                                                            <b>Level 2 </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="10" name="level" <?php if($row->level == 'level_3'){?> checked <?php }?>  value="level_3" type="radio">
                                                        <label class="no-margin" for="10">
                                                            <b>Level 3 </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="11" name="level" <?php if($row->level == 'level_4'){?> checked <?php }?>  value="level_4" type="radio">
                                                        <label class="no-margin" for="11">
                                                            <b>Level 4 </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="12" name="level" <?php if($row->level == 'be_ml_elementary'){?> checked <?php }?>  value="be_ml_elementary" type="radio">
                                                        <label class="no-margin" for="12">
                                                            <b>BE. ML Elementary </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="13" name="level" <?php if($row->level == 'be_pre_inter'){?> checked <?php }?>  value="be_pre_inter" type="radio">
                                                        <label class="no-margin" for="13">
                                                            <b>BE. Pre Inter </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="14" name="level" <?php if($row->level == 'be_inter'){?> checked <?php }?>  value="be_inter" type="radio">
                                                        <label class="no-margin" for="14">
                                                            <b>BE. Inter </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="15" name="level" <?php if($row->level == 'be_upper_inter'){?> checked <?php }?>  value="be_upper_inter" type="radio">
                                                        <label class="no-margin" for="15">
                                                            <b>BE. Upper Inter </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="16" name="level" <?php if($row->level == 'be_advance'){?> checked <?php }?>  value="be_advance" type="radio">
                                                        <label class="no-margin" for="16">
                                                            <b>BE. Advance </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="17" name="level" <?php if($row->level == 'apex_level'){?> checked <?php }?>  value="apex_level" type="radio">
                                                        <label class="no-margin" for="17">
                                                            <b>Apex Level </b><br>
                                                        </label>
                                                    </div>
                                                    <div>
                                                        <input id="18" name="level" <?php if($row->level == 'toeic'){?> checked <?php }?> value="toeic" type="radio">
                                                        <label class="no-margin" for="18">
                                                        <b>TOEIC </b><br>
                                                      </label>
                                                    </div>
                                                    <div>
                                                          <input id="19" name="level" <?php if($row->level == 'ielts'){?> checked <?php }?> value="ielts" type="radio">
                                                          <label class="no-margin" for="19">
                                                          <b>IELTS </b><br>
                                                          </label>
                                                    </div>
                                                    <div>
                                                          <input id="20" name="level" <?php if($row->level == 'toefl'){?> checked <?php }?> value="toefl" type="radio">
                                                          <label class="no-margin" for="20">
                                                          <b>TOEFL  </b><br>
                                                          </label>
                                                    </div>
                                                </div>
                                            </div>
													<div class="form-group">
														<label for=""> Status</label>
														<select name="active" class="form-control">
															<option value="1"<?php if($row->active == '1'){?> selected<?php }?>>Active</option>
															<option value="0"<?php if($row->active == '0'){?> selected<?php }?>>Block</option>
														</select>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
													<input type="hidden" name="id" value="<?php print $row->id;?>"/>
										<?php } ?>		
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	
<script type="text/javascript">
 $(document).ready(function(){
    $(".time_element").timepicki();
  });
</script>