<?php $this->load->view('admin/header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="element-wrapper">
            <h6 class="element-header">Admin Students</h6>
            <?php print flash_message(); ?>
            <div class="controls-above-table">
                <div class="row">
                    <div class="col-sm-6">
        <?php if(   $this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1 ) { ?>   
                        <a class="btn btn-sm btn-success" href="<?php print base_url() . $this->uri->segment(1) . '/add'; ?>"><i class="icon-plus"></i> Add Students</a>
        <?php }?>
                    </div>
                    <div class="col-sm-6">
                        <form method="get" name="post" action="<?php print base_url() . $this->uri->segment(1) . '/search' ?>" class="form-inline justify-content-sm-end">
                            <input class="form-control form-control-sm rounded bright" placeholder="Search" name="q" type="text">
                        </form>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-lg table-v2 table-striped">
                    <thead>
                        <tr>
                            <th>Sr#</th>
                            <th>Name</th>
                            <th>Subject</th>
                            <th>Email</th>
                            <th>Total Classes</th>
                            <th>Attend Classes</th>
                            <th>Remaining Classes</th>
                            <th>Phone Number</th>
                            <th>Status</th>
                            <th>Admission</th>
                            <th>Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                   
                    <tbody>
                        <?php
                        if ($users->num_rows() > 0) {
                            foreach ($users->result() as $row) {
                        ?>

                                <tr>
                                    <td>
                                        <?php print $row->id; ?>
                                    </td>
                                    <td class="text-center"><div class="user-img">
                                            <?php if ($row->fimg){?>
                                                <img src="<?php print base_url().'upload/student/'.$row->fimg; ?>">
                                            <?php }else{?>
                                                <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg">
                                            <?php }?>
                                        </div><?php print ucfirst($row->fname . ' ' . $row->lname); ?></td>
                                    <td><b style="background-color: <?php print $this->subjects_model->getColorById($row->subject_id);?>;padding: 3px 10px;color: <?php print $this->subjects_model->getFontColorById($row->subject_id);?>;"><?php print $this->subjects_model->getNameById($row->subject_id); ?></b></td>
                                    <td><?php print $row->email; ?></td>
                                    <td><?php print $row->total_class; ?></td>
                                    <td><?php print $this->custom->getClassById($row->id); ?></td>
                                    <td><?php
                                        if($row->total_class > 0) {
                                            $remaingClass = $row->total_class - $this->custom->getClassById($row->id);
                                            if ($remaingClass > 0) {
                                                print $remaingClass;
                                            } 
                                        }else {
                                            print 0;
                                        }
                                        ?></td>
                                       <td><?php print $row->phone; ?></td>
                                    <td><?php if ($row->active == 1) {
                                            print 'Active';
                                        } else {
                                            print 'Block';
                                        } ?></td>
                                    <td><?php if ($row->admission == 1) {
                                            print '<span class="mr-2 mb-2 btn btn-danger">Trial Class</span>';
                                        } else {
                                            print '<span class="mr-2 mb-2 btn btn-success">Confirm Admission</span>';
                                        } ?></td>
                                    <td><?php print $row->created; ?></td>
                                    <td class="row-actions">
        <?php if(   $this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 ) { ?> 
                                        <a data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="<?php print base_url() . $this->uri->segment(1) . '/edit/' . $row->id; ?>"><i class="os-icon os-icon-ui-49"></i></a>
                                        <a href="<?php print base_url() . $this->uri->segment(1) . '/profile/' . $row->id; ?>"><i class="os-icon os-icon-grid-10"></i></a>
                                        <a href="javascript:void(0);" onclick="loadClassTeacher(<?php print $row->id ?>,<?php print $row->subject_id ?>,'<?php print $this->subjects_model->getNameById($row->subject_id); ?>');" title="Book A Class"><i class="fa fa-clock-o"></i></a>
        <?php } ?>
        <?php if(   $this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1 ) { ?> 
                                        <a data-placement="top" data-toggle="tooltip" data-original-title="Delete" class="danger" onclick="return confirm('Are your Sure to delete this?');" href="<?php print base_url() . $this->uri->segment(1) . '/delete/' . $row->id; ?>"><i class="os-icon os-icon-ui-15"></i></a>
        <?php } ?>
        <?php if(   $this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 ) { ?> 
                                        <a style="margin-right: 0.8rem;" data-placement="top" data-toggle="tooltip" data-original-title="Class History" href="<?php print base_url() . $this->uri->segment(1) . '/bookings/' . $row->id; ?>"><i class="fa fa-clock-o"></i></a>
        <?php } ?>

                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="controls-below-table">
                <div class="table-records-pages">
                    <ul class="pagination">
                        <?php print $pagination; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="classModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Class</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label id="teacher_subject"></label>
                    <select class="form-control" name="teacer_id" id="teacherBlock"></select>
                </div>
                <div class="form-group">
                    <label for=""> Class Date</label>
                    <div class="date-input">
                        <input id="schdule_date" autocomplete="off" class="datepicker form-control" placeholder="Class Date" type="text" value="" name="schdule_date">
                    </div>
                </div>
                <div class="form-group">
                    <label for=""> Class Time</label>
                    <div class="date-input">
                        <input class="form-control timepicker1" type="text" id="timepickerfrom" name="timepickerfrom" />
                    </div>
                </div>
                <input type="hidden" id="student_ids" name="student_ids" value="" />
                <input type="hidden" id="subject_id" name="subject_id" value="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="newStudentaddClass">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function loadClassTeacher(id, subject_id, subject_name) {
        $.ajax({
            url: '<?php print base_url() . $this->uri->segment(1) . '/getclassteacher' ?>',
            type: 'GET',
            data: {
                's': subject_id
            },
            success: function(resp) {
                var r = JSON.parse(resp);
                $('#student_ids').val(id);
                $('#subject_id').val(subject_id);
                $('#teacherBlock').html(r);
                $('#teacher_subject').html('Teacher List of ( ' + subject_name + ' ) ');
                $('#classModal').modal('show');
            }
        });
    }

    $(function() {
        $('#newStudentaddClass').click(function() {
            var student_ids = $('#student_ids').val();
            var subject_id = $('#subject_id').val();
            var teacher_id = $('#teacherBlock').val();
            var schdule_date = $('#schdule_date').val();
            var timepickerfrom = $('#timepickerfrom').val();
            $.post('<?php print base_url() . $this->uri->segment(1) . '/addclass' ?>', {
                'student_ids': student_ids,
                'subject_id': subject_id,
                'teacher_id': teacher_id,
                'schdule_date': schdule_date,
                'timepickerfrom': timepickerfrom
            }, function(resp) {
                if (resp > 0) {
                    location.reload(true);
                }
            }, 'JSON');
        });
    });
</script>
<?php $this->load->view('admin/footer'); ?>