<?php $this->load->view('admin/header');?>
<style type="text/css">
	body{
		color:#000000 !important;
	}
	.invoice-w{
		padding: 20px 30px;
    	padding-bottom: 0px;
	}
	.invoice-w::before {
	    width: 140%;
	    height: 450px;
	    background-color: #FFFFFF;
	    position: relative;
	    top: 0%;
	    left: 0%;
	    -webkit-transform: rotate(0deg);
	    transform: rotate(0deg);
	    content: "";
	    z-index: 1;
	}
	.invoice-heading {
	    margin-bottom: 0.5rem;
	    margin-top: 0.5rem;
	    position: relative;
	    z-index: 2;
	    font-size: 10.5px;
	}
	.single-border tr td{
		border: 1px solid #EEEEEE;
		border-collapse: collapse;
		padding: 0;
		margin:0;
	}
	.no-border{
		border: 0px solid #EEEEEE;
		width:100%;
		border-collapse: collapse;
		padding: 0;
		margin:0;
	}
	.no-borders tr td{
		border: 0px solid #EEEEEE;
	}
	div.vert{
    	writing-mode: vertical-lr;
    	vertical-align: middle;
    	text-align: center;
    }
    .pad tr td{
    	padding:5px;
    }
</style>
<script type="text/javascript">
	function printdiv(printpage)
	{
		var headstr = "<html><head><title></title></head><body>";
		var footstr = "</body>";
		var newstr = document.all.item(printpage).innerHTML;
		var oldstr = document.body.innerHTML;
		document.body.innerHTML = headstr+newstr+footstr;
		window.print();
		document.body.innerHTML = oldstr;
		return false;
	}
</script>
<div class="printbtn">
	<button type="button" class="btn btn-info" onClick="printdiv('printDiv');">Print</button>
</div>
<div id="printDiv">


<div class="content-is">
    <div class="content-boxs" style="width: 1000px;background: #FFFFFF;
    padding: 20px;">
        <?php 
        	if($profile->num_rows() > 0){
        		$p = $profile->row();
        ?>        
                <table width="100%">
                	 <tr>
                	 	<td width="100%">
                	 		<table width="100%">
                	 			<tr>
                	 				<td width="60%" align="center" valign="middle"><h3><?php print $this->custom->getLang(68);?></h3></td>
                	 				<td>
                	 					<table border="1" width="100%">
                	 						<tr>
                	 							<td width="50%" align="center" valign="middle"><?php print $this->custom->getLang(69);?></td>
                	 							<td align="center" valign="middle">JP<?php print $p->student_id;?></td>
                	 						</tr>
                	 						<tr>
                	 							<td width="50%" align="center" valign="middle"><?php print $this->custom->getLang(70);?></td>
                	 							<td align="center" valign="middle"><?php print date('Y-m-d', strtotime($p->created));?></td>
                	 						</tr>
                	 						<tr>
                	 							<td width="50%" align="center" valign="middle"><?php print $this->custom->getLang(71);?></td>
                	 							<td align="center" valign="middle"><?php print $p->student_id;?></td>
                	 						</tr>
                	 					</table>
                	 				</td>
                	 			</tr>
                	 		</table>
                	 		<table width="100%" style="margin:25px 0;">
                	 			<tr>
                	 				<td align="center" valign="middle"><?php print $this->custom->getLang(72);?></td>
                	 			</tr>
                	 		</table>
                	 		<table width="100%" border="1">
                	 			<tr>
                	 				<td>
                	 					<div style="width: 25px;height:120px;text-align:center;float:left;writing-mode: vertical-lr;-webkit-writing-mode: vertical-lr;-ms-writing-mode: vertical-lr;text-orientation: upright;border-right: 1px solid #999999;padding: 5px 1px;">
                	 						<?php print nl2br($this->custom->getLang(73));?>
                	 					</div>
                	 					<div style="width: 100px;float: left;">
                	 						<div style="width: 100%;display: block;text-align:center;padding-top:18px;border-right: 1px solid #999999;height: 60px;">
                	 							<?php print nl2br($this->custom->getLang(1));?>
                	 						</div>
                	 						<div style="width: 100%;display: block;text-align:center;padding-top:18px;border-top: 1px solid #999999;border-right: 1px solid #999999;height: 60px;">
                	 							<?php print nl2br($this->custom->getLang(2));?>
                	 						</div>
                	 						<div style="clear: both;"></div>
                	 					</div>
                	 					<div style="width: 769px;float: left;">
	                	 					<div style="width: 250px;height: 80px;float: left;">
	                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
	                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding: 0;font-size: 12px;height: 20px;"><?php print $this->students_model->getFnameById($p->student_id);?></p>
	                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding: 0;font-size: 12px;height: 20px;"><?php print $p->email;?></p>
	                	 							<p style="border-bottom: 1px solid #999999;height: 20px;margin:0;padding: 0;font-size: 12px;"></p>
	                	 							<p style="margin:0;padding: 0;font-size: 12px;height: 20px;"><?php print $this->students_model->getlnameById($p->student_id);?></p>
	                	 						</div>
	                	 						<div style="clear: both;"></div>
	                	 					</div>
	                	 					<div style="width: 100px;height: 80px;float: left;">
	                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
	                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding: 0;padding-top:5px;font-size: 12px;height: 30px;"><?php print $this->custom->getLang(3);?></p>
	                	 							<p style="margin:0;padding: 0;padding-top:5px;font-size: 12px;height: 30px;"><?php print $p->gender;?></p>
	                	 						</div>
	                	 						<div style="clear: both;"></div>
	                	 					</div>
	                	 					<div style="width: 150px;height: 80px;float: left;">
	                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
	                	 							<p style="border-bottom: 0px solid #999999;margin:0;padding: 0;padding-top:20px;font-size: 12px;height: 80px;color:#CCCCCC;"><?php print $this->custom->getLang(74);?></p>
	                	 						</div>
	                	 						<div style="clear: both;"></div>
	                	 					</div>
	                	 					<div style="width: 120px;height: 80px;float: left;">
	                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
	                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding: 0;padding-top:5px;font-size: 12px;height: 30px;"><?php print $this->custom->getLang(6);?></p>
	                	 							<p style="margin:0;padding: 0;padding-top:5px;font-size: 12px;height: 30px;"><?php print $this->custom->getLang(7);?></p>
	                	 						</div>
	                	 						<div style="clear: both;"></div>
	                	 					</div>
	                	 					<div style="width: 149px;height: 80px;float: left;">
	                	 						<div style="width: 100%;display: block;text-align:center;">
	                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding: 0;padding-top:5px;font-size: 12px;height: 30px;"></p>
	                	 							<p style="margin:0;padding: 0;padding-top:5px;font-size: 12px;height: 30px;"></p>
	                	 						</div>
	                	 						<div style="clear: both;"></div>
	                	 					</div>
	                	 					<div style="width: 100%;height: 80px;border-top:1px solid #666666;float: left;">
	                	 						<p style="border-bottom: 1px solid #999999;margin:0;padding:0 5px;font-size: 12px;height: 20px;"><?php print $this->students_model->getFnameById($p->student_id);?></p>
	                	 						<p style="padding:5px;"><?php print $p->streetaddredd;?></p>
	                	 						<div style="clear: both;"></div>
	                	 					</div>
	                	 						<div style="clear: both;"></div>
                	 					</div>

                	 				</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;">
                	 			<tr>
                	 				<td>
                	 					
                	 					<div style="width: 126px;float: left;">
                	 						<div style="width: 100%;display: block;text-align:center;padding-top:5px;border-right: 1px solid #999999;border-top: 1px solid #999999;border-left: 1px solid #999999;height: 30px;">
                	 							<?php print $this->custom->getLang(10);?>
                	 						</div>
                	 						<div style="clear: both;"></div>
                	 					</div>
                	 					<div style="width: 200px;float: left;">
                	 						<div style="width: 100%;display: block;text-align:center;padding-top:5px;border-right: 1px solid #999999;border-top: 1px solid #999999;border-left: 0px solid #999999;height: 30px;">
                	 							<?php print $p->expirydate;?>
                	 						</div>
                	 						<div style="clear: both;"></div>
                	 					</div>
	                	 				<div style="clear: both;"></div>
	                	 				<div style="width: 100%; display: block;border:1px solid #666666;">
	                	 					<div style="width: 25px;height:90px;text-align:center;float:left;writing-mode: vertical-lr;-webkit-writing-mode: vertical-lr;-ms-writing-mode: vertical-lr;text-orientation: upright;border-right: 1px solid #999999;padding: 5px 1px;">
	                	 						<?php print $this->custom->getLang(9);?>
	                	 					</div>
	                	 					<div style="width: 869px;float: left;">
		                	 					<div style="width: 100px;float: left;">
		                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print nl2br($this->custom->getLang(12));?></p>
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print nl2br($this->custom->getLang(27));?></p>
		                	 						</div>
		                	 					</div>
		                	 					<div style="width: 300px;float: left;">
		                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print $p->coursename;?></p>
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print $p->startdate;?></p>
		                	 						</div>
		                	 					</div>
		                	 					<div style="width: 229px;float: left;">
		                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print $this->custom->getLang(28);?></p>
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print $this->custom->getLang(29);?></p>
		                	 						</div>
		                	 					</div>
		                	 					<div style="width: 240px;float: left;">
		                	 						<div style="width: 100%;display: block;text-align:center;border-right: 0px solid #999999;">
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print $p->numberofcourses;?></p>
		                	 							<p style="border-bottom: 1px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print $p->instructionform;?></p>
		                	 						</div>
		                	 					</div>
	                	 					</div>
	                	 					<!-- <div style="clear: both;"></div> -->
	                	 					<div style="width: 769px;float: left;">
		                	 					<div style="width: 100px;float: left;">
		                	 						<div style="width: 100%;display: block;text-align:center;border-right: 1px solid #999999;">
		                	 							<p style="border-bottom: 0px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print nl2br($this->custom->getLang(30));?></p>
		                	 						</div>
		                	 					</div>
		                	 					<div style="width: 669px;float: left;">
	                	 							<p style="border-bottom: 0px solid #999999;margin:0;padding:5px 5px 0 5px;font-size: 12px;height: 30px;"><?php print $p->contracttype;?></p>
		                	 					</div>
	                	 					</div>
	                	 					<div style="clear: both;"></div>
                	 					</div>
	                	 				<div style="clear: both;"></div>
                	 					<div style="width: 126px;float: left;">
                	 						<div style="width: 100%;display: block;text-align:center;padding-top:5px;border-right: 1px solid #999999;border-bottom: 1px solid #999999;border-left: 1px solid #999999;height: 30px;">
                	 							<?php print $this->custom->getLang(11);?>
                	 						</div>
                	 						<div style="clear: both;"></div>
                	 					</div>
                	 					<div style="width: 770px;float: left;">
                	 						<div style="width: 100%;display: block;text-align:center;padding-top:5px;border-right: 1px solid #999999;border-bottom: 1px solid #999999;border-left: 0px solid #999999;height: 30px;">
                	 							<?php print $this->custom->getLang(31);?>
                	 						</div>
                	 						<div style="clear: both;"></div>
                	 					</div>
                	 				</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td>
                	 		<?php 
								if($enrollment->num_rows() > 0){
									$enroll = $enrollment->row();
							?>					
												<table border="0">
													<tr>
														<td style="width: 125px;border-collapse: collapse;margin:0px;padding: 0;">
															<div style="text-align:center;border-left:1px solid #666666;border-bottom:1px solid #666666;border-top:1px solid #666666;padding: 50px 0;">
																<?php print $this->custom->getLang(32);?>
															</div>
														</td>
														<td style="border-collapse: collapse;margin:0px;padding: 0;">
															<table border="1" style="width: 770px;border-collapse: collapse;">
																<tr>
																	<td style="text-align: center;"><?php print $this->custom->getLang(33);?></td>
																	<td style="text-align: center;"><?php print $this->custom->getLang(34);?></td>
																	<td style="text-align: center;"><?php print $this->custom->getLang(35);?></td>
																	<td style="text-align: center;"><?php print $this->custom->getLang(36);?></td>
																	<td style="text-align: center;"><?php print $this->custom->getLang(37);?></td>
																	<td style="text-align: center;"><?php print $this->custom->getLang(38);?></td>
																</tr>
																<tr>
																	<td style="padding-left: 10px;"><?php print $this->custom->getLang(39);?></td>
																	<td style="text-align: center;">
																		<?php print $enroll->enrollmentfee_listproce;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->enrollmentfee_discount;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->enrollmentfee_subtotal;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->enrollmentfee_saletax;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->enrollmentfee_meter;?>
																	</td>
																</tr>
																<tr>
																	<td style="padding-left: 10px;"><?php print $this->custom->getLang(40);?></td>
																	<td style="text-align: center;">
																		<?php print $enroll->tuitionfee_listproce;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->tuitionfee_discount;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->tuitionfee_subtotal;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->tuitionfee_saletax;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->tuitionfee_meter;?>
																	</td>
																</tr>
																<tr>
																	<td style="padding-left: 10px;"><?php print $this->custom->getLang(41);?></td>
																	<td style="text-align: center;">
																		<?php print $enroll->curriculumfee_listproce;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->curriculumfee_discount;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->curriculumfee_subtotal;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->curriculumfee_saletax;?>
																	</td>
																	<td style="text-align: center;">
																		<?php print $enroll->curriculumfee_meter;?>
																	</td>
																</tr>
																<tr>
																	<td colspan="4"></td>
																	<td align="center"><?php print $this->custom->getLang(42);?></td>
																	<td style="text-align: center;">
																		<?php print $enroll->curriculumfee_meter_total;?>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>	
							<?php } ?>	
                	 				</td>
                	 			</tr>
                	 		</table>

	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td>
                	 		<?php 
								if($plan->num_rows() > 0){
									$p = $plan->result();
							?>					
												<table>
													<tr>
														<td style="width: 125px;border-collapse: collapse;margin:0px;padding: 0;">
															<div style="text-align:center;border-left:1px solid #666666;border-bottom:1px solid #666666;border-top:1px solid #666666;padding: 74px 0;">
																<?php print $this->custom->getLang(43);?>
															</div>
														</td>
														<td style="border-collapse: collapse;margin:0px;padding: 0;">
															<table border="1" style="width: 395px;border-collapse: collapse;">
																<tr>
																	<td style="width: 100px;text-align:center;"><?php print $this->custom->getLang(44);?></td>
																	<td style="width: 100px;text-align:center;"><?php print $this->custom->getLang(45);?></td>
																	<td style="width: 100px;text-align:center;"><?php print $this->custom->getLang(46);?></td>
																	<!-- <td colspan="4"><?php print $this->custom->getLang(47);?></td> -->
																</tr>
																<tr>
																	<td style="text-align:center;"><?php print $p[0]->paymentplan_date;?></td>
																	<td style="text-align:center;">
																		<?php print $p[0]->paymentplan_method;?>
																	</td>
																	<td style="text-align:center;">
																		<?php print $p[0]->paymentplan_payment;?>
																	</td>
																</tr>
																<tr>
																	<td style="text-align:center;"><?php print $p[1]->paymentplan_date;?></td>
																	<td style="text-align:center;">
																		<?php print $p[1]->paymentplan_method;?>
																	</td>
																	<td style="text-align:center;">
																		<?php print $p[1]->paymentplan_payment;?>
																	</td>
																</tr>
																<tr>
																	<td style="text-align:center;"><?php print $p[2]->paymentplan_date;?></td>
																	<td style="text-align:center;"><?php print $p[2]->paymentplan_method;?></td>
																	<td style="text-align:center;"><?php print $p[2]->paymentplan_payment;?></td>
																	<!-- <td colspan="4"><?php print $this->custom->getLang(48);?></td> -->
																</tr>
																<tr>
																	<td style="text-align:center;"><?php print $p[3]->paymentplan_date;?></td>
																	<td style="text-align:center;"><?php print $p[3]->paymentplan_method;?></td>
																	<td style="text-align:center;"><?php print $p[3]->paymentplan_payment;?></td>
																	<!-- <td colspan="4"><?php print $p[3]->paymentplan_datepicker;?></td> -->
																</tr>
																<tr>
																	<td style="text-align:center;"><?php print $p[4]->paymentplan_date;?></td>
																	<td style="text-align:center;"><?php print $p[4]->paymentplan_method;?></td>
																	<td style="text-align:center;"><?php print $p[4]->paymentplan_payment;?></td>
																</tr>
																<tr>
																	<td style="text-align:center;"><?php print $p[5]->paymentplan_date;?></td>
																	<td style="text-align:center;"><?php print $p[5]->paymentplan_method;?></td>
																	<td style="text-align:center;"><?php print $p[5]->paymentplan_payment;?></td>
																</tr>

															</table>
														</td>
														<td style="border-collapse: collapse;margin:0px;padding: 0;">
															<div style="width: 100%;border-right: 1px solid #666666;border-top: 1px solid #666666;border-bottom: 1px solid #666666;">
																<p style="margin:0;padding: 5px;border-bottom: 1px solid #666666;height: 73px;"><?php print $this->custom->getLang(47);?></p>
																<p style="margin:0;padding: 0px;padding-top: 2px;border-bottom: 1px solid #666666;text-align: center;height: 24px;"><?php print $this->custom->getLang(48);?></p>
																<p style="margin:0;padding: 0px;padding-top: 2px;border-bottom: 1px solid #666666;text-align: center;height: 24px;"><?php print $p[3]->paymentplan_datepicker;?></p>
																<p style="margin:0;padding: 2px;border-bottom: 1px solid #666666;text-align: center;height: 25px;"></p>
																<p style="margin:0;padding: 2px;border-bottom: 0px solid #666666;text-align: center;height: 24px;"><?php print $this->custom->getLang(75);?></p>
															</div>
														</td>
													</tr>
												</table>	
							<?php } ?>	
                	 				</td>
                	 			</tr>
                	 		</table>

	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td style="text-align: left;">【個人情報の取り扱いについて】</td>
                	 				<td style="text-align: right;">株式会社フェニックス　個人情報保護管理者</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td style="text-align: left;">お客様からお預かりする個人情報は、お問い合わせへの対応・連絡、お申し込み頂いた商品・サービスの提供、当社の商品・サービスの案内、アンケート・調査、統計・マーケティング資料作成、研究・企画開発に利用させて頂き、法令に基づく場合を除き、ご本人の同意を得ることなく他に利用または提供することはありません。個人情報の取り扱いの全部又は一部を委託する場合は、当社の厳正な管理の下で行います。必須項目にご記入・ご入力いただけない場合は、お問い合わせへの対応・お申し込みが行えない場合があります。ご本人の個人情報について、利用目的の通知、開示、内容で訂正、追加又は削除、利用の停止、消去および第三者への提供の停止のご希望がございましたら、以下までご連絡ください。
                	 				<br/>TEL：　03-6635-3498　　受付時間：　平日　12：00～21：00　土日祝　10：00～19：00（年末年始を除く）</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td style="text-align: center;">◆ 契約についてのご確認 ◆</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td style="text-align: right;">上記の申込につき同意します。</td>
                	 			</tr>
                	 			<tr>
                	 				<td style="text-align: right;">代表保護者署名欄</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td style="text-align: left;">上記のお申込を確かに承諾したしました。</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="45%" border="1" style="margin:25px 0;border-collapse: collapse;float:left;">
                	 			<tr>
                	 				<td style="text-align: left;font-size: 12px;text-align:center;">会社名</td>
                	 				<td style="text-align: left;font-size: 12px;text-align:center;">株式会社フェニックス</td>
                	 			</tr>
                	 			<tr>
                	 				<td style="text-align: left;font-size: 12px;text-align:center;">所在地</td>
                	 				<td style="text-align: left;font-size: 12px;">
										<table style="width:100%;">
											<tr>
												<td style="border-bottom:1px solid #666666;padding-left:5px;">〒150-0001</td>
											</tr>
											<tr>
												<td style="border-bottom:1px solid #666666;padding-left:5px;">東京都渋谷区神宮前1-5-8</td>
											</tr>
											<tr>
												<td style="padding-left:5px;">神宮前タワービルディング14F</td>
											</tr>
										</table>
									</td>
                	 			</tr>
                	 			<tr>
                	 				<td style="text-align: left;font-size: 12px;text-align:center;">電話番号</td>
                	 				<td style="text-align: left;font-size: 12px;">
										<table style="width:100%;">
											<tr>
												<td style="padding-left:5px;">03-6635-3498</td>
											</tr>
										</table>
									</td>
                	 			</tr>
                	 		</table>
							
                	 		<table width="45%" border="1" style="margin:25px 0;border-collapse: collapse;float:right;">
                	 			<tr>
                	 				<td style="text-align: left;text-align:center;">校舎名</td>
                	 				<td style="text-align: left;text-align:center;">コロンビア　アカデミー原宿校</td>
                	 			</tr>
                	 			<tr>
                	 				<td style="text-align: left;text-align:center;">校舎所在地</td>
                	 				<td style="text-align: left;">
										<table style="width:100%;">
											<tr>
												<td style="border-bottom:1px solid #666666;padding-left:5px;">〒150-0001</td>
											</tr>
											<tr>
												<td style="border-bottom:1px solid #666666;padding-left:5px;">東京都渋谷区神宮前1-5-8</td>
											</tr>
											<tr>
												<td style="padding-left:5px;">神宮前タワービルディング14F</td>
											</tr>
										</table>
									</td>
                	 			</tr>
                	 			<tr>
                	 				<td style="text-align: left;text-align:center;">電話番号</td>
                	 				<td style="text-align: left;">
										<table style="width:100%;">
											<tr>
												<td style="padding-left:5px;">03-6635-3498</td>
											</tr>
										</table>
									</td>
                	 			</tr>
                	 			<tr>
                	 				<td style="text-align: left;font-size: 12px;text-align:center;">契約担当者名</td>
                	 				<td style="text-align: left;font-size: 12px;">
										<table style="width:100%;">
											<tr>
												<td style="padding-left:5px;"></td>
											</tr>
										</table>
									</td>
                	 			</tr>
                	 		</table>

	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td style="text-align: left;">この受講申込契約書は、受講約款・規約などの書類と同様に、特定商取引に関する法律を始めとする関連法令に則り作成されています。</td>
                	 			</tr>
                	 		</table>
	                		<div style="clear: both;"></div>
                	 		<table width="100%" border="0" style="margin:25px 0;border-collapse: collapse;">
                	 			<tr>
                	 				<td style="text-align: right;">校舎控・本人控　　共通</td>
                	 			</tr>
                	 		</table>

                	 	</td>
                	 </tr>
                </table>
        <?php } ?>
    </div>
</div>

</div>
<style type="text/css">
	table {
	  border-collapse: collapse;
	}
</style>			

                        


						
			
<?php $this->load->view('admin/footer');?>	