<?php $this->load->view('admin/header');?>
<?php 
	if($student->num_rows() > 0){
		$row = $student->row();
?>
	<div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Dashboard</h6>
                                    <div class="element-content">
                                        <div class="row">
                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Total Classes</div>
                                                    <div class="value"><?php print $row->total_class;?></div>
                                                    <!-- <div class="trending trending-up-basic"><span>12%</span><i class="os-icon os-icon-arrow-up2"></i></div> -->
                                                </a>
                                            </div>
                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Total Attend Classes</div>
                                                    <div class="value"><?php print $this->students_model->getTotalAttendClass($row->id);?></div>
                                                    <!-- <div class="trending trending-down-basic"><span>12%</span><i class="os-icon os-icon-arrow-down"></i></div> -->
                                                </a>
                                            </div>
                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Feedback</div>
                                                    <div class="value"><?php print $this->students_model->getTotalFeedClass($row->id);?></div>
                                                    <!-- <div class="trending trending-down-basic"><span>9%</span><i class="os-icon os-icon-arrow-down"></i></div> -->
                                                </a>
                                            </div>
                                            <div class="d-none d-xxxl-block col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Refunds Processed</div>
                                                    <div class="value">$294</div>
                                                    <div class="trending trending-up-basic"><span>12%</span><i class="os-icon os-icon-arrow-up2"></i></div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<?php
	}
?>
<?php $this->load->view('admin/footer');?>