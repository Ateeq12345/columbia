<?php $this->load->view('admin/header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="element-wrapper">
            <h6 class="element-header">Booking Details</h6>
            <?php print flash_message(); ?>
            <div class="table-responsive">
                <?php if ($booking_details->num_rows() > 0) { ?>
                    <table class="table table-bordered table-lg table-v2 table-striped">
                        <thead>
                            <tr>
                                <th>Sr #</th>
                                <th>Subject</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Students</th>
                                <th>Slot Status</th>
                                <th>Cancel Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 1;
                            foreach ($booking_details->result() as $row) {
                            ?>
                                <tr>
                                    <td><?php echo $counter++;?></td>
                                    <td><?php print $this->subjects_model->getNameById($row->subject_id); ?></td>
                                    <td><?php print $row->slotdate; ?></td>
                                    <td><?php print date('h:i A', strtotime($row->timefrom)); ?> / <?php print date('h:i A', strtotime($row->timeto)); ?></td>
                                    <td><?php print $this->custom->getStudentCountByTeacher($row->timeslot); ?></td>
                                    <td><?php
                                        if ($row->slotstatus == 1) {
                                            print 'Next Session';
                                        } elseif ($row->slotstatus == 2) {
                                            print 'Session Closed';
                                        } else {
                                        }
                                        ?></td>
                                    <td style="text-align: center;"><?php if ($row->is_canceled == 1) {
                                            print '<span class="mr-2 mb-2 btn btn-danger">Canceled</span>';
                                        } else {
                                            print '<span class="mr-2 mb-2 btn btn-success">Active</span>';
                                        } ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <div class="alert alert-danger">No result found.</div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('admin/footer'); ?>