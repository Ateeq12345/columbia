<?php $this->load->view('admin/header');?>
<div class="row">
   <div class="col-sm-12">
      <div class="element-wrapper">
         <h6 class="element-header">Admin Student</h6>
         <?php print flash_message();?>
         <div class="row">
            <div class="col-sm-12">
               <div class="element-box">
                  <form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/insert';?>" id="formValidate" enctype="multipart/form-data">
                     <div class="form-group">
                        <label for=""> First name</label>
                        <input type="text" name="fname" class="form-control" data-error="Please enter Firstname." placeholder="Enter Firstname" required="required">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Lastname</label>
                        <input type="text" name="lname" class="form-control" data-error="Please enter Lastname." placeholder="Enter Lastname" required="required">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Address</label>
                        <input type="text" name="address" class="form-control" data-error="Please enter Address." placeholder="Enter Address" required="required">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Phone</label>
                        <input type="tel" name="phone" class="form-control" data-error="Please enter Phone." placeholder="Enter Phone" required="required">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Home Phone</label>
                        <input type="tel" name="homephone" class="form-control" placeholder="Enter Home Phone"/>
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Counsellor</label>
                        <input type="text" name="counsellor" class="form-control" placeholder="Enter Counsellor"/>
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Subject</label>
                        <select name="subject_id" class="form-control">
                           <?php 
                              if($subject->num_rows() > 0){
                              	foreach($subject->result() as $sub){
                              ?>
                           <option value="<?php print $sub->id;?>"><?php print $sub->name;?></option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for=""> Student Picture</label>
                        <input type="file" name="fimg" class="form-control"/>
                     </div>
                     <div class="form-group">
                        <label for=""> Student Info</label>
                        <input type="text" name="student_info" class="form-control" placeholder="Student Info" >
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for="">Select Demo Date</label>
                        <input data-error="Please enter Demo Date." type="text" name="demo_date" class="form-control datepicker" placeholder="Please Enter Demo Date" autocomplete="off">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for="">Select Demo Time</label>
                        <input  type="text" name="demo_time" class="time_element form-control" placeholder="Please Enter Demo Time" autocomplete="off"/>
                        <!-- <input data-error="Please enter Demo Date." type="text" name="demo_date" class="form-control datepicker" placeholder="Please Enter Demo Date" autocomplete="off">
                        <div class="help-block form-text with-errors form-control-feedback"></div> -->
                     </div>
                     <div class="form-group">
                        <label for="">Free Classes</label>
                        <input type="text" name="total_free" class="form-control" placeholder="Free Classes" >
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Total Classes</label>
                        <input type="text" name="total_class" class="form-control" placeholder="Total Classes" >
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for="">Joining Date</label>
                        <input data-error="Please enter Joining Date." type="text" name="joining_date" class="form-control datepicker" placeholder="Please Enter Joining Date" autocomplete="off">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for="">Contract Expiry Date</label>
                        <input data-error="Please enter Contract Expiry Date." type="text" name="contract_expiry_date" class="form-control datepicker" placeholder="Please Enter Contract Expiry Date" autocomplete="off">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Email address</label>
                        <input type="email" name="email" class="form-control" data-error="Your email address is invalid" placeholder="Enter email" required="required">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                     </div>
                     <div class="form-group">
                        <label for=""> Password</label>
                        <input type="password" name="password" class="form-control" data-minlength="6" placeholder="Password" required="required">
                        <div class="help-block form-text text-muted form-control-feedback">Minimum of 6 characters</div>
                     </div>
                     <div class="form-group">
                        <label for=""> Admission</label>
                        <select name="admission" class="form-control">
                           <option value="1">Trial Class</option>
                           <option value="2">Confirm Admission</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for=""> Student Comments</label>
                        <textarea name="student_comments" class="form-control tinymce"></textarea>
                     </div>
                     <div class="form-group">
                        <label for=""> Source</label>
                        <div class="box-bordered form-group mt_5 custom-input-check source-bar">
                           <div>	
                              <input id="1" name="source" value="google" type="radio">
                              <label class="no-margin" for="1">
                              <b>Google </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="2" name="source" value="billboard" type="radio">
                              <label class="no-margin" for="2">
                              <b>BillBoard </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="3" name="source" value="walkin" type="radio">
                              <label class="no-margin" for="3">
                              <b>Walk-in </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="4" name="source" value="student_introduction" type="radio">
                              <label class="no-margin" for="4">
                              <b>Student Introduction </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="5" name="source" value="counsellor" type="radio">
                              <label class="no-margin" for="5">
                              <b>Counsellor </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="6" name="source" value="pamphlet" type="radio">
                              <label class="no-margin" for="6">
                              <b>Pamphlet </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="7" name="source" value="others" type="radio">
                              <label class="no-margin" for="7">
                              <b>Others </b><br>
                              </label>
                           </div>
                        </div>
                     </div>
                      <div class="form-group">
                        <label for=""> Add Levels of Sudent</label>
                        <div class="box-bordered form-group mt_5 custom-input-check source-bar">
                           <div> 
                              <input id="8" name="level" value="level_1" type="radio">
                              <label class="no-margin" for="8">
                              <b>Level 1 </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="9" name="level" value="level_2" type="radio">
                              <label class="no-margin" for="9">
                              <b>Level 2 </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="10" name="level" value="level_3" type="radio">
                              <label class="no-margin" for="10">
                              <b>Level 3 </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="11" name="level" value="level_4" type="radio">
                              <label class="no-margin" for="11">
                              <b>Level 4 </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="12" name="level" value="be_ml_elementary" type="radio">
                              <label class="no-margin" for="12">
                              <b>BE. ML Elementary </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="13" name="level" value="be_pre_inter" type="radio">
                              <label class="no-margin" for="13">
                              <b>BE. Pre Inter </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="14" name="level" value="be_inter" type="radio">
                              <label class="no-margin" for="14">
                              <b>BE. Inter </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="15" name="level" value="be_upper_inter" type="radio">
                              <label class="no-margin" for="15">
                              <b>BE. Upper Inter </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="16" name="level" value="be_advance" type="radio">
                              <label class="no-margin" for="16">
                              <b>BE. Advance </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="17" name="level" value="apex_level" type="radio">
                              <label class="no-margin" for="17">
                              <b>Apex Level </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="18" name="level" value="toeic" type="radio">
                              <label class="no-margin" for="18">
                              <b>TOEIC </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="19" name="level" value="ielts" type="radio">
                              <label class="no-margin" for="19">
                              <b>IELTS </b><br>
                              </label>
                           </div>
                           <div>
                              <input id="20" name="level" value="toefl" type="radio">
                              <label class="no-margin" for="20">
                              <b>TOEFL  </b><br>
                              </label>
                           </div>
                           
                        </div>
                     </div>
                     <!--													<div class="form-group">-->
                     <!--														<label for=""> Student Photo</label>-->
                     <!--														<input type="file" name="fimg" value="" class="form-control">-->
                     <!--													</div>-->
                     <div class="form-group">
                        <label for=""> Status</label>
                        <select name="active" class="form-control">
                           <option value="1">Active</option>
                           <option value="0">Block</option>
                        </select>
                     </div>
                     <div class="form-buttons-w">
                        <button class="btn btn-primary" type="submit"> Submit</button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('admin/footer');?>
<script type="text/javascript">
 $(document).ready(function(){
    $(".time_element").timepicki();
  });
</script>
