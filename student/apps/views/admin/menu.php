			<div class="menu-mobile menu-activated-on-click color-scheme-dark">
                <div class="mm-logo-buttons-w">
                    <a class="mm-logo" href="<?php print base_url();?>"><img src="<?php print base_url();?>admin_theme/img/logo.png"><span> Columbia Academy</span></a>
                    <div class="mm-buttons">
                       <!--  <div class="content-panel-open">
                            <div class="os-icon os-icon-grid-circles"></div>
                        </div> -->
                        <div class="mobile-menu-trigger">
                            <div class="os-icon os-icon-hamburger-menu-1"></div>
                        </div>
                    </div>
                </div>
                <div class="menu-and-user">
                    <ul class="main-menu">

                        <!-- <li class="sub-header"><span>CRM</span></li> -->
                <?php if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){ ?>        
                        <li class=" has-sub-menu">
                            <a href="javascript:void(0);">
                                <div class="icon-w">
                                    <div class="os-icon os-icon-user-male-circle2"></div>
                                </div>
                                 <div class="sub-menu-header">Students</div>
                            </a>
                            <div class="sub-menu-w">
                                <div class="sub-menu-i">
                                    <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'students';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'students/add';?>">Add Student</a></li>
                <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('teachers',$this->custom->getUserProfile())== 1){ ?>  
                        <li class=" has-sub-menu">
                            <a href="javascript:void(0);">
                                <div class="icon-w">
                                    <div class="os-icon os-icon-user-male-circle2"></div>
                                </div>
                              <div class="sub-menu-header">Teachers</div></a>
                            <div class="sub-menu-w">
                                <div class="sub-menu-i">
                                    <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('teachers',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'teachers';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getViewPerm('teachers',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'teachers/add';?>">Add Teacher</a></li>
                <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){ ?> 
                        <li class=" has-sub-menu">
                            <a href="javascript:void(0);">
                                <div class="icon-w">
                                    <div class="os-icon os-icon-package"></div>
                                </div>
                                <div class="sub-menu-header">Subjects</div>
                            </a>
                            <div class="sub-menu-w">
                                <div class="sub-menu-i">
                                    <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('subjects',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'subjects';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getViewPerm('subjects',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'subjects/add';?>">Add Subject</a></li>
                <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('class_schdule',$this->custom->getUserProfile())== 1){ ?> 
                        <li class=" has-sub-menu">
                            <a href="javascript:void(0);">
                                <div class="icon-w">
                                    <div class="os-icon os-icon-edit-1"></div>
                                </div>
     <div class="sub-menu-header">Class Schedule</div>
                            </a>
                            <div class="sub-menu-w">
                                <div class="sub-menu-i">
                                    <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('class_schdule',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'schdule';?>">View List</a></li>
                <?php } ?>
                                        <!-- <li><a href="<?php print base_url().'schdule/add';?>">Add Schdule</a></li> -->
                                    </ul>
                                </div>
                            </div>
                        </li>
                <?php } ?>
                 <?php if($this->permission_model->getAllPerm('time_slot_booking',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-edit-1"></div>
                            </div>
                            <div class="sub-menu-header">Time Slot Booking</div>
                        </a>
                        <div class="sub-menu-w">
                            
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('time_slot_booking',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'booking';?>">View Slot</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li> 
                <?php } ?>    
                      <!--   <li class="sub-header"><span>Admin</span></li> -->
                <?php if($this->permission_model->getAllPerm('profile',$this->custom->getUserProfile())== 1){ ?> 
                        
                        <li class=" has-sub-menu">
                            <a href="javascript:void(0);">
                                <div class="icon-w">
                                    <div class="os-icon os-icon-users"></div>
                                </div>
                              
                                <div class="sub-menu-header">Profile</div>
                            </a>
                            <div class="sub-menu-w">
                                <div class="sub-menu-i">
                                    <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('profile',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'profile';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getViewPerm('profile',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'profile/add';?>">Add Subject</a></li>
                <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('adminuser',$this->custom->getUserProfile())== 1){ ?> 
                        <li class=" has-sub-menu">
                            <a href="javascript:void(0);">
                                <div class="icon-w">
                                    <div class="os-icon os-icon-users"></div>
                                </div>
                             
                                 <div class="sub-menu-header">Users</div>
                            </a>
                            <div class="sub-menu-w">
                                <div class="sub-menu-i">
                                    <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('adminuser',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'users';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getViewPerm('adminuser',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'users/add';?>">Add Subject</a></li>
                <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                <?php } ?>
                        <!-- <li>
                            <a href="<?php print base_url().'language';?>">
                                <div class="icon-w">
                                    <div class="fa fa-pencil"></div>
                                </div>
                                <span>Language</span>
                            </a>
                        </li> -->
                      <!--   <li>
                            <a href="<?php print base_url().'logout';?>">
                                <div class="icon-w">
                                    <div class="fa fa-power-off"></div>
                                </div>
                                <span>Logout</span>
                            </a>
                        </li> -->
                    </ul>
                    
                    
                </div>
            </div>
			
            <div class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
                <div class="logo-w">
                    <a class="logo" href="<?php print base_url();?>">
                       <!-- <div class="logo-element"></div> -->
                        <div>
                           <img src="<?php print base_url();?>admin_theme/img/logo.png" alt="">
                        </div>
                        <div class="logo-label">Columbia Academy</div>
                    </a>
                </div>
                <!-- <div class="logged-user-w avatar-inline">
                    <div class="logged-user-i">
                        <div class="logged-user-info-w">
                            <div class="logged-user-name"><?php print $this->custom->getAdminUser();?></div>
                            <div class="logged-user-role">Administrator</div>
                        </div>
                        <div class="logged-user-toggler-arrow">
                            <div class="os-icon os-icon-chevron-down"></div>
                        </div>
                        <div class="logged-user-menu color-style-bright">
                            <ul>
                                <li><a href="#"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <div class="logged-user-w">
                    <!-- <form id="langform" action="<?php print base_url().'language/lang';?>"> -->
                        <div class="form-group mb-0">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="form-check-label langselection" style="width:100px;">
                                        <input <?php if($this->session->userdata('lang') == 'eng'){?> checked<?php }?> class="form-check-input" name="lang" type="radio" value="eng"><img src="<?php print base_url();?>admin_theme/img/usa.png" alt="">
                                    </label>
                                    <label class="form-check-label langselection">
                                        <input<?php if($this->session->userdata('lang') == 'jap'){?> checked<?php }?> class="form-check-input" name="lang" type="radio" value="jap"><img src="<?php print base_url();?>admin_theme/img/japan.png" alt="">
                                    </label>
                                </div>
                            </div>
                        </div>
                    <!-- </form> -->
                </div>
                <h1 class="menu-page-header">Page Header</h1>
                <ul class="main-menu">
					
                    <li class="sub-header"><span>MENU</span></li>
                <?php if($this->permission_model->getAllPerm('school',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="fa fa-building-o"></div>
                            </div><span>Schools</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Schools</div>
                            <div class="sub-menu-icon"><i class="fa fa-building-o"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('school',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'school';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getAddPerm('school',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'school/add';?>">Add School</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user-male-circle2"></div>
                            </div><span>Students</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Students</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-user-male-circle2"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'students';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'students/add';?>">Add Student</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user-male-circle2"></div>
                            </div><span>Students Request <span class="badge bg-primary"><?php print $this->custom->get_StudentRequests(); ?></span></span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Students Request</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-user-male-circle2"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'students_request';?>">View List</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user-male-circle2"></div>
                            </div><span>Confirmed Students</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Confirmed Students</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-user-male-circle2"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'students/confirmedStudents';?>">View List</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('teachers',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-user-male-circle2"></div>
                            </div><span>Teachers</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Teachers</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-user-male-circle2"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('teachers',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'teachers';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getAddPerm('teachers',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'teachers/add';?>">Add Teacher</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-package"></div>
                            </div><span>Subjects</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Subjects</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-package"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('subjects',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'subjects';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getAddPerm('subjects',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'subjects/add';?>">Add Subject</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('class_schdule',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-edit-1"></div>
                            </div><span>Class Schedule</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Class Schdule</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-edit-1"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('class_schdule',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'schdule';?>">View List</a></li>
                <?php } ?>
                                    <!-- <li><a href="<?php print base_url().'schdule/add';?>">Add Schdule</a></li> -->
                                </ul>
                            </div>
                        </div>
                    </li> 
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('time_slot_booking',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-edit-1"></div>
                            </div><span>Time Slot Booking</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Time Slot Booking</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-edit-1"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('time_slot_booking',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'booking';?>">View Slot</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li> 
                <?php } ?> 
                
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-edit-1"></div>
                            </div><span>Annoucements</span></a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Annoucements</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-edit-1"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getAllPerm('adminuser',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'annoucements';?>">Annoucements</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li> 
         

                    <li class="sub-header"><span>Admin</span></li>
                <?php if($this->permission_model->getAllPerm('profile',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-users"></div>
                            </div>
							<span>Profile</span>
						</a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Profile</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-users"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('profile',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'profile';?>">View Profiles</a></li>
                <?php } ?>
                <?php if($this->permission_model->getAddPerm('profile',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'profile/add';?>">Add Profile</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                <?php if($this->permission_model->getAllPerm('adminuser',$this->custom->getUserProfile())== 1){ ?>
                    <li class=" has-sub-menu">
                        <a href="javascript:void(0);">
                            <div class="icon-w">
                                <div class="os-icon os-icon-users"></div>
                            </div>
                            <span>Users</span>
                        </a>
                        <div class="sub-menu-w">
                            <div class="sub-menu-header">Users</div>
                            <div class="sub-menu-icon"><i class="os-icon os-icon-users"></i></div>
                            <div class="sub-menu-i">
                                <ul class="sub-menu">
                <?php if($this->permission_model->getViewPerm('adminuser',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'users';?>">View List</a></li>
                <?php } ?>
                <?php if($this->permission_model->getAddPerm('adminuser',$this->custom->getUserProfile())== 1){ ?>        
                                        <li><a href="<?php print base_url().'users/add';?>">Add User</a></li>
                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                <?php } ?>

                    <!-- <li>
                            <a href="<?php print base_url().'language';?>">
                                <div class="icon-w">
                                    <div class="fa fa-pencil"></div>
                                </div>
                                <span>Language</span>
                            </a>
                    </li> -->
                   <!--  <li>
                        <a href="<?php print base_url().'logout';?>">
                            <div class="icon-w">
                                <div class="fa fa-power-off"></div>
                            </div>
                            <span>Logout</span>
                        </a>
                    </li> -->
                </ul>
            
			</div>

