<?php $this->load->view('admin/header');?>
		
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin Subject</h6>
									<?php print flash_message();?>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/insert';?>" id="formValidate" enctype="multipart/form-data">
													<div class="form-group">
														<label for=""> Subject</label>
														<select name="subject_id" class="form-control">
															<?php 
																if($subjects->num_rows() > 0){
																	foreach($subjects->result() as $srow){
															?>
															<option value="<?php print $srow->id;?>"><?php print ucfirst($srow->name);?></option>
															<?php
																	}
																}
															?>
														</select>
													</div>
													<div class="form-group">
														<label for=""> Teacher</label>
														<select name="teacher_id" class="form-control js-data-example-ajax">
															<!-- <?php 
																if($teacher->num_rows() > 0){
																	foreach($teacher->result() as $srow){
															?>
															<option value="<?php print $srow->id;?>"><?php print ucfirst($srow->fname . ' ' . $srow->lname);?></option>
															<?php
																	}
																}
															?> -->
														</select>
													</div>
													<button type="button" id="addItem" class="btn btn-info btn-xs">Add Date & Time</button>
													<div class="row">
														<div class="col-sm-3">
															<div class="form-group">
																<label for=""> Class Date</label>
																<div class="date-input">
																	<input autocomplete="off" class="datepicker form-control" placeholder="Class Date" type="text" value="" name="schdule_date[]">
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group">
																<label for=""> Class Time From</label>
																<div class="date-input">
																	<input class="form-control timepicker1" type="text" name="timepickerfrom[]"/>
																</div>
															</div>
														</div>
														<div class="col-sm-3">
															<div class="form-group">
																<label for=""> Class Time To</label>
																<div class="date-input">
																	<input class="form-control timepicker1" type="text" name="timepickerto[]"/>
																</div>
															</div>
														</div>
														<div class="col-sm-2">
															<div class="form-group">
																<label for="">No of Student</label>
																<input type="tel" name="noofstudent[]" class="form-control" value="" />
															</div>
														</div>
													</div>
													<div id="datetimerow">
													
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
											
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	