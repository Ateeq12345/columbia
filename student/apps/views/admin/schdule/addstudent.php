<?php $this->load->view('admin/header');?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#student_id" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php print base_url().$this->uri->segment(1).'/searchstudent';?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      	select: function( event, ui ) {
      		// Set selection
		   $('#student_id').val(ui.item.label); // display the selected text
		   $('#selectuser_id').val(ui.item.value); // save selected id to input
		   return false;
      	}
    } );

    $('.studentAdd').submit(function(){
    	$('#loading').show();
    	var studentphone = $('#student_id').val();
    	$.ajax({
    		type: 'POST',
    		data: {student: studentphone},
    		dataType: 'text',
    		url: '<?php print base_url().$this->uri->segment(1).'/searchstudentnew';?>',
    		success: function(resp){
    			var r = JSON.parse(resp);
    			$.each(r, function(key, value) {
    				// alert(value.label);
    				var htmls = '<div class="row">\
    								<div class="col-sm-5">'+value.label+'</div>\
    								<div class="col-sm-5">'+value.phone+'</div>\
    								<div class="col-sm-2"><button type="button" id="confirmstudetn" class="btn btn-success">Confirm</button></div>\
    							</div>';
    				$('.resultstudent').html(htmls);
    			});
    			// alert(r);
    			$('#loading').hide();
    		}
    	});
    	return false;
    });

  });
  </script>		
    <div class="row">
        <div class="col-sm-12">
            <div class="element-wrapper">
                <h6 class="element-header">Admin Subject</h6>
				<?php print flash_message();?>
				<div class="row">
					<div class="col-sm-12">
						<div class="element-box">
							<form method="post" name="postform" action="#" class="studentAdd" id="formValidate" enctype="multipart/form-data">
								<div class="row">
									<div class="col-sm-9">
										<div class="form-group">
											<div class="date-inputss ui-widget">
												<input class="form-control" placeholder="Search Student" id="student_id" type="text" value="" name="schdule_date[]">
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<button class="btn btn-primary" type="submit">Add Student</button>
									</div>
								</div>
							</form>
						</div>
					</div>
                </div>
                <div class="row">
					<div class="col-sm-12">
						<div class="element-box resultstudent">
							<form method="post" name="postform" action="#" class="confirmstudentAdd" id="formValidate">
						<input type="hidden" name="schdule_id" value="<?php print $this->uri->segment(3);?>" />
						<input type="hidden" name="slotdate" value="<?php print $this->input->get('slotdate');?>" />
                        <input type="hidden" name="slotid" value="<?php print $this->input->get('slotid');?>" />
                        <input type="hidden" name="slottype" value="<?php print $this->input->get('type');?>" />
								<div class="row">
									<div class="col-sm-12" id="poststudent"></div>
								</div>
							</form>
						</div>
					</div>
                </div>
			</div>
		</div>
	</div>
			
<?php $this->load->view('admin/footer');?>	