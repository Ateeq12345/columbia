<?php $this->load->view('admin/header');?>
<style type="text/css">
    .student_block{
        width: auto;
        display: inline-block;
        /*padding: 5px 10px;*/
        background: #bfd6f9;
    }
    .student_block span{
        display: inline-block;
        padding: 5px 10px;
    }
    .student_del{
        cursor: pointer;
        background: #e65252;
        color: #FFFFFF;
        font-size: 10px;
        /*padding: 7px 10px;*/
    }
    .cancel_del{
        cursor: pointer;
        background: #34a853;
        color: #FFFFFF;
        font-size: 10px;
        /*padding: 7px 10px;*/
    }
    .edit-btn{
        cursor: pointer;
        background: #047bf8;
        color: #FFFFFF;
        font-size: 10px;
        padding: 5px 10px;
        border: none;
    }
</style>			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Class Schdule</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-4">
        <?php if(   $this->permission_model->getAddPerm('class_schdule',$this->custom->getUserProfile())== 1 ) { ?>   
												<div class="btn btn-sm btn-success" id="add_class">Add Class</div>
        <?php }?>
                                                <!-- <a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add Class</a> -->
											</div>
                                            <div class="col-sm-4">
                                                Today <?php print $date;?>
                                            </div>
                                            <div class="col-sm-4">
                                                <form class="form-inline justify-content-sm-end" action="<?php print base_url().$this->uri->segment(1);?>" method="get">
                                                    <input autocomplete="off" class="datepicker form-control form-control-sm rounded bright" placeholder="Search" name="d" type="text">
                                                    <button type="submit" class="btn btn-sm btn-default">Search</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
													<th>Class Time</th>
                                                    <th>Class Detail</th>
													<th>Class Student</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
												if($users->num_rows() > 0){
													foreach($users->result() as $row){
											?>    
												<tr>
													<td style="width: 120px;"><?php print $row->timepickerfrom;?></td>
                                                    <td style="width: 200px;"><span style="width:100%;display: block;margin-bottom: 5px;"><?php print $this->teachers_model->getNameById($row->teacher_id);?>      
                                                    <button data-toggle="modal" onclick="updateTeacher(<?php print $row->subject_id ?>,<?php print $row->id ?>,)" class="edit-btn pull-right">Edit</button></span>
                                                        <span>
                                                            <img src="<?php print $this->subjects_model->getFlagById($row->subject_id);?>" width="30">
                                                            <?php print $this->subjects_model->getNameById($row->subject_id);?>
                                                        </span>
                                                        <span><?php print $this->school_model->getSchoolById($row->school_id);?>
                                                            <button data-toggle="modal" onclick="updateSchool(<?php print $row->subject_id ?>,<?php print $row->id ?>,)" class="edit-btn pull-right">Edit</button></span>
                                                   
                                                    </td>
                                                    <td><?php 
                                                      $stu = $this->custom->getStudentCountByClass($row->date_id);
                                                      if($stu->num_rows() > 0){
                                                         foreach($stu->result() as $st){
                                                    ?>
                                                    <div class="student_block">
                                                        <span class="student_name">
                                                    <?php
                                                            print $this->students_model->getNameById($st->student_id);
                                                    ?>
                                                        </span>
                                                        <button class="edit-btn"  data-toggle="modal" onclick="studeUpdate(<?php print $st->booking_id ?>)" data-target="#editStudentModal">Edit</button>
                                                        <?php if($st->is_canceled == 1){?>
                                                        <span class="student_del" onclick="load_cancel_data(<?php print $st->timeslot;?>);">Canceled</span>

                                                        <?php } else { ?>
                                                        <span class="student_del" onclick="delete_student(<?php print $st->booking_id;?>);">Delete</span>
                                                        <span class="cancel_del" onclick="cancel_student(<?php print $st->booking_id;?>,<?php print $st->timeslot;?>);">Cancel</span>
                                                        
                                                        <?php } ?>
                                                    </div>
                                                    <?php     
                                                         }
                                                      }  
                                                    ?></td>
                                                    <td class="row-actions" style="width: 300px;">

                                                        <div class="btn btn-sm btn-success" onclick="addStudent(<?php print $row->id;?>,<?php print $row->subject_id;?>);">Add Student</div>

														<!-- <a href="<?php print base_url().$this->uri->segment(1).'/addstudent/'.$row->id.'?slotdate='.date('Y-m-d', strtotime($row->schdule_date)).'&slotid='.$row->date_id.'&type=booking';?>"><i class="os-icon os-icon-grid-10" title="Add Student"></i></a>
                                                        <a href="<?php print base_url().$this->uri->segment(1).'/edit/'.$row->id;?>"><i class="os-icon os-icon-ui-49"></i></a> -->

        <?php if(   $this->permission_model->getDelPerm('class_schdule',$this->custom->getUserProfile()) == 1 ) { ?>  
														<a class="btn btn-sm btn-danger" onclick="return confirm('Are your Sure to delete this?');" href="<?php print base_url().$this->uri->segment(1).'/delete/'.$row->id.'?date_id='.$row->date_id.'&schdule_id='.$row->schdule_id;?>"><i class="os-icon os-icon-ui-15"></i> Delete</a>
        <?php } ?>
													</td>
                                                </tr>
                                            <?php 
													}
												}
											?>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>

<div class="modal" id="classModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Class</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for=""> Subject</label>
                <select name="subject_id" id="subject_id" class="form-control" onchange="getTeacherList(this.value);">
                    <option value="0">Select Subject</option>
                    <?php 
                        if($subjects->num_rows() > 0){
                            foreach($subjects->result() as $srow){
                    ?>
                    <option value="<?php print $srow->id;?>" style="background-color: <?php print $srow->color_code;?>"><?php print ucfirst($srow->name);?></option>
                    <?php
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for=""> Teacher</label>
                <select name="teacher_id" id="teacher_id" class="form-control">
                    <!-- <?php 
                        if($teacher->num_rows() > 0){
                            foreach($teacher->result() as $srow){
                    ?>
                    <option value="<?php print $srow->id;?>"><?php print ucfirst($srow->fname . ' ' . $srow->lname);?></option>
                    <?php
                            }
                        }
                    ?> -->
                </select>
            </div>
            <div class="form-group">
                <label for=""> School</label>
                <select name="school_id" id="school_id" class="form-control">
                    <option value="0">Select School</option>
                    <?php 
                        if($school->num_rows() > 0){
                            foreach($school->result() as $srow){
                    ?>
                    <option value="<?php print $srow->id;?>"><?php print ucfirst($srow->school_name);?></option>
                    <?php
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for=""> Class Date</label>
                <div class="date-input">
                    <input id="schdule_date" autocomplete="off" class="datepicker form-control" placeholder="Class Date" type="text" value="" name="schdule_date">
                </div>
            </div>
            <div class="form-group">
                <label for=""> Class Time</label>
                <div class="date-input">
                    <input class="form-control timepicker1" type="text" id="timepickerfrom" name="timepickerfrom"/>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="newaddClass">Add</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>    

<div class="modal" id="studentModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Student</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label for=""> Search Student</label>
                <input type="text" id="student_id" name="student_id" class="form-controls" style="width: 500px;" />
                <input type="hidden" id="student_ids" name="student_ids" value="">
                <input type="hidden" id="class_ids" name="class_ids" value="">
                <input type="hidden" id="booking_subject_ids" name="booking_subject_ids" value="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="addClassStudent">Add</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>    

<div class="modal" id="loadCalcenModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form action="" method="">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Student</h5>
            <button type="button" class="close" data-dismiss="modal" onclick="$('#loadCalcenModal').hide();" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="loadClassDetail"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="$('#loadCalcenModal').hide();" data-dismiss="modal">Close</button>
          </div>
        </div>
    </form>
  </div>
</div> 

<!-- Edit Teacher Modal -->

<div class="modal fade custom-modal-desgin" id="editTeacherModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form method="post" action="<?php print base_url().'schdule/updateTeacher'?>">
        <div class="modal-content">
          <div class="modal-header">
              <input type="hidden" id="teacherScheduleClass" name="id">
              <input type="hidden" id="teacherScheduleSubject" name="subject_id">
            <h5 class="modal-title" id="exampleModalLabel">Change Teacher</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <label>Select Teacher</label>
           <select name="teacher_id" id="updateTeacher" class="form-control">
               <option>Select teacher</option>
           </select>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Save changes</button>
          </div>
        </div>
    </form>
  </div>
</div>  


<!-- Edit Student Modal -->

<div class="modal fade custom-modal-desgin" id="editStudentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form method="post" action="<?php print base_url().'schdule/studentUpdate'?>">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Change Student</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <?php
            $allStudents = $this->custom->getAllStudent()
            ?>
            <input type="hidden" name="booking_id" id="bookingId">
          <div class="modal-body">
           <label>Select Student</label>
           <select class="form-control" name="student_id">
               <option>Select student</option>
               <?php
               foreach ($allStudents as $stud) {
                   ?>
                   <option value="<?php echo  $stud['code'] ?>"><?php echo  $stud['name'] ?></option>
                   <?php
               }
               ?>
           </select>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Save changes</button>
          </div>
        </div>
    </form>
  </div>
</div>
    <div class="modal fade custom-modal-desgin" id="editSchoolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="<?php print base_url().'schdule/updateSchool'?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="hidden" id="schoolScheduleClass" name="id">
                        <input type="hidden" id="schoolScheduleSubject" name="subject_id">
                        <h5 class="modal-title" id="exampleModalLabel">Change School</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label>Select School</label>
                        <select name="school_id" id="schoolTeacher" class="form-control">
                            <option>Select teacher</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function updateSchool(ids,classId) {
            $('#schoolScheduleClass').val(classId);
            $('#schoolScheduleSubject').val(ids);
            $.get( "<?php print base_url().$this->uri->segment(1).'/getSchoolList';?>", { 'subject_id': ids}, function( data ) {
                $('#schoolTeacher').html(JSON.parse(data));
            });
            $('#editSchoolModal').modal('show')
        }
        function updateTeacher(ids,classId) {
        $('#teacherScheduleClass').val(classId);
        $('#teacherScheduleSubject').val(ids);
        $.get( "<?php print base_url().$this->uri->segment(1).'/getTeacherList';?>", { 'subject_id': ids}, function( data ) {
            $('#updateTeacher').html(JSON.parse(data));
        });
        $('#editTeacherModal').modal('show')
    }
    function studeUpdate(bookingId) {
        $('#bookingId').val(bookingId);
        $('#editStudentModal').modal('show')

    }
    function cancel_student(ids,classid)
    {
        confirm('Are you sure to cancel this class?');
        $.get( "<?php print base_url().$this->uri->segment(1).'/cancelstudent';?>", { student_id: ids, classids: classid}, function( data ) {
            location.reload(true);
        });
    }
    
    function delete_student(ids)
    {
        confirm('Are you sure to delete this class?');
        $.get( "<?php print base_url().$this->uri->segment(1).'/deletestudent';?>", { student_id: ids}, function( data ) {
            location.reload(true);
        });
    }
    
    function load_cancel_data(ids)
    {
        $.get( "<?php print base_url().$this->uri->segment(1).'/getUserActivity';?>", { class_id: ids}, function( data ) {
            var r = JSON.parse(data);
            $('#loadClassDetail').html(r);
            $('#loadCalcenModal').show();
        });
    }
    
    function getTeacherList(ids)
    {
        $.get( "<?php print base_url().$this->uri->segment(1).'/getTeacherList';?>", { 'subject_id': ids}, function( data ) {
            $('#teacher_id').html(JSON.parse(data));
        });
    }


    function addStudent(ids, subject_id)
    {
        $('#class_ids').val(ids);
        $('#booking_subject_ids').val(subject_id);
        $('#studentModal').modal('show');

        var booking_subject_ids = $("#booking_subject_ids").val();
        var options = {
          url: "<?php print base_url().$this->uri->segment(1).'/searchstudent/';?>" + booking_subject_ids,
          getValue: "name",
          list: {
            onSelectItemEvent: function() {
                var value = $("#student_id").getSelectedItemData().code;
                $("#student_ids").val(value).trigger("change");
            },
            maxNumberOfElements: 1000,
             match: {
                enabled: true
            }
          },
          theme: "square"
        };
        $("#student_id").easyAutocomplete(options);
    }


</script>
<?php $this->load->view('admin/footer');?>