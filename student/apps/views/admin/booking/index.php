<?php $this->load->view('admin/header');?>         
    <div class="row">
        <div class="col-sm-12">
            <div class="element-wrapper">
                <h6 class="element-header">Time Slot Booking</h6>
                <div class="element-content">
                    <div class="element-box">
                        <h5 class="form-header">Classes Schdule</h5>
                        <div id="fullCalendar_class"></div>
                    </div>
                </div>
            </div>



        </div>
    </div>
                        
<script type="text/javascript">
$(function(){
    if ($("#fullCalendar_class").length) {
        var calendar;
        calendar = $("#fullCalendar_class").fullCalendar({
          header: {
            left: "prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay"
          },
          selectable: true,
          selectHelper: true,
          editable: true,
          events: [<?php 
                    $cal = $this->custom->getAllSlot();
                if($cal->num_rows() > 0){
                    foreach($cal->result() as $row){
                          $teacherNote= $this->custom->getTeacherNote($row->teacher_id,$row->booking_id);  
                          $studentNote=$this->custom->getStudentNote($row->student_id,$row->booking_id);  
                          $bell = "";
                          if($teacherNote!=''|| $teacherNote!=null || $teacherNote !=NULL)
                          {
                              $bell = "<a href='#' style='height:20px;width:20px;line-height:20px;background:red;position:absolute;top:0;right:0;text-align:center;border-radius:100%;'><i class='os-icon os-icon-bell' style='color:#fff;'></i></a>";
                          }
                        
                          if($studentNote!=''|| $studentNote!=null || $studentNote !=NULL)
                          {
                              $bell = "<a href='#' style='height:20px;width:20px;line-height:20px;background:red;position:absolute;top:0;right:0;text-align:center;border-radius:100%;'><i class='os-icon os-icon-bell' style='color:#fff;'></i></a>";
                          }


                        if($this->custom->getStudentAdmission($row->student_id) == 1){?>{
                        title: "T: <?php print $this->custom->getTeacherName($row->teacher_id);?>",
                        description: "S: <?php print $this->custom->getStudentName($row->student_id);?>",
                      subject: "Sub: <?php print $this->custom->getSubjectName($row->subject_id);?>",
                      subject_id: '<?php print $row->subject_id?>',
                      start: '<?php print $row->slotdate;?>T<?php print $row->timefrom;?>',
                      end: '<?php print $row->slotdate;?>T<?php print $row->timeto;?>',
                      dateValue: '<?php echo $row->slotdate;?>',
                      bookingID: '<?php print $row->booking_id;?>',
                      timeslot: '<?php print $row->timeslot;?>',
                      display: 'background',
                      clicon: '<?php print $this->custom->getSbColor($row->subject_id);?>',
                      clicons: '<?php print $this->custom->getSbBColor($row->subject_id);?>',
                      color: '#e65252',
                      school_id : '<?php print $this->custom->getSchoolName($row->school_id);?>',
                      school_color : '<?php print $this->custom->getSchoolColor($row->school_id);?>',
                      schoolid : '<?php print $row->school_id?>',
                      school_img : '<?php print $this->subjects_model->getFlagById($row->subject_id);?>',
                      teacherNote: '<?php print  trim(preg_replace('/\s\s+/', ' ', $teacherNote));?>',
                      studentNoteId: '<?php print ($this->custom->getStudentNoteId($row->student_id,$row->booking_id))?$this->custom->getStudentNoteId($row->student_id,$row->booking_id):0 ?>',
                      student_id : '<?php print $row->student_id;?>',
                      studentLesson: '<?php print $this->custom->getStudentLesson($row->student_id);?>',
                      upComingLesson: '<?php print $this->custom->upComingLesson($row->student_id);?>',
                      studentName: '<?php print $this->custom->getStudentName($row->student_id);?>',
                      studentNumber:'<?php print $this->custom->getStudentPhone($row->student_id); ?>',
                      studentLevel: '<?php print $this->custom->getStudentLevel($row->student_id);?>',
                      studentImage: '<?php print $this->custom->getStudentImage($row->student_id);?>',
                      teacherImage: '<?php print $this->custom->getTeacherImage($row->teacher_id);?>',
                      studentNote: '<?php print trim(preg_replace('/\s\s+/', ' ', $studentNote));?>',
                      teacherName: '<?php print $this->custom->getTeacherName($row->teacher_id);?>',
                      getStudentLessonTaken: '<?php print $this->custom->getClassById($row->student_id);?>',
                      teacherNoteId: '<?php print ( $this->custom->getTeacherNoteId($row->teacher_id,$row->booking_id))? $this->custom->getTeacherNoteId($row->teacher_id,$row->booking_id):0;?>',
                      teacher_id : '<?php print $row->teacher_id;?>',
                      student: 0,
                      teacher: 1,
                      bell: "<?php print $bell;?>"
          },<?php  } else { ?>{
                        title: "T: <?php print $this->custom->getTeacherName($row->teacher_id);?>",
                        description: "S: <?php print $this->custom->getStudentName($row->student_id);?>",
                        studentName: '<?php print $this->custom->getStudentName($row->student_id);?>',
                        studentNumber:'<?php print $this->custom->getStudentPhone($row->student_id); ?>',
                        subject: "Sub: <?php print $this->custom->getSubjectName($row->subject_id);?>",
                        subject_id: '<?php print $row->subject_id?>',
                        start: '<?php print $row->slotdate;?>T<?php print $row->timefrom;?>',
                        teacherName: '<?php print $this->custom->getTeacherName($row->teacher_id);?>',
                        upComingLesson: '<?php print $this->custom->upComingLesson($row->student_id);?>',
                        timeslot: '<?php print $row->timeslot;?>',
                        end: '<?php print $row->slotdate;?>T<?php print $row->timeto;?>',
                        clicon: '<?php print $this->custom->getSbColor($row->subject_id);?>',
                        studentNote: '<?php print trim(preg_replace('/\s\s+/', ' ', $studentNote));?>',
                        studentNoteId: '<?php print ($this->custom->getStudentNoteId($row->student_id,$row->booking_id))?$this->custom->getStudentNoteId($row->student_id,$row->booking_id):0 ?>',
                        dateValue: '<?php echo $row->slotdate;?>',
                        bookingID: '<?php print $row->booking_id;?>',
                        studentLevel: '<?php print $this->custom->getStudentLevel($row->student_id);?>',
                        studentImage: '<?php print $this->custom->getStudentImage($row->student_id);?>',
                        teacherImage: '<?php print $this->custom->getTeacherImage($row->teacher_id);?>',
                        studentLesson: '<?php print $this->custom->getStudentLesson($row->student_id);?>',
                        getStudentLessonTaken: '<?php print $this->custom->getClassById($row->student_id);?>',
                        teacherNote: '<?php print  trim(preg_replace('/\s\s+/', ' ', $teacherNote));?>',
                        teacherNoteId: '<?php print ( $this->custom->getTeacherNoteId($row->teacher_id,$row->booking_id))? $this->custom->getTeacherNoteId($row->teacher_id,$row->booking_id):0;?>',
                        clicons: '<?php print $this->custom->getSbBColor($row->subject_id);?>',
                        display: 'background',
                        school_id : '<?php print $this->custom->getSchoolName($row->school_id);?>',
                        school_color : '<?php print $this->custom->getSchoolColor($row->school_id);?>',
                        schoolid : '<?php print $row->school_id?>',
                        school_img : '<?php print $this->subjects_model->getFlagById($row->subject_id);?>',
                        student_id : '<?php print $row->student_id;?>',
                        teacher_id : '<?php print $row->teacher_id;?>',
                        student: 0,
                        teacher: 1,
                        bell: "<?php print $bell;?>"
                  },<?php
                        }
                    }
                }
          ?>],
            eventRender: function(event, element) {
                // To append if is assessment
                if(event.description != '' && typeof event.description  !== "undefined" || event.subject != '' && typeof event.subject  !== "undefined")
                // {  
                //     element.find(".fc-title").empty();
                //     element.find(".fc-title").append("<br/><a href='#' style='height:20px;width:20px;line-height:20px;background:red;position:absolute;top:0;right:0;text-align:center;border-radius:100%;'><i class='os-icon os-icon-bell' style='color:#fff;'></i></a><b class='tInfoModal' data-toggle='modal' data-target='#teacherInfoModal'>"+event.title+"<a></a></b>"+"<br/><b class='bold_student sInfoModal'data-toggle='modal' data-target='#studentInfoModal'>"+event.description+"</b><br/><b class='bold_student' style='background-color:"+event.clicon+";color:"+event.clicons+";padding:3px 5px;display:block;line-height:16px;margin-top:5px;display:flex;justify-content:space-between;align-items:center;'>"+event.subject+" <img src='"+event.school_img+"' width='30'></b><br/><b>School: "+event.school_id+"</b></br></br><a href='<?php print base_url();?>students/viewprofile/"+event.student_id+"' target='_blank' class='btn btn-warning btn-sm'>View</a>");
                // }
                {
                    console.log(event.upComingLesson);
                    element.find(".fc-title").empty();
                    element.find(".fc-title").append("<br/>"+event.bell+"<b class='tInfoModal tooltip-col'><span>"+event.title+"</span><span class='tooltiptext3'><a href='#' data-toggle='modal'  onclick='functionEdit("+ event.teacher +","+event.bookingID+","+event.teacher_id +","+event.teacherNoteId +")'  data-target='#teachereditModal' style='position:absolute;color:#9e9e9e;right:10px;top:7px;' class='os-icon os-icon-edit-1'></a><input type='hidden' id='teacherNoteHidden"+event.teacherNoteId +"' value='"+event.teacherNote +"'> <br><div class='user-img-wrapper'><div class='user-circle-img'><img src='"+ event.teacherImage +"'></div><div class='teacherName'>"+event.teacherName+"</div></div><div class='sticky-note'><div class='stickyHeading'>Sticky Note</div><p>"+event.teacherNote+"</p></div></span></b>"+"<br/><b class='bold_student sInfoModal tooltip-col'><span>"+event.description+"</span><span class='tooltiptext3'><a href='#' style='position:absolute;color:#9e9e9e;right:10px;top:7px;' class='os-icon os-icon-edit-1' data-toggle='modal' onclick='functionEdit("+event.student+","+event.bookingID+","+event.student_id+","+event.studentNoteId+",)' data-target='#studenteditModal'></a> <br><input type='hidden' id='studentNoteHidden"+event.studentNoteId +"' value='"+event.studentNote +"'><div class='user-img-wrapper'><div class='user-circle-img'><img src='"+event.studentImage+"'></div><div><div class='studentName'>"+event.studentName+"</div><div style='text-align:left;margin-top:3px;'><i style='margin-right:5px;' class='os-icon os-icon-phone'></i>"+event.studentNumber+"</div></div></div><ul style='text-align:left;margin-bottom:0;margin-top:12px;padding-left:15px;'><li>Total Lesson : <b>"+event.studentLesson+"</b></li><li>Lesson Taken : <b>"+event.getStudentLessonTaken+"</b></li><li>Upcoming Lesson :<b>"+event.upComingLesson +" </b></li><li>Level : <b style='text-transform:uppercase;'>"+event.studentLevel+"</b></li><li>Due Payment : <b>Nothing</b></li></ul><div class='sticky-note'><div class='stickyHeading'>Sticky Note</div><p>"+event.studentNote+"</p></div></span></b><br/><b class='bold_student' style='background-color:"+event.clicon+";color:"+event.clicons+";padding:3px 5px;display:block;line-height:16px;margin-top:5px;display:flex;justify-content:space-between;align-items:center;'>"+event.subject+" <img src='"+event.school_img+"' width='30'></b><b style=' "+event.school_color+"margin-top:10px;margin-bottom:10px;display:inline-block;display:block;padding:3px 5px;'><span style='color:#000;white-space:normal'>School : <span onclick='schoolEdit("+event.bookingID+","+event.schoolid+","+event.timeslot+","+event.subject_id+")'> "+event.school_id+" <i style='color:#000;' class='os-icon os-icon-edit'></i></span></span><br></b><a href='<?php print base_url();?>students/viewprofile/"+event.student_id+"' target='_blank' class='btn btn-warning btn-sm'>View</a>");

                }
            }
        });
      }

});
</script>     

<!-- Teacher Edit Modal -->
<div class="infoModal">
  <div class="modal fade" id="teachereditModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Teacher</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

          <?php
          $allTeachers = $this->custom->getAllTeachers();
          ?>
        <form  action="<?php print base_url().'note/insert';?>" name="postform" method="post">
            <div class="modal-body">
              <div class="container">
                  <input type="hidden" id="dateValueT" name="date">
                  <input type="hidden" id="bookingIdT" name="booking_id">
                  <input type="hidden" id="noteIdT" name="id">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Change Teacher</label>
                                  <select class="form-control" id="teacherIdSelect" name="teacher_id">
                                    <option>Select teacher</option>
                                      <?php
                                      foreach ($allTeachers as $teach) {
                                          ?>
                                          <option value="<?php echo  $teach['code'] ?>"><?php echo  $teach['name'] ?></option>
                                          <?php
                                      }
                                      ?>
                                  </select>
                              </div>
                              <div class="">
                                  <label>Add Sticky Notes</label>
                                  <textarea class="form-control" id="teacherNoteText" name="note"></textarea>
                              </div>
                          </div>
                          
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>


<!-- Student Edit Modal -->
<div class="infoModal">
  <div class="modal fade" id="studenteditModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Student</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form action="<?php print base_url().'note/insert';?>" name="postform" method="post">
            <input type="hidden" id="dateValue" name="date">
            <input type="hidden" id="bookingIdS" name="booking_id">
            <input type="hidden" id="noteIdS" name="id">
            <div class="modal-body">
              <div class="container">    
                      <div class="row">
                          <div class="col-md-12">

                              <?php
                              $allStudents = $this->custom->getAllStudent();
                              ?>
                              <div class="form-group">
                                  <label>Change Student</label>
                                  <select class="form-control" name="student_id" id="stdIdSelect">
                                      <option>Select student</option>
                                      <?php
                                      foreach ($allStudents as $stud) {
                                          ?>
                                          <option value="<?php echo  $stud['code'] ?>"><?php echo  $stud['name'] ?></option>
                                          <?php
                                      }
                                      ?>
                                  </select>
                              </div>
                              <div class="">
                                  <label>Add Sticky Notes</label>
                                  <textarea id="studentNote" class="form-control" name="note"></textarea>
                              </div>
                          </div>
                          
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
<!-- School Edit Modal -->
<div class="infoModal">
  <div class="modal fade" id="schooleditModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit School</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form action="<?php print base_url().'note/updateSchool';?>" name="postform" method="post">
            <input type="hidden" id="bookingIdSchool" name="booking_id">
            <input type="hidden" id="subjectIdSchool" name="subject_id">
            <input type="hidden" id="schoolSlotid" name="slotId">
            <div class="modal-body">
              <div class="container">
                      <div class="row">
                          <div class="col-md-12">

                              <?php
                              $allStudents = $this->custom->getAllSchool();
                              ?>
                              <div class="form-group">
                                  <label>Change School</label>
                                  <select class="form-control" name="school_id" id="stdIdSchool">
                                      <option>Select school</option>
                                      <?php
                                      foreach ($allStudents as $stud) {
                                          ?>
                                          <option value="<?php echo  $stud['id'] ?>"><?php echo  $stud['name'] ?></option>
                                          <?php
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/footer');?>

<script>

    function functionEdit(typeT, idBooking, stdID, noteId){

        if(typeT == 0){
            $('#bookingIdS').val(idBooking);
            document.getElementById('stdIdSelect').value = stdID;
            if(noteId > 0){

                $('#noteIdS').val(noteId);
            $('#studentNote').html($('#studentNoteHidden'+noteId).val())
            }
        }
        if(typeT == 1){
            $('#bookingIdT').val(idBooking);
            document.getElementById('teacherIdSelect').value = stdID;
            if(noteId > 0){

                $('#noteIdT').val(noteId);
                $('#teacherNoteText').html($('#teacherNoteHidden'+noteId).val())
            }
        }


    }
    function schoolEdit(bookingId, schoolId,timeslot,subjectId) {

        $('#subjectIdSchool').val(subjectId);
        $('#bookingIdSchool').val(bookingId);
        $('#schoolSlotid').val(timeslot);
        $('#schooleditModal').modal('show');
        document.getElementById('stdIdSchool').value = schoolId;
    }
</script>