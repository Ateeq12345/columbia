<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin User</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-6">
        <?php if(   $this->permission_model->getAddPerm('adminuser',$this->custom->getUserProfile())== 1 ) { ?>
												<a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add User</a>
        <?php } ?>
											</div>
                                            <div class="col-sm-6">
                                                <form class="form-inline justify-content-sm-end">
                                                    <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                    <th>Created</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
												if($users->num_rows() > 0){
													foreach($users->result() as $row){
											?>    
												
												<tr>
                                                    <td><?php print ucfirst($row->fname . ' ' . $row->lname);?></td>
                                                    <td><?php print $row->email;?></td>
                                                    <td><?php if($row->active == 1){ print 'Active'; } else { print 'Block';}?></td>
                                                    <td><?php print $row->created;?></td>
                                                    <td class="row-actions">
        <?php if (   $this->permission_model->getEditPerm('adminuser',$this->custom->getUserProfile())== 1 ) { ?>
														<a href="<?php print base_url().$this->uri->segment(1).'/edit/'.$row->id;?>"><i class="os-icon os-icon-ui-49"></i></a>
        <?php } ?>
														<!--<a href="#"><i class="os-icon os-icon-grid-10"></i></a>-->
        <?php if (   $this->permission_model->getDelPerm('adminuser',$this->custom->getUserProfile())== 1 ) { ?>
														<a class="danger" onclick="return confirm('Are your Sure to delete this?');" href="<?php print base_url().$this->uri->segment(1).'/delete/'.$row->id;?>"><i class="os-icon os-icon-ui-15"></i></a>
        <?php } ?>
													</td>
                                                </tr>
                                            <?php 
													}
												}
											?>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	