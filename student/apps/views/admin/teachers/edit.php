<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Edit Teacher</h6>
                                    <?php print flash_message();?>
									<div class="row">
										<div class="col-sm-6">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/update';?>" id="formValidate" enctype="multipart/form-data">
									<?php 
										if($user->num_rows() > 0){
											$row = $user->row();
									?>			
													<div class="form-group">
														<label for=""> Firstname</label>
														<input type="text" name="fname" class="form-control" data-error="Please enter Firstname." placeholder="Enter Firstname" required="required" value="<?php print $row->fname;?>"/>
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Lastname</label>
														<input type="text" name="lname" class="form-control" data-error="Please enter Lastname." placeholder="Enter Lastname" required="required" value="<?php print $row->lname;?>">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Email address</label>
														<input type="email" name="email" class="form-control" data-error="Your email address is invalid" placeholder="Enter email" required="required" value="<?php print $row->email;?>" readonly disabled>
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Address</label>
														<input type="text" name="address" class="form-control" data-error="Please enter Address." placeholder="Enter Address" value="<?php print $row->address;?>" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Phone</label>
														<input type="tel" name="phone" class="form-control" data-error="Please enter Phone." placeholder="Enter Phone" value="<?php print $row->phone;?>" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Teacher Picture</label>
														<input type="file" name="fimg" class="form-control"/>
														<img src="" alt=""/>
													</div>
													<div class="form-group">
														<label for=""> Password</label>
														<input type="password" name="password" class="form-control" data-minlength="6" placeholder="Password" />
														<div class="help-block form-text text-muted form-control-feedback">Minimum of 6 characters</div>
													</div>
													<div class="form-group">
														<label for=""> Subject</label>
														<select name="subject_id" class="form-control">
										<?php 
											if($subject->num_rows() > 0){
												foreach($subject->result() as $sub){
										?>
															<option value="<?php print $sub->id;?>"<?php if($row->subject_id == $sub->id){?> selected<?php }?>><?php print $sub->name;?></option>
										<?php
												}
											}
										?>
														</select>
													</div>
													<div class="form-group">
														<label for=""> Status</label>
														<select name="active" class="form-control">
															<option value="1"<?php if($row->active == '1'){?> selected<?php }?>>Active</option>
															<option value="0"<?php if($row->active == '0'){?> selected<?php }?>>Block</option>
														</select>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
													<input type="hidden" name="id" value="<?php print $row->id;?>"/>
										<?php } ?>		
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	