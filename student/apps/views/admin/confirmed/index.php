<?php $this->load->view('admin/header'); ?>

<div class="row">

    <div class="col-sm-12">
        <div class="element-wrapper">
            <h6 class="element-header">Confirmed Students</h6>
            <div class="table-responsive confirmed-students-wrapper">
                <div class="row" style="margin-bottom: 15px;">
                   <div class="col-sm-6">
                        <form method="get" name="post" action="<?php print base_url() . $this->uri->segment(1) . '/search' ?>" class="form-inline justify-content-sm-end">
                            <input class="form-control form-control-sm rounded bright" placeholder="Search" name="q" type="text">
                            <input  placeholder="Search" name="ty" type="hidden" value="con">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </form>
                    </div> 
                </div>
                
                <table class="table table-bordered table-lg table-v2 table-striped">
                    <thead>
                        <tr>
                            <th>Pic</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($users->num_rows() > 0) {
                        foreach ($users->result() as $row) {
                            ?>

                            <tr>
                                <td class="text-center"><div class="user-img">
                                        <?php if ($row->fimg){?>
                                        <img src="<?php print base_url().'upload/student/'.$row->fimg; ?>">
                                <?php }else{?>
                                <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg">
                            <?php }?>
                                    <!-- </div><?php print ucfirst($row->fname . ' ' . $row->lname); ?> -->
                                    </div>
                                </td>
                                <td><?php print ucfirst($row->fname); ?></td>
                                <td><?php print ucfirst($row->lname); ?></td>
                                <td><?php print $row->email; ?></td>
                                <td><?php print $row->address; ?></td>
                                <td><?php print $row->phone; ?></td>
                                <td>
                                    <a data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="<?php print base_url() . $this->uri->segment(1) . '/edit/' . $row->id; ?>"><i class="os-icon os-icon-ui-49"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>

        </div>
            <div class="controls-below-table">
                <div class="table-records-pages">
                    <ul class="pagination"><?php print $pagination; ?></ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/footer'); ?>
<script type="text/javascript">
    $(document).ready( function () {
        $('#confirmed-students-table').DataTable();
    } );
</script>