<?php $this->load->view('admin/header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="element-wrapper">
            <h6 class="element-header">Admin Students Request</h6>
            <?php print flash_message(); ?>
            <div class="table-responsive">
                <table class="table table-bordered table-lg table-v2 table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Subject</th>
                            <th>School</th>
                            <th>Slot Date</th>
                            <th>Slot Time</th>
                            <th>Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($users->num_rows() > 0) {
                            foreach ($users->result() as $row) {
                        ?>

                                <tr>
                                    <td>
                                        <?php print $row->id; ?>
                                    </td>
                                    <td><?php print $this->students_model->getNameById($row->student_id); ?></td>
                                    <td><b style="background-color: <?php print $this->subjects_model->getColorById($row->subject);?>;padding: 3px 10px;color: <?php print $this->subjects_model->getFontColorById($row->subject);?>;"><?php print $this->subjects_model->getNameById($row->subject); ?></b></td>
                                    <td><?php print $this->school_model->getSchoolById($row->school);?></td>
                                    <td><?php print $row->slotdate; ?></td>
                                    <td><?php print $row->slottime . 'PM'; ?></td>
                                    <td><?php print $row->created; ?></td>
                                    <td class="row-actions">
        <?php if(   $this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 ) { ?> 
                                        <a data-placement="top" data-toggle="tooltip" data-original-title="Confirm" class=" btn-sm btn btn-success" href="<?php print base_url() . $this->uri->segment(1) . '/confirm/' . $row->id; ?>"><i class="os-icon os-icon-edit-1"></i> Confirm</a>
        <?php } ?>
        <?php if(   $this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1 ) { ?> 
                                        <a data-placement="top" data-toggle="tooltip" data-original-title="Delete" class=" btn-sm btn btn-danger" onclick="return confirm('Are your Sure to delete this?');" href="<?php print base_url() . $this->uri->segment(1) . '/delete/' . $row->id; ?>"><i class="os-icon os-icon-ui-15"></i> Delete</a>
        <?php } ?>

                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="controls-below-table">
                <div class="table-records-pages">
                    <ul class="pagination"><?php print $pagination; ?></ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/footer'); ?>