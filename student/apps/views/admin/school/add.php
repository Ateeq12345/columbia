<?php $this->load->view('admin/header');?>
		
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="element-wrapper">
                                    <h6 class="element-header">School</h6>
									<?php print flash_message();?>
									<div class="row">
										<div class="col-sm-12">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/insert';?>" id="formValidate" enctype="multipart/form-data">
													
													<div class="row">
														<div class="col-sm-12">
															<div class="form-group">
																<label for=""> School Name</label>
																<input autocomplete="off" class="form-control" placeholder="School Name" type="text" name="school_name" required="">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-12">
															<div class="form-group">
																<label for=""> School Address</label>
																<input autocomplete="off" class="form-control" placeholder="School Address" type="text" name="school_address" required="">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label for=""> Status</label>
														<select name="school_status" class="form-control">
															<option value="1">Active</option>
															<option value="2">Block</option>
														</select>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Add</button>
													</div>
											
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	