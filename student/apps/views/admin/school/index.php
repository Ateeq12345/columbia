<?php $this->load->view('admin/header');?>
<style type="text/css">
    .student_block{
        width: auto;
        display: inline-block;
        /*padding: 5px 10px;*/
        background: #bfd6f9;
    }
    .student_block span{
        display: inline-block;
        padding: 5px 10px;
    }
    .student_del{
        cursor: pointer;
        background: #e65252;
        color: #FFFFFF;
        font-size: 10px;
        /*padding: 7px 10px;*/
    }
    .cancel_del{
        cursor: pointer;
        background: #34a853;
        color: #FFFFFF;
        font-size: 10px;
        /*padding: 7px 10px;*/
    }
</style>			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">School</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-4">
        <?php if(   $this->permission_model->getAddPerm('school',$this->custom->getUserProfile())== 1 ) { ?>   
											 <a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add School</a>
        <?php }?>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
													<th>School Name</th>
                                                    <th>School Address</th>
													<th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
												if($sql->num_rows() > 0){
													foreach($sql->result() as $row){
											?>    
												<tr>
													<td style="width: 120px;"><?php print $row->id;?></td>
                                                    <td style="width: 200px;"><?php print $row->school_name;?></td>
                                                    <td><?php print $row->school_address;?></td>
                                                    <td class="row-actions" style="width: 300px;">
        <?php if(   $this->permission_model->getEditPerm('school',$this->custom->getUserProfile()) == 1 ) { ?>  
                                                        <a class="btn btn-sm btn-info" href="<?php print base_url().$this->uri->segment(1).'/edit/'.$row->id;?>"><i class="os-icon os-icon-ui-49"></i> Edit</a>
        <?php } ?>
        <?php if(   $this->permission_model->getDelPerm('school',$this->custom->getUserProfile()) == 1 ) { ?>  
														<a class="btn btn-sm btn-danger" onclick="return confirm('Are your Sure to delete this?');" href="<?php print base_url().$this->uri->segment(1).'/delete/'.$row->id;?>"><i class="os-icon os-icon-ui-15"></i> Delete</a>
        <?php } ?>
													</td>
                                                </tr>
                                            <?php 
													}
												}
											?>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>

<?php $this->load->view('admin/footer');?>