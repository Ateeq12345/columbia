<?php $this->load->view('admin/header');?>


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin User</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-6">
		<?php if(	$this->permission_model->getAddPerm('profile',$this->custom->getUserProfile())== 1 ) { ?>	
					<a class="btn btn-info" href="<?php print base_url().$this->uri->segment(1);?>/add<?php print $this->config->item('url_suffix');?>" title="Add"><i class="fa fa-plus"></i></a></li>
		<?php }?>	
												<!-- <a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add User</a> -->
											</div>
                                            <div class="col-sm-6">
                                                <form class="form-inline justify-content-sm-end">
                                                    <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>        
													<th width="5%">ID</th>
													<th width="75%">Name</th>
													<th width="20%">Action</th>  
                                                </tr>
                                            </thead>
                                            <tbody>
						<?php 
							if($sql->num_rows() > 0){
								foreach($sql->result() as $row){
						?>
							<tr>                               
								<td><?php print $row->id;?></td>
								<td><?php print ucfirst($row->name);?></td>
								<td>
						<?php if(	$this->permission_model->getEditPerm('profile',$this->custom->getUserProfile())== 1 ) { ?>	
									<a class="btn btn-xs btn-success" href="<?php print base_url().$this->uri->segment(1);?>/edit/<?php print $row->id.$this->config->item('url_suffix');?>" title="Edit"><i class="fa fa-pencil"></i></a>
						<?php }?>					
						<?php if(	$this->permission_model->getDelPerm('profile',$this->custom->getUserProfile())== 1 ) { ?>	
									<a class="btn btn-xs btn-danger" href="<?php print base_url().$this->uri->segment(1);?>/delete/<?php print $row->id.$this->config->item('url_suffix');?>" onclick="return confirm('Are you sure to delete this?');" title="Delete"><i class="fa fa-trash"></i></a>
						<?php }?>						
						<?php if(	$this->permission_model->getAddPerm('permission',$this->custom->getUserProfile())== 1 ) { ?>	
									<a class="btn btn-xs btn-warning" href="<?php print base_url().$this->uri->segment(1);?>/permission/<?php print $row->id.$this->config->item('url_suffix');?>"><i class="fa fa-lock"></i></a>
						<?php }?>					
								</td>
							</tr>                     
						<?php 
								}
							}	
						?>	   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	