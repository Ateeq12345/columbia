<?php $this->load->view('admin/header');?>   
        
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Profile</h6>
                                    <?php print flash_message();?>
									<div class="row">
										<div class="col-sm-6">
											<div class="element-box">
        <form  role="form" method="post" name="postform" action="<?php print base_url().$this->uri->segment(1);?>/insert<?php print $this->config->item('url_suffix');?>" enctype="multipart/form-data">
					<!-- /.box-header -->
					<div class="box-body">
						<?php print flash_message();?>
						<!-- text input -->
						<div class="form-group">
						  <label>Profile Name</label>
						  <input type="text" class="form-control" name="name" placeholder="Profile Name ..." required>
						</div>
					</div>	
					<div class="box-footer">
						<button class="btn btn-primary" type="submit">Submit</button>
					</div>
		</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	