<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin User</h6>
                                    <?php print flash_message();?>
									<div class="row">
										<div class="col-sm-6">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/update';?>" id="formValidate">
						<?php 
							if ( $lang->num_rows() > 0 ) {
								$row = $lang->row();
						?>
								<input type="hidden" name="id" value="<?php print $row->id;?>" />
													<div class="form-group">
														<label for=""> English</label>
														<input type="text" name="eng" class="form-control" data-error="Please enter English." placeholder="Enter Firstname" required="required"  value="<?php print $row->eng;?>">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Japanese</label>
														<input type="text" name="jap" class="form-control" data-error="Please enter Japanese." placeholder="Enter Lastname" required="required" value="<?php print $row->jap;?>">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
						<?php } ?>					
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	