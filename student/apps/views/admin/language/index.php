<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Admin Language</h6>
									<?php print flash_message();?>
									<div class="controls-above-table">
                                        <div class="row">
                                            <div class="col-sm-6">
												<a class="btn btn-sm btn-success" href="<?php print base_url().$this->uri->segment(1).'/add';?>"><i class="icon-plus"></i> Add language</a>
											</div>
                                            <div class="col-sm-6">
                                                <form class="form-inline justify-content-sm-end">
                                                    <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
									<div class="table-responsive">
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
                                                    <th>English</th>
                                                    <th>Japanese</th>
                                                    <th>Created</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
												if($users->num_rows() > 0){
													foreach($users->result() as $row){
											?>    
												
												<tr>
                                                    <td><?php print ucfirst($row->eng);?></td>
                                                    <td><?php print $row->jap;?></td>
                                                    <td><?php print $row->created;?></td>
                                                    <td class="row-actions">
														<a href="<?php print base_url().$this->uri->segment(1).'/edit/'.$row->id;?>"><i class="os-icon os-icon-ui-49"></i></a>
														<!--<a href="#"><i class="os-icon os-icon-grid-10"></i></a>-->
													</td>
                                                </tr>
                                            <?php 
													}
												}
											?>    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	