<?php $this->load->view('admin/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Edit Subject</h6>
                                    <?php print flash_message();?>
									<div class="row">
										<div class="col-sm-6">
											<div class="element-box">
												<form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/update';?>" id="formValidate" enctype="multipart/form-data">
									<?php 
										if($user->num_rows() > 0){
											$row = $user->row();
									?>			
													<div class="form-group">
														<label for=""> Subject Name</label>
														<input type="text" name="name" class="form-control" data-error="Please enter Subject Name." placeholder="Enter Subject Name" required="required" value="<?php print $row->name;?>"/>
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Background Color</label>
														<input type="color" name="color_code" class="form-control" data-error="Please Select Subject Color." id="color_code" placeholder="Background Color" required="required" value="<?php print $row->color_code;?>">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Font Color</label>
														<input type="color" name="colors" class="form-control" data-error="Please Select Font Color." value="<?php print $row->colors;?>" id="colors" placeholder="Font Color" required="required">
														<div class="help-block form-text with-errors form-control-feedback"></div>
													</div>
													<div class="form-group">
														<label for=""> Upload Flags</label>
														<input type="file" class="form-control" name="fimg">
														<?php if($row->fimg !== ""){ ?>
															<img src="<?php print base_url().'upload/subjects/thumb/thumb_'.$row->fimg;?>" alt="">
														<?php } ?>
													</div>
													<div class="form-buttons-w">
														<button class="btn btn-primary" type="submit"> Submit</button>
													</div>
													<input type="hidden" name="id" value="<?php print $row->id;?>"/>
										<?php } ?>		
												</form>
											</div>
										</div>
                                    </div>
								</div>
							</div>
						</div>
			
<?php $this->load->view('admin/footer');?>	