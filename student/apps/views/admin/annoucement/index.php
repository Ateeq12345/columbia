<?php $this->load->view('admin/header'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="element-wrapper">
            <h6 class="element-header">Admin Annoucements</h6>
            <?php print flash_message(); ?>
            <div class="table-responsive">
                <table class="table table-bordered table-lg table-v2 table-striped">
                    <thead>
                        <tr>
                            <th width="5%" class="text-center">Sr #</th>
                             <th>Date</th>
                            <th class="text-left">Title</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($sql->num_rows() > 0) {
                            foreach ($sql->result() as $row) {
                        ?>

                                <tr>
                                    <td class="text-center">
                                        <?php print $row->id; ?>
                                    </td>
                                    <td class="text-center" width="10%"><?php echo $row->date?></td>
                                    <td><?php echo $row->title?></td>
                                    
                                    
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="controls-below-table">
                <div class="table-records-pages">
                    <ul class="pagination"><?php print $pagination; ?></ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/footer'); ?>