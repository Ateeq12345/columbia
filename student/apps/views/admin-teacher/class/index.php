<?php $this->load->view('teacher/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Dashboard</h6>
                                    <div class="table-responsive">
                                <?php if($sql->num_rows() > 0){ ?>        
                                        <table class="table table-bordered table-lg table-v2 table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Subject</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                    <th>Students</th>
                                                    <th>Status</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                                    foreach($sql->result() as $row){
                                            ?>    
                                                <tr>
                                                    <td><?php print $this->subjects_model->getNameById($row->subject_id);?></td>
                                                    <td><?php print $row->slotdate;?></td>
                                                    <td><?php print date('h:i A', strtotime($row->timefrom));?> / <?php print date('h:i A', strtotime($row->timeto));?></td>
                                                    <td><?php print $this->custom->getStudentCountByTeacher($row->timeslot);?></td>
                                                    <td><?php 
                                                        if ( $row->slotstatus == 1 ) {
                                                            print 'Next Session';
                                                        } elseif ( $row->slotstatus == 2 ) {
                                                            print 'session Closed';
                                                        } else {
                                                            
                                                        }
                                                    ?></td>
                                                    <td class="row-actions">
                                                        <a href="<?php print base_url().$this->uri->segment(1).'/classes/edit/'.$row->timeslot;?>"><i class="os-icon os-icon-ui-49"></i></a>
                                                    </td>
                                                </tr>
                                            <?php 
                                                    }
                                            ?>    
                                            </tbody>
                                        </table>
                                <?php } else { ?>
                                    <div class="alert alert-danger">No result found.</div>
                                <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    
<?php $this->load->view('teacher/footer');?>	