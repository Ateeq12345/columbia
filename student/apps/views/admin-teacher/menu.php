			<div class="menu-mobile menu-activated-on-click color-scheme-dark">
                <div class="mm-logo-buttons-w">
                    <a class="mm-logo" href="index.html"><img src="<?php print base_url();?>admin_theme/img/logo.png"><span>Clean Admin</span></a>
                    <div class="mm-buttons">
                        <div class="content-panel-open">
                            <div class="os-icon os-icon-grid-circles"></div>
                        </div>
                        <div class="mobile-menu-trigger">
                            <div class="os-icon os-icon-hamburger-menu-1"></div>
                        </div>
                    </div>
                </div>
                <div class="menu-and-user">
                    <ul class="main-menu">
                        <li class="has-sub-menu">
                            <a href="index.html">
                                <div class="icon-w">
                                    <div class="os-icon os-icon-layout"></div>
                                </div><span>Dashboard</span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                        <a href="<?php print base_url().'admin-teacher'?>">
                            <div class="icon-w">
                                <div class="os-icon os-icon-grid-squares2"></div>
                            </div><span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print base_url().'admin-teacher/classes'?>">
                            <div class="icon-w">
                                <div class="fa fa-graduation-cap"></div>
                            </div><span>Classes</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print base_url().'logout'?>">
                            <div class="icon-w">
                                <div class="fa fa-power-off"></div>
                            </div><span>Logout</span>
                        </a>
                    </li>
                            </ul>
                        </li>
                    </ul>
                    
                </div>
            </div>
			
            <div class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
                <div class="logo-w">
                    <a class="logo" href="index.html">
                        <div class="logo-element"></div>
                        <div class="logo-label">Clean Admin</div>
                    </a>
                </div>
                <div class="logged-user-w avatar-inline">
                    <div class="logged-user-i">
                        <div class="logged-user-info-w">
                            <div class="logged-user-name"><?php print $this->custom->getTeacherUser();?></div>
                            <div class="logged-user-role">Teacher</div>
                        </div>
                        <div class="logged-user-toggler-arrow">
                            <div class="os-icon os-icon-chevron-down"></div>
                        </div>
                        <div class="logged-user-menu color-style-bright">
                            <ul>
                                <li><a href="#"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h1 class="menu-page-header">Page Header</h1>
                <ul class="main-menu">
                    <li>
                        <a href="<?php print base_url().'admin-teacher'?>">
                            <div class="icon-w">
                                <div class="os-icon os-icon-grid-squares2"></div>
                            </div><span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print base_url().'admin-teacher/classes'?>">
                            <div class="icon-w">
                                <div class="fa fa-graduation-cap"></div>
                            </div><span>Classes</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print base_url().'logout'?>">
                            <div class="icon-w">
                                <div class="fa fa-power-off"></div>
                            </div><span>Logout</span>
                        </a>
                    </li>
                </ul>
            
			</div>

