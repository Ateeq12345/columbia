<?php $this->load->view('student/header');?>
			
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Slot Booking</h6>
									
                                    <div class="element-box">
												<div class="element-wrapper">
                            <h6 class="element-header">Select Time <?php print $this->input->get('d');?></h6>
                            <div class="element-box-tp">
                                <div class="table-responsive">
                        <form method="post" name="postform" action="<?php print base_url().'student/booking/sendrequest';?>">
                            <input type="hidden" name="slotdate" value="<?php print $this->input->get('d');?>" />
                                    <table class="table table-padded">
                                        <thead>
                                            <tr>
                                                <th>Subject</th>
                                                <th>School</th>
                                                <th>Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="nowrap">
                                                    <select name="subject" class="form-control">
                                                    <?php 
                                                        if($subj->num_rows() > 0){
                                                            foreach($subj->result() as $s){
                                                    ?>
                                                        <option value="<?php print $s->id;?>"><?php print $s->name;?></option>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                    </select>
                                                </td>
                                                <td class="nowrap">
                                                    <select name="school" class="form-control">
                                                    <?php 
                                                        if($school->num_rows() > 0){
                                                            foreach($school->result() as $sch){
                                                    ?>
                                                        <option value="<?php print $sch->id;?>"><?php print $sch->school_name;?></option>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="slottime" class="form-control">
                                                        <?php for($time=1; $time < 10; $time++){?>
                                                            <option value="<?php print $time;?>">0<?php print $time;?> PM</option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    	<tfoot>
                                    		<tr>
                                    			<td colspan="6">
                                    				<button type="submit" class="btn btn-success">Send Request</button>
                                    			</td>
                                    		</tr>
                                    	</tfoot>
                                    </table>

                        </form>
                                </div>
                            </div>
                        </div>
                        				
											</div>

								</div>
							</div>
						</div>
			
<?php $this->load->view('student/footer');?>	