<!DOCTYPE html>
<html>
<head>
    <title>Admin Panel</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="<?php print base_url();?>admin_theme/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="<?php print base_url();?>admin_theme/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php print base_url();?>admin_theme/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="<?php print base_url();?>admin_theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php print base_url();?>admin_theme/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php print base_url();?>admin_theme/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="<?php print base_url();?>admin_theme/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
    <link href="<?php print base_url();?>admin_theme/css/maince5a.css?version=4.4.1" rel="stylesheet">
</head>

<body class="auth-wrapper">
    <div class="all-wrapper menu-side with-pattern">
        <div class="auth-box-w">
            <div class="logo-w">
                <a href="<?php print base_url().$this->uri->segment(1);?>">
					<img alt="" src="<?php print base_url();?>admin_theme/img/logo-big.png">
				</a>
            </div>
            <h4 class="auth-header">Login Form</h4>
            <form method="post" name="postform" action="<?php print base_url().$this->uri->segment(1).'/auth';?>">
				<?php print flash_message();?>
                <div class="form-group">
                    <label for="">Username</label>
                    <input class="form-control" name="user.email" placeholder="Enter your username" type="text">
                    <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                </div>
                <div class="form-group">
                    <label for="">Password</label>
                    <input class="form-control" name="user.password" placeholder="Enter your password" type="password">
                    <div class="pre-icon os-icon os-icon-fingerprint"></div>
                </div>
                <div class="buttons-w">
                    <button type="submit" class="btn btn-primary">Log me in</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>