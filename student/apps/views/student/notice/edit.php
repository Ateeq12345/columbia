<?php $this->load->view('student/header');?>
<script type="text/javascript">
    function printDiv() 
    {
      var divToPrint=document.getElementById('printNote');
      var newWin=window.open('','Print-Window');
      newWin.document.open();
      newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
      newWin.document.close();
      setTimeout(function(){newWin.close();},10);
    }
</script>
            <?php if($class_document->num_rows() > 0){ ?>
                    <?php $cl = $class_document->row();?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Classes</h6>
                                    <div class="float-right btn btn-primary" onclick="printDiv();">Print</div>
                                    <div style="clear: both;"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                            <div class="element-box">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Class Notes and Videos</h6>
                                    <div class="element-box-tp">
                            
                            <div class="form-group">
                                <label class="element-header">Class Notes</label>
                                <div class="ticket-reply-content">
                                <?php print html_entity_decode($cl->class_notes);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="element-header">Video Title</label>
                                <div class="ticket-reply-content">
                                <?php print $cl->video_title;?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="element-header">Video Link</label>
                                <div class="ticket-reply-content">
                                    <?php  if($cl->video_link != null || $cl->video_link!=NULL || $cl->video_link!="" || $cl->video_link!='') {?>
                                    <a href="<?php print html_entity_decode($cl->video_link);?>" class="btn btn-primary">
                                        Go to Video 1
                                    </a>
                                    <?php }?>
                                    <?php if($cl->video_link_2 != null || $cl->video_link_2!=NULL || $cl->video_link_2!="" || $cl->video_link_2!='') {?>
                                    <a href="<?php print html_entity_decode($cl->video_link_2);?>" class="btn btn-primary">
                                        Go to Video 2
                                    </a>
                                    <?php }?>
                                    <?php if($cl->video_link_3 != null || $cl->video_link_3!=NULL || $cl->video_link_3!="" || $cl->video_link_3!='') {?>
                                    <a href="<?php print html_entity_decode($cl->video_link_3);?>" class="btn btn-primary">
                                        Go to Video 3
                                    </a>
                                    <?php }?>
                                </div>
                            </div>
                                    </div>
                                </div>
                            </div>                

                                        </div>
                                        
                                            </div>

                                </div>
                            </div>
                        </div>
            <div id="printNote" style="display: none;"><?php print html_entity_decode($cl->class_notes);?></div>
            
            <?php } ?>
<?php $this->load->view('student/footer');?>	