					</div>
				</div>
            </div>
        </div>
        <div class="display-type"></div>
    </div>
	
    <script src="<?php print base_url();?>admin_theme/bower_components/moment/moment.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/slick-carousel/slick/slick.min.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="<?php print base_url();?>admin_theme/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/demo_customizerce5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/maince5a.js?version=4.4.1"></script>
    <script src="<?php print base_url();?>admin_theme/js/timepicki.js"></script>
    <script src="<?php print base_url();?>admin_theme/js/datepicker.js"></script>
    <script>
		$('.timepicker1').timepicki();
    </script>
    <script >
		function myFunction(url) {
		  // alert(url);
		  $("#delClassModal").modal();
		  $("#a_append_url").attr('href','');
		  $("#a_append_url").attr('href',url);
		}
    </script>
    <script>
$(function(){
	if ($("#booking_fullCalendar_class").length) {
	   var calendar;
	     calendar = $("#booking_fullCalendar_class").fullCalendar({
	      header: {
	        left: "prev,next today",
	        center: "title",
	        right: "month"
	      },
	      selectable: true,
	      selectHelper: true,
	      editable: true,
	      events: [<?php 
	      			$cal = $this->custom->getBookingDateAndTime();
	      		if($cal->num_rows() > 0){
	      			foreach($cal->result() as $row){
	      				if ( $this->custom->getSlotById($row->id) == true ) {
	      				} else {
	      				?>{
			      	title: "<?php print date('h:i A', strtotime($row->timepickerfrom));?>",
					description: "S: <?php print $this->custom->getSubjectName($row->subject_id);?>",
					school_id: "School: <?php print $this->custom->getSchoolName($row->school_id);?>",
					school_color: "<?php print $this->custom->getSchoolColor($row->school_id);?>",
					clicon: '<?php print $this->custom->getSbColor($row->subject_id);?>',
					clicons: '<?php print $this->custom->getSbBColor($row->subject_id);?>',
			        start: '<?php print date('Y-m-d', strtotime($row->schdule_date));?>',
			        url : '<?php print base_url().'student/booking/timeslot?date='.date('Y-m-d', strtotime($row->schdule_date)).'&booking='.$row->date_id.'&type=booking&school='.$row->school_id;?>'
	      		},<?php 
	      				}
	      			}
	      		}
	      ?>],
			eventRender: function(event, element) {

				// To append if is assessment
				if(event.description != '' && typeof event.description  !== "undefined")
				{  
					element.find(".fc-title").empty();
					element.find(".fc-title").append("<b>"+event.title+"</b>"+"<br/><b class='bold_student' style='background-color:"+event.clicon+";color:"+event.clicons+";padding:3px 5px;display:block;line-height:16px;margin-top:5px;'>"+event.description+"</b><br/><b style='"+event.school_color+"padding:3px 5px;display:block;'>"+event.school_id+"</b>");
				}
			},eventClick: function (event, jsEvent, view) {
	           //what should happen when event is clicked   
	        	console.log(view);
	        },
			viewRender: function(view){
				var add_button = '<span class="add_event_label" style="font-size: 10px;cursor: pointer;text-align: left;display: inline-block;padding: 0px 10px;background: #047bf8;color: #FFF;height: 20px;line-height: 18px;">Add Request</span>'; 
                $(".fc-day-top").prepend(add_button);              
			} 
	    });
	}

	$( "#booking_fullCalendar_class" ).on( "click", ".fc-day-top span.add_event_label", function() {
        var dt = $(this).parent().attr('data-date');
      	window.location.href = '<?php print base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/addrequest?d=';?>'+dt;
  	});

	if ($("#fullCalendar_class").length) {
	    var calendar;
	    calendar = $("#fullCalendar_class").fullCalendar({
	      header: {
	        left: "prev,next today",
	        center: "title",
	        right: "month,agendaWeek,agendaDay"
	      },
	      selectable: true,
	      selectHelper: true,
	      editable: false,
	      events: [<?php 
					$today = date('Y-m-d');
					$cal = $this->custom->getStudentBookingDateForStudent();
					if($cal->num_rows() > 0){
	      			foreach($cal->result() as $row){
						if( $today < $row->slotdate ) {
				?>{
					title: "Time Slot Booked",
					school_id: "School: <?php print $this->custom->getSchoolName($row->school_id);?>",
					start: '<?php print $row->slotdate;?>T<?php print $row->timefrom;?>',
					end: '<?php print $row->slotdate;?>T<?php print $row->timeto;?>',
					icon : "trash",
					clicon: '<?php print $this->custom->getSbColor($row->subject_id);?>',
						clicons: '<?php print $this->custom->getSbBColor($row->subject_id);?>',
						appendData:"'<?php print base_url()."student/notice/cancel/".$row->timeslot;?>'"
					// url : '<?php print base_url().'student/notice/cancel/'.$row->timeslot;?>'

				},<?php 
					} else { 
				?>{
					title: "Time Slot Booked",
					school_id: "School: <?php print $this->custom->getSchoolName($row->school_id);?>",
					clicon: '<?php print $this->custom->getSbColor($row->subject_id);?>',
					clicons: '<?php print $this->custom->getSbBColor($row->subject_id);?>',
					start: '<?php print $row->slotdate;?>T<?php print $row->timefrom;?>',
					end: '<?php print $row->slotdate;?>T<?php print $row->timeto;?>',
				},		
				<?php
						}
	      			}
	      		}
	      ?>],
			eventRender: function(event, element) {
				if(event.icon || event.clicon){          
			        element.find(".fc-title").append('&nbsp;&nbsp;&nbsp;<a style="position:absolute;right:2px;top:2px;background:#fff;height:17px;width:17px;line-height:17px;text-align:center;border-radius:100%;" href="javascript:void(0);" onclick="myFunction('+event.appendData+')" ><i class="fa fa-trash" style="color:red"></i></a>');       
			        // element.find(".fc-title").append("&nbsp;&nbsp;&nbsp;<i class='fa fa-address-book' style='color:"+event.clicon+"'></i>");
			        element.find(".fc-title").append("<br/><b>"+event.school_id+"</b>");
			        element.find(".fc-title").append("<br/><b>Add</b>");
				}
				// var add_button = '<input type="button" value="+" />'; 
				// $(".fc-day-number").prepend(add_button);
			}
	    });
	  }
});
</script>

	


</body>
</html>