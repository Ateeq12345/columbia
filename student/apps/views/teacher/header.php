<!DOCTYPE html>
<html>
<head>
    <title>Admin Panel</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
<?php $this->load->view('teacher/css');?>
</head>
<body class="menu-position-side menu-side-left full-screen with-content-panel">


    <div class="all-wrapper with-side-panel solid-bg-all">
        
        <div class="layout-w">
          
<?php $this->load->view('teacher/menu');?>

            <div class="content-w">
                
				<div class="top-bar color-scheme-transparent">
                    <h3 class="top-bar-heading">Teacher Dashboard</h3>
                    <div class="top-menu-controls">
                          <div class="logged-user-w avatar-inline">
                           <div class="logged-user-i">
                              <!-- <div class="avatar-w"><img alt="" src="<?php print base_url();?>admin_theme/img/avatar1.jpg"></div> -->
                              <div class="avatar-w">
                                  <div class="user-img">
                                        <?php if ($this->custom->getTeacherUserImage()){?>
                                        <img src="<?php print base_url().'upload/teacher/'.$this->custom->getTeacherUserImage(); ?>">
                                <?php }else{?>
                                 <img src="<?php print base_url();?>admin_theme/img/nobody-user.jpg" alt="">
                            <?php }?>
                                    <!-- </div><?php print ucfirst($row->fname . ' ' . $row->lname); ?> -->
                                    </div>
                              </div>
                              <div class="logged-user-info-w">
                                 <div class="logged-user-name"><?php print $this->custom->getTeacherUser();?></div>

                                 <div class="logged-user-role">Teacher</div>
                              </div>
                              <div class="logged-user-toggler-arrow">
                                 <div class="os-icon os-icon-chevron-down"></div>
                              </div>
                              <div class="logged-user-menu color-style-bright">
                                 <div class="logged-user-avatar-info">
                                    <div class="avatar-w">
                                  <div class="user-img">
                                        <?php if ($this->custom->getTeacherUserImage()){?>
                                        <img src="<?php print base_url().'upload/teacher/'.$this->custom->getTeacherUserImage(); ?>">
                                <?php }else{?>
                                 <img src="<?php print base_url();?>admin_theme/img/nobody-user.jpg" alt="">
                            <?php }?>
                                    <!-- </div><?php print ucfirst($row->fname . ' ' . $row->lname); ?> -->
                                    </div>
                              </div>
                                    <div class="logged-user-info-w">
                                       <div class="logged-user-name"><?php print $this->custom->getTeacherUser();?></div>
                                       <div class="logged-user-role">Teacher</div>
                                    </div>
                                 </div>
                                 <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                                 <ul>
                                        <li><a href="<?php print base_url().'logout';?>"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a></li>
                                    </ul>
                              </div>
                           </div>
                        </div>



                    </div>
                </div>
                
				
                <div class="content-panel-toggler"><i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span></div>
                <div class="content-i">
                    <div class="content-box">