<?php $this->load->view('teacher/header');?>

            <?php if($class_document->num_rows() > 0){ ?>
                    <?php $cl = $class_document->row();?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Classes</h6>
                                    <div class="row">
                                        <div class="col-sm-12">
                            <div class="element-box">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Class Notes and Videos</h6>
                                    
                                    <div style="clear: both;"></div>
                                    <div class="element-box-tp">


                            
                            <div class="form-group">
                                <label>Class Notes</label>
                                <textarea cols="80" id="ckeditor1_1" name="class_notes" rows="10" style="visibility: hidden;"><?php print html_entity_decode($cl->class_notes);?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Video Title</label>
                                <input type="text" name="video_title" class="form-control" value="<?php print $cl->video_title;?>" />
                            </div>
                            <div class="form-group">
                                <label>Video Link</label>
                                 <textarea name="video_link" class="form-control" rows="5"></textarea>
                                <!-- <textarea name="video_link" class="form-control" rows="5"><?php print html_entity_decode($cl->video_link);?></textarea> -->
                            </div>

                                    </div>
                                </div>
                            </div>                

                                        </div>

                                        
                                    </div>

                                </div>
                            </div>
                        </div>

            <?php } ?>
<?php $this->load->view('teacher/footer');?>	