<?php $this->load->view('teacher/header');?>
<script type="text/javascript">
    function printDiv() 
    {
      var divToPrint=document.getElementById('printNote');
      var newWin=window.open('','Print-Window');
      newWin.document.open();
      newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
      newWin.document.close();
      setTimeout(function(){newWin.close();},10);
    }
</script>
			<?php if($class->num_rows() > 0){ ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Classes</h6>
                                    
                                    <div class="element-box">
                                                <div class="element-wrapper">
                            <h6 class="element-header">Student Attendance</h6>
                            <div class="element-box-tp">
                                <div class="table-responsive">
                        <form method="post" name="postform" action="<?php print base_url().'teacher/classes/update';?>">
                            <input type="hidden" name="class_id" value="<?php print $this->uri->segment(4);?>" />
                                    <table class="table table-padded">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Time</th>
                                                <th class="text-right">Select</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                                foreach($class->result() as $time){
                                        ?>
                                            <tr>
                                                <td class="nowrap"><span class="status-pill smaller green"></span><span>Available</span></td>
                                                <td><?php print $this->students_model->getNameById($time->student_id);?></td>
                                                <td class="text-right bolder nowrap">
                                                    <input type="checkbox" name="student[]" value="<?php print $time->student_id.'_'.$time->booking_id;?>"/>
                                                </td>
                                            </tr>
                                        <?php
                                                }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">
                                                    <button type="submit" class="btn btn-success">Start Class</button>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                        </form>
                                </div>
                            </div>
                        </div>
                                        
                                            </div>

                                </div>
                            </div>
                        </div>
            <?php } ?>

            <?php if($class_document->num_rows() > 0){ ?>
                    <?php $cl = $class_document->row();?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Classes</h6>
                                    <div class="row">
                                        <div class="col-sm-7">
                            <div class="element-box">
                                <div class="element-wrapper">
                                    <h6 class="element-header">Class Notes and Videos</h6>
                                    <div class="text-right mb-4">
                                        <div class="btn btn-primary" onclick="printDiv();">Print</div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-6">
                                            <div class="profileImg">
                                                <?php $teacherImg = $this->custom->getTeacherImage($cl->teacher_id);
                                                $extT = substr($teacherImg, strrpos($teacherImg, '/') + 1);
                                                if ( $extT) {?>
                                                    <img  src="<?php echo $teacherImg ?>" >
                                                <?php }else{ ?>
                                                    <img  src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" >
                                                <?php } ?>

                                            </div>
                                             <div style="font-size: 16px;font-weight: 500;margin-top: 10px;">
                                                 <?php print $this->custom->getTeacherName($cl->teacher_id);?>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pull-right">
                                            <div class="profileImg">
                                                <?php
                                                $stdImage =$this->custom->getStudentImage($cl->student_id);
                                                $ext = substr($stdImage, strrpos($stdImage, '/') + 1);

                                                if ( $ext) {?>
                                                    <img  src="<?php echo $stdImage ?>" >
                                                <?php }else{ ?>
                                                    <img  src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" >
                                                <?php } ?>
<!--                                                <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg">-->
                                            </div>
                                            <div style="font-size: 16px;font-weight: 500;margin-top: 10px;">
                                                <?php print $this->custom->getStudentName($cl->student_id);?>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="clear: both;"></div>
                                    <div class="element-box-tp">
                        <form method="post" name="postform" action="<?php print base_url().'teacher/classes/updatenotes';?>" id="formValidate">
                            <input type="hidden" name="class_id" value="<?php print $this->uri->segment(4);?>" />
                            <div class="form-group">
                                <label style="font-size: 20px;font-weight: 500;margin-top: 10px;background: #ddd;width: 100%;padding:5px 10px;margin-bottom: 0px;">Class Notes</label>
                                <textarea cols="80" id="ckeditor1_1" name="class_notes" rows="10" style="visibility: hidden;"><?php print html_entity_decode($cl->class_notes);?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Video Title</label>
                                <input type="text" name="video_title" class="form-control" value="<?php print $cl->video_title;?>" />
                            </div>
                            <div class="form-group">
                                <label>Video Link 1</label>
                                <!-- <textarea name="video_link" class="form-control" rows="1"><?php print html_entity_decode($cl->video_link);?></textarea> -->
                                <input type="text" name="video_link" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Video Link 2</label>
                                <!-- <textarea name="video_link_2" class="form-control" rows="1"><?php print html_entity_decode($cl->video_link_2);?></textarea> -->
                                
                                <input type="text" name="video_link_2" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Video Link 3</label>
                                <!-- <textarea name="video_link_3" class="form-control" rows="1"><?php print html_entity_decode($cl->video_link_3);?></textarea> -->
                                <input type="text" name="video_link_3" class="form-control" />
                            </div>
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>            
                                    </div>
                                </div>
                            </div>                

                                        </div>
                                        <div class="col-sm-5">
                                            
                                            <div class="element-box">
                                                <div class="element-wrapper">
                                                    <h6 class="element-header">Student Attendance</h6>
                                                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target=".bd-example-modal-lg">Profile</button>
                                                    <div class="element-box-tp">
                                                        <div class="table-responsive">
                                                            <table class="table table-padded">
                                                                <tbody>
                                                                <?php 
                                                                        foreach($class_document->result() as $time){
                                                                ?>
                                                                    <tr>
                                                                        <td class="nowrap">
                                                                        <?php if ( $time->class_status == 1 ) { ?>    
                                                                            <span class="status-pill smaller green"></span>
                                                                            <span>Available</span>
                                                                        <?php } else { ?>
                                                                            <span class="status-pill smaller red"></span>
                                                                            <span>Not Available</span>
                                                                        <?php } ?>
                                                                        </td>
                                                                        <td><?php print $this->students_model->getNameById($time->student_id);?></td>
                                                                        <td><a href="<?php print base_url().'teacher/classes/student/'.encode_url($time->student_id);?>" target="_blank">View Lesson</a></td>
                                                                    </tr>
                                                                <?php
                                                                        }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
            <div id="printNote" style="display: none;">
                <div style="display: flex;justify-content: space-between;">
                    <div>
                        <div style="width: 100px;height: 100px;border-radius: 100%;border: 2px solid #ddd;overflow: hidden;">
                            <?php
                            if ( $extT) {?>
                                <img style="height: 100%;width: 100%;object-fit: cover;" src="<?php echo $teacherImg ?>" >
                            <?php }else{ ?>
                                <img style="height: 100%;width: 100%;object-fit: cover;" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" >
                            <?php } ?>
                            ?>
<!--                            <img  src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg">-->
                        </div>
                        <div style="font-size: 16px;font-weight: 500;margin-top: 10px;">
                            <?php print $this->custom->getTeacherName($cl->teacher_id);?>
                        </div>
                    </div>
                    <div>
                        <div style="width: 100px;height: 100px;border-radius: 100%;border: 2px solid #ddd;overflow: hidden;">
                            <?php
                            if ( $ext) {?>
                            <img style="height: 100%;width: 100%;object-fit: cover;" src="<?php echo $stdImage ?>" >
                            <?php }else{ ?>
                                <img style="height: 100%;width: 100%;object-fit: cover;" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" >
                            <?php } ?>
<!--                            <img  src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg">-->
                        </div>
                        <div style="font-size: 16px;font-weight: 500;margin-top: 10px;">
                            <?php print $this->custom->getStudentName($cl->student_id);?>
                        </div>
                    </div>
                </div>

                <?php print html_entity_decode($cl->class_notes);?></div>
            <?php } ?>
<?php $this->load->view('teacher/footer');?>	
<!-- Profile Modal -->
<div class="modal fade bd-example-modal-lg custom-modal-desgin" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Student Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="student-profile">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="card shadow-sm">
          <div class="card-header bg-transparent text-center">
            <div class="profile_img">
              <?php
              if ( $ext) {?>
                  <img class="" style="height: 100%;width: 100%;object-fit: cover;" src="<?php echo $stdImage ?>" >
              <?php }else{ ?>
                  <img class="" style="height: 100%;width: 100%;object-fit: cover;" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" alt="">
              <?php } ?>
              </div>
            <h3>
                <?php print $this->custom->getStudentName($cl->student_id);?>
            </h3>
          </div>
          <div class="card-body">
            <!-- <p class="mb-0"><strong class="pr-1">Class:</strong>4</p>
            <p class="mb-0"><strong class="pr-1">Level:</strong>A</p> -->
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card shadow-sm">
          <div class="card-header bg-transparent border-0">
            <h3 class="">Student Information</h3>
          </div>
          <div class="card-body pt-0">
            <p>
                <?php print $this->custom->getStudentInfo($cl->student_id);?>

            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>