<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Students_request extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->custom->user_id() > 0) {
		} else {
			redirect('login');
		}
		$this->load->model('students_model');
		$this->load->model('subjects_model');
		$this->load->model('teachers_model');
		$this->load->model('schdule_model');
		$this->load->model('school_model');
	}

	public function index()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1){
					$limit = 15;
					// $total_result = $this->students_model->get();
					// $data['total_items']  = $total_result->num_rows();
					
					$data['total_items'] = $this->db->count_all_results('student_request');
					$config['base_url'] = base_url() . $this->uri->segment(1) . '/index/';
					$config['total_rows'] = $data['total_items'];
					$config['per_page'] = $limit;
					$config['uri_segment'] = 3;
					// First Links
					$config['first_link'] = 'First';
					$config['first_tag_open'] = '<li>';
					$config['first_tag_close'] = '</li>';
					// Last Links
					$config['last_link'] = 'Last';
					$config['last_tag_open'] = '<li>';
					$config['last_tag_close'] = '</li>';
					// Next Link
					$config['next_link'] = '&raquo;';
					$config['next_tag_open'] = '<li>';
					$config['next_tag_close'] = '</li>';
					// Previous Link
					$config['prev_link'] = '&laquo;';
					$config['prev_tag_open'] = '<li>';
					$config['prev_tag_close'] = '</li>';
					// Current Link
					$config['cur_tag_open'] = '<li class="active"><a>';
					$config['cur_tag_close'] = '</a></li>';
					// Digit Link
					$config['num_tag_open'] = '<li>';
					$config['num_tag_close'] = '</li>';
					$this->pagination->initialize($config);
					$offset = $this->uri->segment(3);
					$this->db->limit($limit, $offset);
					$data['pagination'] = $this->pagination->create_links();
					// $data['users'] = $this->students_model->get(array('sort_by' => 'id'));
					$data['users'] = $this->db->order_by('id','desc')
												  ->get('student_request');
					$this->load->view('admin/student_request/index', $data);
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}


	public function confirm()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 ){
				$sql = $this->db->where('id',$this->uri->segment(3))
								->get('student_request')->row();

				$teacher_id = $this->teachers_model->getTeacherBySubId($sql->subject);				

				$post = array('subject_id'=>$sql->subject,'teacher_id'=>$teacher_id);
				$this->schdule_model->add($post);
				$ids = $this->db->insert_id();
				
				$timefrom = $sql->slottime + 12;
				$timeto = $timefrom + 1;
				
				$schdule_date = date('m/d/Y', strtotime($sql->slotdate));
				$timepickerfrom = date('H:i:s', strtotime($timefrom.':00:00'));
				$timepickerto = date('H:i:s', strtotime($timeto.':00:00'));

				if( $this->getDate($schdule_date,$ids) > 0){
					$date_id = $this->getDate($schdule_date,$ids);
				} else {
					$this->db->insert('schdule_date', array(
																'schdule_date_id'=>$ids,
																'subject_id' => $subject_id,
																'teacher_id' => $teacher_id,
																'dates'=>$schdule_date,
																'created'=>date('Y-m-d H;i:s')
														));
					$date_id = $this->db->insert_id();
				}
						
				$data = array(
							'subject_id' => $sql->subject,
							'teacher_id' => $teacher_id,
							'schdule_id' => $ids,
							'date_id' => $date_id,
							'schdule_date' => $schdule_date,
							'timepickerfrom' => $timepickerfrom,
							'timepickerto' => $timepickerto,
							'noofstudent' => 1,
							'expiry_time' => date('h:i', strtotime($timepickerto)),
							'created' => date('Y-m-d H:i:s')
						 );



				$this->db->insert('schdule_time',$data);
				
				$slot_ids = $this->db->insert_id();
				// add student
				$datas = array(
					'student_id' => $sql->student_id,
					'slotdate' => date('Y-m-d', strtotime($schdule_date)),
					'slotid' => $date_id,
					'subject_id' => $sql->subject,
					'teacher_id' => $teacher_id,
					'school_id'	=>	$sql->school,
					'slottype' => 'booking',
					'timeslot' => $slot_ids,
					'timefrom' => date('H:i:s', strtotime($timepickerfrom)),
					'slotstatus' => 1,
					'created' => date('Y-m-d H:i:s')
				);



				$this->db->insert('student_booking', $datas);
									

				// ........... Send Email

				$student_name = $this->students_model->getFnameById($sql->student_id);
				$subject_name = $this->subjects_model->getNameById($sql->subject);
				$teacher_name = $this->teachers_model->getNameById($teacher_id);
				$school_name = $this->school_model->getSchoolById($sql->school);
				$postdata['message'] = '<table>
										<tr>
											<td>Student Name: </td>
											<td>'.$student_name.'</td>
										</tr>
										<tr>
											<td>Subject Name: </td>
											<td>'.$subject_name.'</td>
										</tr>
										<tr>
											<td>Teacher Name: </td>
											<td>'.$teacher_name.'</td>
										</tr>
										<tr>
											<td>School Name: </td>
											<td>'.$school_name.'</td>
										</tr>
										<tr>
											<td>Date</td>
											<td>'.date('Y-m-d H:i:s').'</td>
										</tr>
										<tr>
											<td>Slot date: </td>
											<td>'.$schdule_date.' From '.$timepickerfrom.' To '.$timepickerto.'</td>
										</tr>
										<tr>
											<td></td>
											<td>Your Lesson has been Booked.</td>
										</tr>
									</table>';

				$st_email = $this->students_model->getEmailById($sql->student_id);
				$this->custom->SendMail('info@columbia-academy.jp',$st_email,'Student Class Booking',$postdata,'booking');


				// .......................................

				$this->db->where('id', $this->uri->segment(3))->delete('student_request');


				$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'Class Successfully created.', 'type' => 'message'));
				redirect($this->uri->segment(1));

			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
		// }

	}

	public function delete()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1){
				$this->db->where('id', $this->uri->segment(3))->delete('student_request');
				$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'Class Request Successfully deleted.', 'type' => 'message'));
				redirect($this->uri->segment(1));
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}


	private function getDate($date,$subject_id)
	{
		$sql = $this->db->where('dates',$date)
						->where('subject_id',$subject_id)
						->get('schdule_date');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->id;
		}
		return false;
	}


}
