<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('subjects_model');
		$this->load->model('schdule_model');
		$this->load->model('teachers_model');
	}
	
	public function index()
	{
	   if($this->permission_model->getAllPerm('time_slot_booking',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('time_slot_booking',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getEditPerm('time_slot_booking',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getViewPerm('time_slot_booking',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getDelPerm('time_slot_booking',$this->custom->getUserProfile())== 1){
					$this->load->view('admin/booking/index');
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}

	
}
