<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('logged_in') != ""){
		
		}else{
		    redirect('login');
		}
		$this->load->model('profile_model');
	}
	
	public function index()
	{
		$data['sql'] = $this->profile_model->get();
		$this->load->view('admin/profile/index',$data);
	}
	
	
	public function add()
	{
		$this->load->view('admin/profile/add');
	}
	
	
	public function insert(){
		$filter = $this->custom->filter_all();	
		if(strlen($filter['post']['name']) == 0){
			$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Please fill all field.', 'type' => 'message' )); 
			redirect($this->uri->segment(1).'/add');
		}else{
			$this->profile_model->add($filter['post']);
			$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'New profile Created.', 'type' => 'message' )); 
			redirect($this->uri->segment(1));
		}
	}
	
	public function edit()
	{
		$data['sql'] = $this->profile_model->get(array('id'=>$this->uri->segment(3)));
		$this->load->view('admin/profile/edit',$data);
	}
	
	
	public function update(){
		$filter = $this->custom->filter_all();	
		if(strlen($filter['post']['name']) == 0){
			$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Please fill all field.', 'type' => 'message' )); 
			redirect($this->uri->segment(1).'/edit/'.$filter['post']['id']);
		}else{
			$this->profile_model->update($filter['post']);
			$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Profile Updated Created.', 'type' => 'message' )); 
			redirect($this->uri->segment(1));
		}
	}
	
	public function delete(){
		$this->profile_model->id = $this->uri->segment(3);
		$this->profile_model->delete($this->profile_model->id);
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Profile Deleted.', 'type' => 'message' )); 	
		redirect($this->uri->segment(1));
	}
	
	/*
	* permission Controller
	*/
	
	public function permission(){			 
		$data['query'] = $this->profile_model->get(array('id'=>$this->uri->segment(3)));
		$this->db->order_by("name", "asc");
		$data['allroles'] = $this->db->get('users_right');
		$data['roletype'] = $this->profile_model->getPerm($this->uri->segment(3));
		$this->load->view('admin/profile/permission',$data);
	}
	
	public function permission_update(){
	 	$this->db->delete('users_permission', array('pid'=>$this->input->post('hid')));
		$insert_id = $this->input->post('hid');
		if($insert_id != ""){
			foreach($_POST as $post => $also){
				if($post == 'name' or $post == 'Submit' or $post == 'hid'){
				
				} else {
					$insert = ($also['addp']!= '') ? $also['addp'] : '0';
					$edit = ($also['editp']!= '') ? $also['editp'] : '0';
					$view = ($also['view']!= '') ? $also['view'] : '0';
					$delete = ($also['deletep'] != '') ? $also['deletep'] : '0';
					$viewall = ($also['allview'] != '') ? $also['allview'] : '0';
					$role_data = array(
						'pid' => $insert_id,
						'pright' => $post,
						'addp' => $insert,
						'view' =>	$view,
						'editp' => 	$edit,
						'deletep' =>	$delete,
						'allview' =>	$viewall,
						'created' => date('Y-m-d')
					);		
					$this->db->insert('users_permission', $role_data);
				}
			}
		} 
		redirect($this->uri->segment(1).'/permission/'.$insert_id);
	}
	
	
	
}

