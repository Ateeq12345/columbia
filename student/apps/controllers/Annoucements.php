<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Annoucements extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->custom->user_id() > 0) {
			$this->load->model('annoucement_model');
		} else {
			redirect('login');
		}
		
	}

	public function index()
	{
		$data['sql'] = $this->db->get('annoucements');
		$this->load->view('admin/annoucement/index',$data);

	}
	public function saveAnnoucements(){
		$array = array('date'=>date('Y-m-d',strtotime($this->input->post('date'))),'title'=>$this->input->post('title'));
	    $annoucement = new Annoucement_model();
        $annoucement->insert($array);
        redirect('/');
	}


}
