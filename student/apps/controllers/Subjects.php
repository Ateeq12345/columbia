<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('subjects_model');
	}
	
	public function index()
	{
		if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('subjects',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getEditPerm('subjects',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getViewPerm('subjects',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getDelPerm('subjects',$this->custom->getUserProfile())== 1){
					$data['users'] = $this->subjects_model->get(array('sort_by'=>'created'));
					$this->load->view('admin/subjects/index',$data);
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}
	
	public function add()
	{
		if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('subjects',$this->custom->getUserProfile())== 1 ){
				$this->load->view('admin/subjects/add');
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}
	
	public function insert()
	{
		if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('subjects',$this->custom->getUserProfile())== 1 ){
				$filter = $this->custom->filter_all();
				$this->subjects_model->add($filter['post']);
				$ids = $this->db->insert_id();

				if($_FILES['fimg']['size'] != 0){	
					$path = './upload/subjects/';
					$file_type = $_FILES['fimg']['type'];
					$allowed = array("image/png", "image/gif" ,"image/jpg" ,"image/jpeg");
					if(!in_array($file_type, $allowed)) {
						$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Only gif | jpg | png | jpeg Files Allow.', 'type' => 'message' )); 
						redirect($this->uri->segment(1).'/subjects/edit/'.$ids);
					}else{	
						$image_data = $this->custom->upload_image('fimg',$path);
						$upload = $image_data['file_name'];
						$opt = [
									'id' => $ids,
									'fimg' => $upload
								];
						$this->subjects_model->update($opt);
					}
				}

				$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully added.', 'type' => 'message' )); 
				redirect($this->uri->segment(1));
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}
	
	public function edit()
	{
		if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getEditPerm('subjects',$this->custom->getUserProfile())== 1 ){
				$data['user'] = $this->subjects_model->get(array('id'=>$this->uri->segment(3)));
				$this->load->view('admin/subjects/edit',$data);
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}
	
	public function update()
	{
		if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getEditPerm('subjects',$this->custom->getUserProfile())== 1 ){
				$filter = $this->custom->filter_all();
				$data = array(
								'id' => $filter['post']['id'],
								'color_code' => $filter['post']['color_code'],
								'colors' => $filter['post']['colors'],
								'name' => $filter['post']['name']
							);
				$this->subjects_model->update($data);
				
				if($_FILES['fimg']['size'] != 0){	
					$path = './upload/subjects/';
					$file_type = $_FILES['fimg']['type'];
					$allowed = array("image/png", "image/gif" ,"image/jpg" ,"image/jpeg");
					if(!in_array($file_type, $allowed)) {
						$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Only gif | jpg | png | jpeg Files Allow.', 'type' => 'message' )); 
						redirect($this->uri->segment(1).'/subjects/edit/'.$filter['post']['id']);
					}else{	
						$image_data = $this->custom->upload_image('fimg',$path);
						$upload = $image_data['file_name'];
						$opt = [
									'id' => $filter['post']['id'],
									'fimg' => $upload
								];
						$this->subjects_model->update($opt);
					}
				}

				$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully updated.', 'type' => 'message' )); 
				redirect($this->uri->segment(1));
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}
	
	public function delete()
	{
		if($this->permission_model->getAllPerm('subjects',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getDelPerm('subjects',$this->custom->getUserProfile())== 1){
				$this->subjects_model->delete($this->uri->segment(3));
				$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully deleted.', 'type' => 'message' )); 
				redirect($this->uri->segment(1));
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}
	
}
