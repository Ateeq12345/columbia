<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Classes extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->custom->teacher_user_id() > 0) {
		} else {
			redirect('login');
		}
		$this->load->model('schdule_model');
		$this->load->model('subjects_model');
		$this->load->model('students_model');
	}

	public function index()
	{
		$teacher_id = $this->custom->teacher_login_user_id();
		$this->getResult($teacher_id);
		// $data['sql'] = $this->db->select('any_value(booking_id) as booking_id,
		// 								   any_value(student_id) as student_id,
		// 								   any_value(subject_id) as subject_id,
		// 								   any_value(teacher_id) as teacher_id,
		// 								   any_value(school_id) as school_id,
		// 								   any_value(slotdate) as slotdate,
		// 								   any_value(slotid) as slotid,
		// 								   timeslot as timeslot,
		// 								   any_value(slottype) as slottype,
		// 								   any_value(timefrom) as timefrom,
		// 								   any_value(timeto) as timeto,
		// 								   any_value(slotstatus) as slotstatus,
		// 								   any_value(class_status) as class_status,
		// 								   any_value(class_notes) as class_notes,
		// 								   any_value(video_title) as video_title,
		// 								   any_value(video_link) as video_link,
		// 								   any_value(created) as created')

		$data['sql'] = $this->db->select('*')

			//->group_by('timeslot')
			->where('teacher_id', $teacher_id)
			->where('is_canceled', 0)
			->order_by('slotdate', "desc")
			->get('student_booking');
		$this->load->view('teacher/class/index', $data);
	}

	public function getResult($teacher_id)
	{
		$date = date('Y-m-d');
		$sql = $this->db
			->where('slotdate >', $date)
			->where('is_canceled', 0)
			->get('student_booking');
		if ($sql->num_rows() > 0) {
			foreach ($sql->result() as $row) {
				$this->db->where('booking_id', $row->booking_id)->update('student_booking', array('slotstatus' => 1));
			}
		}
	}

	public function edit()
	{
		$data['class_id'] = $this->uri->segment(4);
		$data['class'] = $this->db->where('booking_id', $data['class_id'])
			->where('slotstatus', 1)
			->where('is_canceled', 0)
			->get('student_booking');
		$data['class_document'] = $this->db->where('booking_id', $data['class_id'])
			->where('slotstatus', 2)
			->get('student_booking');
		$this->load->view('teacher/class/edit', $data);
	}

	public function update()
	{
		$class_id = trim(strip_tags($this->input->post('class_id')));
		$student =  $this->input->post('student');
		for ($i = 0; $i < count($student); $i++) {
			$single = explode('_', $student[$i]);
			$this->db->where('booking_id', $class_id)
				->where('student_id', $single[0])
				->where('booking_id', $single[1])
				->update('student_booking', array('class_status' => 1));
		}
		$this->db->where('booking_id', $class_id)
			->update('student_booking', array('slotstatus' => 2));
		redirect(base_url() . 'teacher/classes/edit/' . $class_id);
	}

	public function updatenotes()
	{	

		$class_id = trim(strip_tags($this->input->post('class_id')));
		$class_notes = trim(htmlentities($this->input->post('class_notes')));
		$video_title = trim(strip_tags($this->input->post('video_title')));
		$video_link = trim(strip_tags($this->input->post('video_link')));
		$video_link_2 = trim(strip_tags($this->input->post('video_link_2')));
		$video_link_3 = trim(strip_tags($this->input->post('video_link_3')));

		$array = array(
				'class_notes' => $class_notes,
				'video_title' => $video_title
			);
		if($video_link!=null && $video_link!="" && $video_link!='' )
		{
			$array['video_link'] = $video_link;
		}
		if($video_link_2!=null && $video_link_2!="" && $video_link_2!='' )
		{
			$array['video_link_2'] = $video_link_2;
		}
		if($video_link_3!=null && $video_link_3!="" && $video_link_3!='' )
		{
			$array['video_link_3'] = $video_link_3;
		}
		// print_r($array);
		// die;
		$this->db->where('booking_id', $class_id)
			->update('student_booking', $array);
		$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'Class Successfully Updated.', 'type' => 'message'));
		// redirect(base_url() . 'teacher/classes/');
		redirect(base_url().$this->uri->segment(1).'/classes/edit/'.trim(strip_tags($this->input->post('class_id'))));
	}

	public function student()
	{
		$id = decode_url($this->uri->segment(4));
		$data['class_document'] = $this->db->where('student_id', $id)
										   ->where('slotstatus', 2)
										   ->where('class_notes !=', "")
										   ->order_by('booking_id desc')
										   ->limit(1,1)
										   ->get('student_booking');
		$this->load->view('teacher/class/view', $data);
	}

}
