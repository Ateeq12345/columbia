<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schdule extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('subjects_model');
		$this->load->model('schdule_model');
		$this->load->model('teachers_model');
		$this->load->model('students_model');
		$this->load->model('school_model');
	}
	
	public function index()
	{

		if($this->permission_model->getAllPerm('class_schdule',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('class_schdule',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getEditPerm('class_schdule',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getViewPerm('class_schdule',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getDelPerm('class_schdule',$this->custom->getUserProfile())== 1){
					$d = isset( $_GET['d']) ? $_GET['d'] : '';
					if($d){
						$data['date'] = urldecode($d);
						$data['subjects'] = $this->subjects_model->get();
						$data['teacher'] = $this->teachers_model->get();
						$data['school'] = $this->school_model->get();
						$data['users'] = $this->db->where('schdule_date',$data['date'])
												  ->order_by('timepickerfrom','asc')
												  // ->order_by('id','desc')
												  ->get('schdule_time');
						$this->load->view('admin/schdule/index',$data);
					} else {
						$data['date'] = date('m/d/Y');
						$data['subjects'] = $this->subjects_model->get();
						$data['teacher'] = $this->teachers_model->get();
						$data['school'] = $this->school_model->get();
						$data['users'] = $this->db->where('schdule_date',$data['date'])
												  ->order_by('timepickerfrom','asc')
												  // ->order_by('id','desc')
												  ->get('schdule_time');
						$this->load->view('admin/schdule/index',$data);
					}
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}
	
	public function addclass()
	{
		$filter = $this->custom->filter_all();
		$subject_id = $filter['post']['subject_id'];
		$teacher_id = $filter['post']['teacher_id'];
		$school_id = $filter['post']['school_id'];
		$schdule_date = $filter['post']['schdule_date'];
		$timepickerfrom = $filter['post']['timepickerfrom'];
		$post = array('subject_id'=>$subject_id,'teacher_id'=>$teacher_id,'school_id' => $school_id);
		$this->schdule_model->add($post);
		$ids = $this->db->insert_id();
		
		$this->db->insert('schdule_date', array(
													'schdule_date_id'=>$ids,
													'subject_id' => $subject_id,
													'teacher_id' => $teacher_id,
													'school_id' => $school_id,
													'dates'=>$schdule_date,
													'created'=>date('Y-m-d H;i:s'
												)
											));
		$date_id = $this->db->insert_id();
		
		$data = array(
						'subject_id' => $subject_id,
						'teacher_id' => $teacher_id,
						'school_id' => $school_id,
						'schdule_id' => $ids,
						'date_id' => $date_id,
						'schdule_date' => $schdule_date,
						'timepickerfrom' => $timepickerfrom,
						'expiry_time' => date('h:i', strtotime($timepickerfrom)),
						'created' => date('Y-m-d H:i:s')
					 );
		$this->db->insert('schdule_time',$data);
		print $this->db->insert_id();
	}

	public function add()
	{
		$data['subjects'] = $this->subjects_model->get();
		$data['teacher'] = $this->teachers_model->get();
		$this->load->view('admin/schdule/add',$data);
	}
	
	public function insert()
	{
		$subject_id = trim(strip_tags($this->input->post('subject_id')));
		$teacher_id = trim(strip_tags($this->input->post('teacher_id')));
		$schdule_date = $this->input->post('schdule_date');
		$timepickerfrom = $this->input->post('timepickerfrom');
		$timepickerto = $this->input->post('timepickerto');
		$noofstudent = $this->input->post('noofstudent');
		$post = array('subject_id'=>$subject_id,'teacher_id'=>$teacher_id);
		$this->schdule_model->add($post);
		$ids = $this->db->insert_id();
		for($i=0;$i < count($schdule_date); $i++ ){
			if( $this->getDate($schdule_date[$i],$ids) > 0){
				$date_id = $this->getDate($schdule_date[$i],$ids);
			} else {
				$this->db->insert('schdule_date', array(
															'schdule_date_id'=>$ids,
															'subject_id' => $subject_id,
															'teacher_id' => $teacher_id,
															'dates'=>$schdule_date[$i],
															'created'=>date('Y-m-d H;i:s'
														)
													));
				$date_id = $this->db->insert_id();
			}
			$data = array(
							'subject_id' => $subject_id,
							'teacher_id' => $teacher_id,
							'schdule_id' => $ids,
							'date_id' => $date_id,
							'schdule_date' => $schdule_date[$i],
							'timepickerfrom' => $timepickerfrom[$i],
							'timepickerto' => $timepickerto[$i],
							'noofstudent' => $noofstudent[$i],
							'expiry_time' => date('h:i', strtotime($timepickerto[$i])),
							'created' => date('Y-m-d H:i:s')
						 );
			$this->db->insert('schdule_time',$data);
		}
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Schdule Successfully added.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function edit()
	{
		$data['user'] = $this->schdule_model->get(array('id'=>$this->uri->segment(3)));
		$data['subjects'] = $this->subjects_model->get();
		$data['teacher'] = $this->teachers_model->get();
		$this->load->view('admin/schdule/edit',$data);
	}
	public function updateTeacher(){
    	// print_r($this->input->post());
		// die;
	    $arr = array(
            'id' => trim(strip_tags($this->input->post('id'))),
        'subject_id' => trim(strip_tags($this->input->post('subject_id'))),
        'teacher_id' => trim(strip_tags($this->input->post('teacher_id')))
    );
	    $arr_2 = array(
            'timeslot' => trim(strip_tags($this->input->post('id'))),
        
        'teacher_id' => trim(strip_tags($this->input->post('teacher_id')))
    );

        $schedule= new Schdule_model();
        $schedule->updateTeacher($arr);
        $schedule->updateTeacherInStudentBooking($arr_2);
        redirect($this->uri->segment(1));
    }
	public function updateSchool(){
    	// print_r($this->input->post());
		// die;
	    $arr = array(
        'id' => trim(strip_tags($this->input->post('id'))),
        'subject_id' => trim(strip_tags($this->input->post('subject_id'))),
        'school_id' => trim(strip_tags($this->input->post('school_id')))
    );
	    $arr_2 = array(
        'timeslot' => trim(strip_tags($this->input->post('id'))),
        'school_id' => trim(strip_tags($this->input->post('school_id')))
    );
//print_r($arr_2);
//print_r($arr);
//die;
        $schedule= new Schdule_model();
        $schedule->updateTeacher($arr);
        $schedule->updateTeacherInStudentBooking($arr_2);
        redirect($this->uri->segment(1));
    }
    public function studentUpdate(){

	    $arr = array(
            'booking_id' => trim(strip_tags($this->input->post('booking_id'))),
        'student_id' => trim(strip_tags($this->input->post('student_id'))),
    );


        $schedule= new Schdule_model();
        $schedule->studentUpdate($arr);
        redirect($this->uri->segment(1));
    }
	public function update()
	{

		$id = trim(strip_tags($this->input->post('id')));
		$subject_id = trim(strip_tags($this->input->post('subject_id')));
		$teacher_id = trim(strip_tags($this->input->post('teacher_id')));
		$school_id = trim(strip_tags($this->input->post('school_id')));
		$schdule_date = $this->input->post('schdule_date');
		$timepickerfrom = $this->input->post('timepickerfrom');
		$timepickerto = $this->input->post('timepickerto');
		$noofstudent = $this->input->post('noofstudent');
		$post = array('id'=>$id,'subject_id'=>$subject_id,'teacher_id'=>$teacher_id,'school_id' => $school_id);
		$this->schdule_model->update($post);
		$ids = $id;
		$this->db->where('schdule_id',$ids)->delete('schdule_time');
		$this->db->where('subject_id',$ids)->delete('schdule_date');
		for($i=0;$i < count($schdule_date); $i++ ){
			if( $this->getDate($schdule_date[$i],$ids) > 0){
				$date_id = $this->getDate($schdule_date[$i],$ids);
			} else {
				$this->db->insert('schdule_date', array(
															'schdule_date_id'=>$ids,
															'subject_id' => $subject_id,
															'teacher_id' => $teacher_id,
															'school_id' => $school_id,
															'dates'=>$schdule_date[$i],
															'created'=>date('Y-m-d H;i:s'
														)
													));
				$date_id = $this->db->insert_id();
			}
			$data = array(
							'subject_id' => $subject_id,
							'teacher_id' => $teacher_id,
							'school_id' => $school_id,
							'schdule_id' => $ids,
							'date_id' => $date_id,
							'schdule_date' => $schdule_date[$i],
							'timepickerfrom' => $timepickerfrom[$i],
							'timepickerto' => $timepickerto[$i],
							'noofstudent' => $noofstudent[$i],
							'expiry_time' => date('H:i A', strtotime($timepickerto[$i])),
							'created' => date('Y-m-d H:i:s')
						 );
			$this->db->insert('schdule_time',$data);
		}
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Schdule Successfully added.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function delete()
	{
		$this->db->where('id',$this->uri->segment(3))
				 ->delete('schdule_time');
		$this->db->where('id',$this->input->get('schdule_id'))
				 ->delete('schdule');
		$this->db->where('id',$this->input->get('date_id'))
				 ->delete('schdule_date');
		$this->db->where('timeslot',$this->uri->segment(3))
				 ->delete('student_booking');
		// die();
		// $this->schdule_model->delete($this->input->get('schdule_id'));
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Subject Successfully deleted.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	private function getDate($date,$subject_id)
	{
		$sql = $this->db->where('dates',$date)
						->where('subject_id',$subject_id)
						->get('schdule_date');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->id;
		}
		return false;
	}

	public function addstudent()
	{
		$data[] = '';
		$this->load->view('admin/schdule/addstudent',$data);
	}

	public function searchstudent()
	{
		$q = $this->input->get('term');
		$result = array();
		$sql = $this->db->like('fname',$q)
						->or_like('phone',$q)
						->where('admission',2)
						->where('subject_id', $this->uri->segment(4))
						->get('students');
		if($sql->num_rows() > 0){
			foreach($sql->result() as $row){
				$result[] = array('code'=>$row->id,'name'=>$row->fname .' '. $row->lname);
			}
			print json_encode($result);
		}
	}

	public function searchstudentnew()
	{
		$q = trim(strip_tags($this->input->post('student')));
		$result = array();
		$sql = $this->db->like('fname',$q)
						->or_like('phone',$q)
						->where('admission',2)
						->get('students');
		if($sql->num_rows() > 0){
			foreach($sql->result() as $row){
				$result[] = array('value'=>$row->id,'label' => $row->fname .' '. $row->lname, 'phone'=>$row->phone);
			}
			print json_encode($result);
		}	
	}

	public function poststudent()
	{
		$filter = $this->custom->filter_all();
		$sql = $this->db->like('id',$filter['post']['class_ids'])
						->get('schdule_time');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			$data = array(
							'student_id' => $filter['post']['student_ids'],
							'slotdate' => date('Y-m-d', strtotime($row->schdule_date)),
							'slotid' => $row->date_id,
							'subject_id' => $row->subject_id,
							'teacher_id' => $row->teacher_id,
							'school_id' => $row->school_id,
							'slottype' => 'booking',
							'timeslot' => $row->id,
							'timefrom' => date('H:i:s', strtotime($row->timepickerfrom)),
							'slotstatus' => 1,
							'created' => date('Y-m-d H:i:s')
						);
			$this->db->insert('student_booking',$data);
			print $this->db->insert_id();
		}
	}

	public function deletestudent()
	{
		$student_id = $this->input->get('student_id');	
		$this->db->where('booking_id',$student_id)
				 ->delete('student_booking');
		return true;
	}

	public function cancelstudent()
	{
		$student_id = $this->input->get('student_id');	
		$classids = $this->input->get('classids');	
		$this->db->where('booking_id', $student_id)
				 ->update('student_booking', array( 'is_canceled' => 1, 'canceled_at' => date('Y-m-d H:i:s'), 'canceled_ip' => $this->input->ip_address() ));
		$this->custom->addActivity($classids,'admin',1,$this->custom->user_id(), get_current_user());
		return true;
	}

	public function getTeacherList()
	{
		$result = array();
		$sql = $this->teachers_model->get(array('subject_id' => $this->input->get('subject_id') ));
		if($sql->num_rows() > 0){
			foreach($sql->result() as $row){
				$result[] = '<option value="'.$row->id.'">'.$row->fname . ' ' . $row->lname .'</option>';
			}
		}
		print json_encode($result);
	}
	public function getSchoolList()
	{
		$result = array();
		$sql = $this->school_model->get(array('subject_id' => $this->input->get('subject_id') ));
		if($sql->num_rows() > 0){
			foreach($sql->result() as $row){
				$result[] = '<option value="'.$row->id.'">'.$row->school_name .'</option>';
			}
		}
		print json_encode($result);
	}

	public function getUserActivity()
	{
		$class_id = $this->input->get('class_id');
		$sql = $this->db->where('class_id',$class_id)
						->get('activity');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			$html = '<table class="table table-bordered">
						<tr>
							<td>Canceled By</td>
							<td>'.ucfirst($row->user_type).'</td>
						</tr>
						<tr>
							<td>Username</td>';
						if ( $row->user_type == 'admin' ) {
					$html .= '<td>'.$this->custom->getAdminUserById($row->user_id).'</td>';
						} else {
					$html .= '<td>'.$this->custom->getStudentUserById($row->user_id).'</td>';
						}
				$html .= '</tr>
						<tr>
							<td>Ip Address</td>
							<td>'.$row->ip_id.'</td>
						</tr>
						<tr>
							<td>Entry Date</td>
							<td>'.$row->created.'</td>
						</tr>

					</table>';
			print json_encode($html);
		} else {
			print 0;
		}

	}

}
