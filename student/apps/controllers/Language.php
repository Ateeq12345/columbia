<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('language_model');
	}
	
	public function index()
	{
		$limit = 15;
		$total_result = $this->language_model->get();
		$data['total_items']  = $total_result->num_rows();
		$config['base_url'] = base_url().$this->uri->segment(1).'/index/';
		$config['total_rows'] = $data['total_items'];
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		// First Links
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		// Last Links
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		// Next Link
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		// Previous Link
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		// Current Link
		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		// Digit Link
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$offset = $this->uri->segment(3);
		$this->db->limit($limit, $offset);
		$data['pagination'] = $this->pagination->create_links();
        $data['users'] = $this->language_model->get();
		$this->load->view('admin/language/index',$data);
	}
	
	public function add()
	{
		$this->load->view('admin/language/add');
	}
	
	public function insert()
	{
		$filter = $this->custom->filter_all();
		if( $filter['post']['eng'] == "" || $filter['post']['jap'] == "" ) {
			$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Please fill all fields.', 'type' => 'message' )); 
			redirect($this->uri->segment(1).'/add');
		} else {		
			$this->language_model->add($filter['post']);
			$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'New Language Successfully added.', 'type' => 'message' )); 
			redirect($this->uri->segment(1));
		}
	}
	
	public function edit()
	{
		$data['lang'] = $this->language_model->get(array('id'=>$this->uri->segment(3)));
		$this->load->view('admin/language/edit',$data);
	}
	
	public function update()
	{
		$filter = $this->custom->filter_all();
		$this->language_model->update($filter['post']);
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Language Successfully updated.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
	
	public function delete()
	{
		$this->language_model->delete($this->uri->segment(3));
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Language Successfully deleted.', 'type' => 'message' )); 
		redirect($this->uri->segment(1));
	}
    
    public function lang()
    {
        return $this->session->set_userdata('lang',$_POST['lang']);
        // redirect(base_url());
    }
    
	
}
