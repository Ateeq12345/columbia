<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}

		$this->load->model('students_model');
		$this->load->model('subjects_model');
		$this->load->model('teachers_model');
		$this->load->model('schdule_model');

	}
	
	public function index()
	{
	
		// echo $getAllSchool;
		// die;
		
		
		// $this->custom->SendMail('info@columbia-academy.jp','farziali2000@gmail.com','Student Cancel Booking','','cancel');
		$this->load->view('admin/index');
	}
}
