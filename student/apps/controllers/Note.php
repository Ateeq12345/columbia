<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Note extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if ($this->custom->user_id() > 0) {
        } else {
            redirect('login');
        }
        $this->load->model('notes_model');
        $this->load->model('subjects_model');
        $this->load->model('teachers_model');
        $this->load->model('schdule_model');
    }
    public function insert(){
        $filter = $this->custom->filter_all();
      $formData =   array(
            'student_id' => isset($filter['post']['student_id'])?$filter['post']['student_id']:null,
            'teacher_id' => isset($filter['post']['teacher_id'])?$filter['post']['teacher_id']:null,
            'date' => $filter['post']['date'],
            'note' => $filter['post']['note'],
            'id' => $filter['post']['id'],
            'booking_id' => $filter['post']['booking_id'],
        );
    $notes = new Notes_model();
        $notes->insert($formData);
        redirect('booking/');
    }
    public function updateSchool(){
        $filter = $this->custom->filter_all();
        $arr = array(
            'id' => isset($filter['post']['slotId'])?$filter['post']['slotId']:null,
            'subject_id' => isset($filter['post']['subject_id'])?$filter['post']['subject_id']:null,
            'school_id' => isset($filter['post']['school_id'])?$filter['post']['school_id']:null,
             );
        $arr_2 = array(
            'school_id' => isset($filter['post']['school_id'])?$filter['post']['school_id']:null,
            'booking_id' => isset($filter['post']['booking_id'])?$filter['post']['booking_id']:null,
        );
//        print_r($arr_2);
//        print_r($arr);
//        die;
        $schedule= new Schdule_model();
        $schedule->updateSchool($arr);
        $schedule->updateSchoolInStudentBooking($arr_2);
        redirect('booking/');
    }

}
?>