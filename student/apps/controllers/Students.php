<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Students extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
//
//        print_r($this->custom->getUserProfile());
//die;

		if ($this->custom->user_id() > 0) {
		} else {
			redirect('login');
		}
		$this->load->model('students_model');
		$this->load->model('subjects_model');
		$this->load->model('teachers_model');
		$this->load->model('schdule_model');
	}

	public function index()
	{

        if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1){
					$limit = 15;
                    $offset = $this->uri->segment(3);
                    $this->db->limit($limit, $offset);
                    if ( $this->custom->getUserProfile() == 2 || $this->custom->getUserProfile() == "2" ||  $this->custom->getUserProfile() == '2') {
						$data['users'] = $this->db->limit($limit, $offset)
												  ->order_by('id','desc')
												  ->get('students');
                         $data['total_items'] = $this->db->count_all_results('students');
                        	 }  else {
					 	$data['users'] = $this->db->where('entry_user',$this->custom->user_id())
					 							  ->limit($limit, $offset)
					 							  ->order_by('id','desc')
					 							  ->get('students');
                         $data['total_items'] = $this->db->where('entry_user',$this->custom->user_id())->count_all_results('students');
					 	 }
//                    if ( $this->custom->getUserProfile() == 2 ) {
//						 $data['users'] = $this->students_model->limit($limit, $offset)->get(array('sort_by' => 'id'));
//						$data['total_items'] = $this->db->count_all_results('students');
//					 }  else {
//                         $data['users'] = $this->students_model->limit($limit, $offset)->get(array('entry_user'=>$this->custom->user_id()));
//					 	$data['total_items'] = $this->db->where('entry_user',$this->custom->user_id())->count_all_results('students');
//					 }
					$config['base_url'] = base_url() . $this->uri->segment(1) . '/index/';
					$config['total_rows'] = $data['total_items'];
					$config['per_page'] = $limit;
					$config['uri_segment'] = 3;
					// First Links
					$config['first_link'] = 'First';
					$config['first_tag_open'] = '<li>';
					$config['first_tag_close'] = '</li>';
					// Last Links
					$config['last_link'] = 'Last';
					$config['last_tag_open'] = '<li>';
					$config['last_tag_close'] = '</li>';
					// Next Link
					$config['next_link'] = '&raquo;';
					$config['next_tag_open'] = '<li>';
					$config['next_tag_close'] = '</li>';
					// Previous Link
					$config['prev_link'] = '&laquo;';
					$config['prev_tag_open'] = '<li>';
					$config['prev_tag_close'] = '</li>';
					// Current Link
					$config['cur_tag_open'] = '<li class="active"><a>';
					$config['cur_tag_close'] = '</a></li>';
					// Digit Link
					$config['num_tag_open'] = '<li>';
					$config['num_tag_close'] = '</li>';
					$this->pagination->initialize($config);
//					$this->db->limit($limit, $offset);
					$data['pagination'] = $this->pagination->create_links();
//					 if ( $this->custom->getUserProfile() == 2 ) {
//						$data['users'] = $this->db->limit($limit, $offset)
//												  ->order_by('id','desc')
//												  ->get('students');
//					 		 }  else {
//					 	$data['users'] = $this->db->where('entry_user',$this->custom->user_id())
//					 							  ->limit($limit, $offset)
//					 							  ->order_by('id','desc')
//					 							  ->get('students');
//					 	 }
					$this->load->view('admin/students/index', $data);
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}


	public function search()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1 or
				$this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1){
					if ( $this->custom->getUserProfile() == 2 ) {
						// $data['users'] = $this->students_model->get(array('sort_by' => 'id'));
                        if($this->input->get('ty') == 'con'){

                            $data['users'] = $this->db->like('fname',$this->input->get('q'))
                                ->or_like('phone',$this->input->get('q'))
                                ->order_by('id','desc')
                                ->where('admission','1')
                                ->get('students');
                        }
                       else {
                           $data['users'] = $this->db->like('fname', $this->input->get('q'))
                               ->or_like('phone',$this->input->get('q'))
                               ->order_by('id', 'desc')
                               ->get('students');
                       }
					}  else {
						// $this->db->where('entry_user',$this->custom->user_id());
                        if($this->input->get('ty') == 'con'){
								$data['users'] = $this->db->where('entry_user',$this->custom->user_id())
												  ->like('fname',$this->input->get('q'))
                                                     ->or_like('phone',$this->input->get('q'))
                                                    ->where('admission','1')
												   ->order_by('id','desc')
												  ->get('students');
                        }else{

                            $data['users'] = $this->db->where('entry_user',$this->custom->user_id())
                                ->like('fname',$this->input->get('q'))
                                ->or_like('phone',$this->input->get('q'))
                                ->order_by('id','desc')
                                ->get('students');
                        }
					}
                if($this->input->get('ty') == 'con') {
                    $this->load->view('admin/confirmed/index', $data);
                }else {
                    $this->load->view('admin/students/index', $data);
                }
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}


	public function add()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1 ){
				$data['subject'] = $this->subjects_model->get();
				$this->load->view('admin/students/add', $data);
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}

	public function insert()
	{

		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1 ){
				$filter = $this->custom->filter_all();

				if ($this->custom->checkloginemail($filter['post']['email']) == true) {
					$this->session->set_flashdata('message', array('title' => 'error', 'content' => 'Email already taken. please choose another email.', 'type' => 'message'));
					redirect($this->uri->segment(1) . '/add');
				} else {
					$this->students_model->add($filter['post']);
					$ids = $this->db->insert_id();
					$login = array(
						'user_id' => $ids,
						'email' => $filter['post']['email'],
						'password' => $this->custom->hash_password($filter['post']['password']),
						'user_type' => 3,
						'status' => $filter['post']['active'],
					);
					$this->db->insert('login', $login);
					if ($_FILES['fimg']['size'] != 0) {
						$path = 'upload';
						if (is_dir($path)) {
						} else {
							mkdir($path, 0755);
							mkdir($path . '/student', 0755);
							mkdir($path . '/student/thumb', 0755);
						}
						$pathdir = './upload/student/';
						$file_type = $_FILES['fimg']['type'];
						$allowed = array("image/png", "image/gif", "image/jpg", "image/jpeg");
						if (!in_array($file_type, $allowed)) {
							$this->session->set_flashdata('message', array('title' => 'error', 'content' => 'Only gif | jpg | png | jpeg Files Allow.', 'type' => 'message'));
							redirect($this->uri->segment(1) . '/add');
						} else {
							$image_data = $this->custom->upload_image('fimg', $pathdir);
							$upload = $image_data['file_name'];
							$this->students_model->update(array('id' => $ids, 'fimg' => $upload));
						}
					}

					if ($filter['post']['admission'] == 0) {
						redirect($this->uri->segment(1) . '/profile/' . $ids);
					} else {
						$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'New Student Successfully added.', 'type' => 'message'));
						redirect($this->uri->segment(1));
					}
					// $this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'New User Successfully added.', 'type' => 'message' )); 
					// redirect($this->uri->segment(1));
				}
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}

	public function edit()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 ){
				$data['subject'] = $this->subjects_model->get();
				$data['user'] = $this->students_model->get(array('id' => $this->uri->segment(3)));
				$this->load->view('admin/students/edit', $data);
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}

	public function update()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 ){
				$filter = $this->custom->filter_all();
//				print_r($filter);
//				die;
				// if( $this->custom->checkloginemail($filter['post']['email']) == true ) {
				// 	$this->session->set_flashdata( 'message', array( 'title' => 'error', 'content' => 'Email already taken. please choose another email.', 'type' => 'message' )); 
				// 	redirect($this->uri->segment(1).'/edit/'.$filter['post']['id']);
				// } else {
				if ($filter['post']['password'] == "") {
					$data = array(
						'id' => $filter['post']['id'],
						'subject_id' => $filter['post']['subject_id'],
						'fname' => $filter['post']['fname'],
						'lname' => $filter['post']['lname'],
						'counsellor' => $filter['post']['counsellor'],
						'email' => $filter['post']['email'],
						'address' => $filter['post']['address'],
						'phone' => $filter['post']['phone'],
						'admission' => $filter['post']['admission'],
						'student_info' => $filter['post']['student_info'],
						'demo_date' => $filter['post']['demo_date'],
						'demo_time' => $filter['post']['demo_time'],
						'joining_date' => $filter['post']['joining_date'],
						'contract_expiry_date' => $filter['post']['contract_expiry_date'],
						'source' => $filter['post']['source'],
						'level' => $filter['post']['level'],
						'total_class' => $filter['post']['total_class'],
						'student_comments' => $filter['post']['student_comments'],
						'active' => $filter['post']['active']
					);
					$this->students_model->update($data);
					$login = array(
						'user_type' => 3,
						'status' => $filter['post']['active']
					);
					$this->db->where('user_id', $filter['post']['id'])->update('login', $login);
				} else {
					$data = array(
						'id' => $filter['post']['id'],
						'subject_id' => $filter['post']['subject_id'],
						'fname' => $filter['post']['fname'],
						'lname' => $filter['post']['lname'],
						'counsellor' => $filter['post']['counsellor'],
						'email' => $filter['post']['email'],
						'active' => $filter['post']['active'],
						'admission' => $filter['post']['admission'],
						'student_info' => $filter['post']['student_info'],
						'total_free' => $filter['post']['total_free'],
						'total_class' => $filter['post']['total_class'],
						'student_comments' => $filter['post']['student_comments'],
						'password' => $filter['post']['password']
					);
					$this->students_model->update($data);
					$login = array(
						'email' => $filter['post']['email'],
						'password' => $this->custom->hash_password($filter['post']['password']),
						'user_type' => 3,
						'status' => $filter['post']['active']
					);
					$this->db->where('user_id', $filter['post']['id'])->update('login', $login);
				}
				if ($_FILES['fimg']['size'] != 0) {
					$path = 'upload';
					if (is_dir($path)) {
					} else {
						mkdir($path, 0755);
						mkdir($path . '/student', 0755);
						mkdir($path . '/student/thumb', 0755);
					}
					$pathdir = './upload/student/';
					$file_type = $_FILES['fimg']['type'];
					$allowed = array("image/png", "image/gif", "image/jpg", "image/jpeg");
					if (!in_array($file_type, $allowed)) {
						$this->session->set_flashdata('message', array('title' => 'error', 'content' => 'Only gif | jpg | png | jpeg Files Allow.', 'type' => 'message'));
						redirect($this->uri->segment(1) . '/edit/' . $filter['post']['id']);
					} else {
						$image_data = $this->custom->upload_image('fimg', $pathdir);
						$upload = $image_data['file_name'];
						$this->students_model->update(array('id' => $filter['post']['id'], 'fimg' => $upload));
					}
				}

				if ($filter['post']['admission'] == 0) {
					redirect($this->uri->segment(1));
				} else {
					$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'Student Successfully Updated.', 'type' => 'message'));
					redirect($this->uri->segment(1));
				}
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
		// }

	}

	public function delete()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1){
				$this->db->where('user_id', $this->uri->segment(3))->where('user_type', 3)->delete('login');
				$this->students_model->delete($this->uri->segment(3));
				$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'User Successfully deleted.', 'type' => 'message'));
				redirect($this->uri->segment(1));
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}

	public function profile()
	{
		$data['subject'] = $this->subjects_model->get();
		$data['profile'] = $this->db->where('student_id', $this->uri->segment(3))->get('student_profile_basic');
		$data['enrollment'] = $this->db->where('student_id', $this->uri->segment(3))->get('student_profile_payment');
		$data['plan'] = $this->db->where('student_id', $this->uri->segment(3))->get('student_profile_plan');
		$this->load->view('admin/students/profile', $data);
	}

	public function profileadd()
	{
		if ($this->input->post('profile_id')) {
			$data = array(
				'student_id' => trim(strip_tags($this->input->post('student_id'))),
				'fullname' => trim(strip_tags($this->input->post('fullname'))),
				'email' => trim(strip_tags($this->input->post('email'))),
				'streetaddredd' => trim(strip_tags($this->input->post('streetaddredd'))),
				'gender' => trim(strip_tags($this->input->post('gender'))),
				'phonenumber' => trim(strip_tags($this->input->post('phonenumber'))),
				'birthday' => trim(strip_tags($this->input->post('birthday'))),
				'noteofattending' => trim(strip_tags($this->input->post('noteofattending'))),
				'expirydate' => trim(strip_tags($this->input->post('expirydate'))),
				'coursename' => trim(strip_tags($this->input->post('coursename'))),
				'startdate' => trim(strip_tags($this->input->post('startdate'))),
				'numberofcourses' => trim(strip_tags($this->input->post('numberofcourses'))),
				'instructionform' => trim(strip_tags($this->input->post('instructionform'))),
				'contracttype' => trim(strip_tags($this->input->post('contracttype'))),
				'created' => date('Y-m-d')
			);
			// $this->db->insert('student_profile_basic', $data);
			$this->db->where('student_id', $this->input->post('student_id'))->update('student_profile_basic', $data);
			$enroll = array(
				'student_id' => trim(strip_tags($this->input->post('student_id'))),
				'enrollmentfee_listproce' => trim(strip_tags($this->input->post('enrollmentfee_listproce'))),
				'enrollmentfee_discount' => trim(strip_tags($this->input->post('enrollmentfee_discount'))),
				'enrollmentfee_subtotal' => trim(strip_tags($this->input->post('enrollmentfee_subtotal'))),
				'enrollmentfee_saletax' => trim(strip_tags($this->input->post('enrollmentfee_saletax'))),
				'enrollmentfee_meter' => trim(strip_tags($this->input->post('enrollmentfee_meter'))),
				'tuitionfee_listproce' => trim(strip_tags($this->input->post('tuitionfee_listproce'))),
				'tuitionfee_discount' => trim(strip_tags($this->input->post('tuitionfee_discount'))),
				'tuitionfee_subtotal' => trim(strip_tags($this->input->post('tuitionfee_subtotal'))),
				'tuitionfee_saletax' => trim(strip_tags($this->input->post('tuitionfee_saletax'))),
				'tuitionfee_meter' => trim(strip_tags($this->input->post('tuitionfee_meter'))),
				'curriculumfee_listproce' => trim(strip_tags($this->input->post('curriculumfee_listproce'))),
				'curriculumfee_discount' => trim(strip_tags($this->input->post('curriculumfee_discount'))),
				'curriculumfee_subtotal' => trim(strip_tags($this->input->post('curriculumfee_subtotal'))),
				'curriculumfee_saletax' => trim(strip_tags($this->input->post('curriculumfee_saletax'))),
				'curriculumfee_meter' => trim(strip_tags($this->input->post('curriculumfee_meter'))),
				'curriculumfee_meter_total' => trim(strip_tags($this->input->post('curriculumfee_meter_total'))),
				'created' => date('Y-m-d')
			);
			$this->db->where('student_id', $this->input->post('student_id'))->update('student_profile_payment', $enroll);
			$paymentplan_date = $this->input->post('paymentplan_date');
			$paymentplan_method = $this->input->post('paymentplan_method');
			$paymentplan_payment = $this->input->post('paymentplan_payment');
			$paymentplan_datepicker = $this->input->post('paymentplan_datepicker');
			$this->db->where('student_id', $this->input->post('student_id'))->delete('student_profile_plan');
			for ($i = 0; $i < count($paymentplan_date); $i++) {
				$p = array(
					'student_id' => trim(strip_tags($this->input->post('student_id'))),
					'paymentplan_date' => $paymentplan_date[$i],
					'paymentplan_method' => $paymentplan_method[$i],
					'paymentplan_payment' => $paymentplan_payment[$i],
					'paymentplan_datepicker' => $paymentplan_datepicker,
					'created' => date('Y-m-d')
				);
				$this->db->insert('student_profile_plan', $p);
			}
		} else {
			$data = array(
				'student_id' => trim(strip_tags($this->input->post('student_id'))),
				'fullname' => trim(strip_tags($this->input->post('fullname'))),
				'email' => trim(strip_tags($this->input->post('email'))),
				'streetaddredd' => trim(strip_tags($this->input->post('streetaddredd'))),
				'gender' => trim(strip_tags($this->input->post('gender'))),
				'phonenumber' => trim(strip_tags($this->input->post('phonenumber'))),
				'birthday' => trim(strip_tags($this->input->post('birthday'))),
				'noteofattending' => trim(strip_tags($this->input->post('noteofattending'))),
				'expirydate' => trim(strip_tags($this->input->post('expirydate'))),
				'coursename' => trim(strip_tags($this->input->post('coursename'))),
				'startdate' => trim(strip_tags($this->input->post('startdate'))),
				'numberofcourses' => trim(strip_tags($this->input->post('numberofcourses'))),
				'instructionform' => trim(strip_tags($this->input->post('instructionform'))),
				'contracttype' => trim(strip_tags($this->input->post('contracttype'))),
				'created' => date('Y-m-d')
			);
			$this->db->insert('student_profile_basic', $data);
			$enroll = array(
				'student_id' => trim(strip_tags($this->input->post('student_id'))),
				'enrollmentfee_listproce' => trim(strip_tags($this->input->post('enrollmentfee_listproce'))),
				'enrollmentfee_discount' => trim(strip_tags($this->input->post('enrollmentfee_discount'))),
				'enrollmentfee_subtotal' => trim(strip_tags($this->input->post('enrollmentfee_subtotal'))),
				'enrollmentfee_saletax' => trim(strip_tags($this->input->post('enrollmentfee_saletax'))),
				'enrollmentfee_meter' => trim(strip_tags($this->input->post('enrollmentfee_meter'))),
				'tuitionfee_listproce' => trim(strip_tags($this->input->post('tuitionfee_listproce'))),
				'tuitionfee_discount' => trim(strip_tags($this->input->post('tuitionfee_discount'))),
				'tuitionfee_subtotal' => trim(strip_tags($this->input->post('tuitionfee_subtotal'))),
				'tuitionfee_saletax' => trim(strip_tags($this->input->post('tuitionfee_saletax'))),
				'tuitionfee_meter' => trim(strip_tags($this->input->post('tuitionfee_meter'))),
				'curriculumfee_listproce' => trim(strip_tags($this->input->post('curriculumfee_listproce'))),
				'curriculumfee_discount' => trim(strip_tags($this->input->post('curriculumfee_discount'))),
				'curriculumfee_subtotal' => trim(strip_tags($this->input->post('curriculumfee_subtotal'))),
				'curriculumfee_saletax' => trim(strip_tags($this->input->post('curriculumfee_saletax'))),
				'curriculumfee_meter' => trim(strip_tags($this->input->post('curriculumfee_meter'))),
				'curriculumfee_meter_total' => trim(strip_tags($this->input->post('curriculumfee_meter_total'))),
				'created' => date('Y-m-d')
			);
			$this->db->insert('student_profile_payment', $enroll);
			$paymentplan_date = $this->input->post('paymentplan_date');
			$paymentplan_method = $this->input->post('paymentplan_method');
			$paymentplan_payment = $this->input->post('paymentplan_payment');
			$paymentplan_datepicker = $this->input->post('paymentplan_datepicker');

			for ($i = 0; $i < count($paymentplan_date); $i++) {
				$p = array(
					'student_id' => trim(strip_tags($this->input->post('student_id'))),
					'paymentplan_date' => $paymentplan_date[$i],
					'paymentplan_method' => $paymentplan_method[$i],
					'paymentplan_payment' => $paymentplan_payment[$i],
					'paymentplan_datepicker' => $paymentplan_datepicker,
					'created' => date('Y-m-d')
				);
				$this->db->insert('student_profile_plan', $p);
			}
		}
		$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'User Successfully Updated.', 'type' => 'message'));
		redirect($this->uri->segment(1) . '/profileprint/' . $this->input->post('student_id'));
	}

	public function profileprint()
	{
		$data['profile'] = $this->db->where('student_id', $this->uri->segment(3))->get('student_profile_basic');
		$data['enrollment'] = $this->db->where('student_id', $this->uri->segment(3))->get('student_profile_payment');
		$data['plan'] = $this->db->where('student_id', $this->uri->segment(3))->get('student_profile_plan');
		$this->load->view('admin/students/profileprint', $data);
	}

	public function addclass()
	{
		$filter = $this->custom->filter_all();
		$student_ids = $filter['post']['student_ids'];
		$subject_id = $filter['post']['subject_id'];
		$teacher_id = $filter['post']['teacher_id'];
		$schdule_date = $filter['post']['schdule_date'];
		$timepickerfrom = $filter['post']['timepickerfrom'];
		$post = array('subject_id' => $subject_id, 'teacher_id' => $teacher_id);
		$this->schdule_model->add($post);
		$ids = $this->db->insert_id();

		$this->db->insert('schdule_date', array(
			'schdule_date_id' => $ids,
			'subject_id' => $subject_id,
			'teacher_id' => $teacher_id,
			'dates' => $schdule_date,
			'created' => date('Y-m-d H;i:s')
		));
		$date_id = $this->db->insert_id();

		$data = array(
			'subject_id' => $subject_id,
			'teacher_id' => $teacher_id,
			'schdule_id' => $ids,
			'date_id' => $date_id,
			'schdule_date' => $schdule_date,
			'timepickerfrom' => $timepickerfrom,
			'expiry_time' => date('h:i', strtotime($timepickerfrom)),
			'created' => date('Y-m-d H:i:s')
		);
		$this->db->insert('schdule_time', $data);
		$slot_ids = $this->db->insert_id();
		// add student
		$datas = array(
			'student_id' => $filter['post']['student_ids'],
			'slotdate' => date('Y-m-d', strtotime($schdule_date)),
			'slotid' => $date_id,
			'subject_id' => $subject_id,
			'teacher_id' => $teacher_id,
			'slottype' => 'booking',
			'timeslot' => $slot_ids,
			'timefrom' => date('H:i:s', strtotime($timepickerfrom)),
			'slotstatus' => 1,
			'created' => date('Y-m-d H:i:s')
		);
		$this->db->insert('student_booking', $datas);
		print $this->db->insert_id();
	}

	public function getclassteacher()
	{
		$result = array();
		$sql = $this->teachers_model->get(array('subject_id' => $this->input->get('s')));
		if ($sql->num_rows() > 0) {
			foreach ($sql->result() as $row) {
				$result[] = '<option value="' . $row->id . '">' . $row->fname . ' ' . $row->lname . '</option>';
			}
		}
		print json_encode($result);
	}

	public function bookings()
	{
		$student_id = $this->uri->segment(3);

		$data['booking_details'] = $this->db
			->where('student_id', $student_id)
			->order_by('booking_id', 'desc')
			->get('student_booking');
		$this->load->view('admin/students/booking_details', $data);
	}

	public function viewprofile()
	{
		if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
			if(	$this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1 ){
				$data['student'] = $this->students_model->get(['id' => $this->uri->segment(3)]);
				$this->load->view('admin/students/viewprofile', $data);
			}else{
				redirect('home');
			}
		}else{
				redirect('home');
		}
	}

	public function confirmedStudents()
	{
        if($this->permission_model->getAllPerm('students',$this->custom->getUserProfile())== 1){
            if(	$this->permission_model->getAddPerm('students',$this->custom->getUserProfile())== 1 or
                $this->permission_model->getEditPerm('students',$this->custom->getUserProfile())== 1 or
                $this->permission_model->getViewPerm('students',$this->custom->getUserProfile())== 1 or
                $this->permission_model->getDelPerm('students',$this->custom->getUserProfile())== 1){
                $limit = 15;
                $profile= $this->custom->getUserProfile();
                $std = new Students_model();
                if ( $profile == 2 ) {
                    $data['total_items'] = $this->students_model->count_all_confirm_student();
                }else{
                    $data['total_items'] = $this->students_model->count_all_confirm_student_emp();
                }
                $config['base_url'] = base_url() . $this->uri->segment(1) . '/confirmedStudents/';
                $config['total_rows'] = $data['total_items'];
                $config['per_page'] = $limit;
                $config['uri_segment'] = 3;
                // First Links
                $config['first_link'] = 'First';
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                // Last Links
                $config['last_link'] = 'Last';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                // Next Link
                $config['next_link'] = '&raquo;';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                // Previous Link
                $config['prev_link'] = '&laquo;';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                // Current Link
                $config['cur_tag_open'] = '<li class="active"><a>';
                $config['cur_tag_close'] = '</a></li>';
                // Digit Link
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $this->pagination->initialize($config);
                $offset = $this->uri->segment(3);
                $this->db->limit($limit, $offset);
                $data['pagination'] = $this->pagination->create_links();
                 if ( $profile == 2 ) {
                $data['users'] = $this->db->limit($limit, $offset)
                    ->where('admission','0')
                    ->order_by('id','desc')
                    ->get('students');
                 } else{
                     $data['users'] = $this->db->limit($limit, $offset)
                         ->where('admission','0')
                         ->where('entry_user',$this->custom->user_id())
                         ->order_by('id','desc')
                         ->get('students');

                 }
                $this->load->view('admin/confirmed/index', $data);
            }else{
                redirect('home');
            }
        }else{
            redirect('home');
        }

	}

}
