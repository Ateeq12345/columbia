<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('school_model');
	}
	
	public function index()
	{
		$data['sql'] = $this->school_model->get(['school_status' => 1]);
		$this->load->view('admin/school/index',$data);
	}

	public function add()
	{
		$this->load->view('admin/school/add');
	}

	public function insert()
	{
		$filter = $this->custom->filter_all();
		$this->school_model->add($filter['post']);
		redirect( $this->uri->segment(1) );
	}

	public function edit()
	{
		$data['sql'] = $this->school_model->get(['id' => $this->uri->segment(3)]);
		$this->load->view('admin/school/edit',$data);
	}

	public function update()
	{
		$filter = $this->custom->filter_all();
		$this->school_model->update($filter['post']);
		redirect( $this->uri->segment(1) );
	}


	public function delete()
	{
		$this->school_model->delete($this->uri->segment(3));
		redirect( $this->uri->segment(1) );
	}


}
