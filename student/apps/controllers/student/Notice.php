<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notice extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->custom->student_user_id() > 0) {
		} else {
			redirect('login');
		}
		$this->load->model('schdule_model');
		$this->load->model('subjects_model');
		$this->load->model('students_model');
		$this->load->model('teachers_model');
		$this->load->model('school_model');
	}

	public function index()
	{
		$data['student_id'] = $this->custom->student_user_id();
		$data['sql'] = $this->db
			->where('student_id', $data['student_id'])
			->where('is_canceled', 0)
			->order_by('booking_id', 'desc')
			->get('student_booking');
		$this->load->view('student/notice/index', $data);
	}

	public function feedback()
	{
		$data['student_id'] = $this->custom->student_user_id();
		$data['class_id'] = $this->uri->segment(4);
		$data['class_document'] = $this->db->where('timeslot', $data['class_id'])
			->where('student_id', $data['student_id'])
			->where('slotstatus', 2)
			->get('student_booking');
		$this->load->view('student/notice/feedback', $data);
	}

	public function addfeedback()
	{
		$filter = $this->custom->filter_all();

		$student_name = $this->students_model->getFnameById($filter['post']['student_id']);
		$subject_name = $this->subjects_model->getNameById($filter['post']['subject_id']);
		$teacher_name = $this->teachers_model->getNameById($filter['post']['teacher_id']);
		$school_name = $this->school_model->getSchoolById($filter['post']['school_id']);
		$this->db->where('timeslot', $filter['post']['timeslot'])
						->where('student_id', $filter['post']['student_id'])
						->update('student_booking', array(
							'feeback' => $filter['post']['feeback']
						));
		$data['message'] = '<table>
						<tr>
							<td>Student Name: </td>
							<td>'.$student_name.'</td>
						</tr>
						<tr>
							<td>Subject Name: </td>
							<td>'.$subject_name.'</td>
						</tr>
						<tr>
							<td>Teacher Name: </td>
							<td>'.$teacher_name.'</td>
						</tr>
						<tr>
							<td>School Name: </td>
							<td>'.$school_name.'</td>
						</tr>
						<tr>
							<td>Slot date: </td>
							<td>'.$filter['post']['slotdate'].'</td>
						</tr>
						<tr>
							<td>Slot Time: </td>
							<td>'.date('H:i:s A', strtotime( $filter['post']['timeto'] ) ).'</td>
						</tr>
						<tr>
							<td>Teaching tyle: </td>
							<td>'.$filter['post']['teaching_style'].'</td>
						</tr>
						<tr>
							<td>Time Management: </td>
							<td>'.$filter['post']['time_style'].'</td>
						</tr>
						<tr>
							<td>Lesson Management: </td>
							<td>'.$filter['post']['lesson_style'].'</td>
						</tr>
						<tr>
							<td>Feedback: </td>
							<td>'.$filter['post']['feeback'].'</td>
						</tr>
					</table>';

		$st_email = $this->students_model->getEmailById($filter['post']['student_id']);
		$this->custom->SendMail($st_email,'info@columbia-academy.jp','Student Feedback',$data,'feedback');
		$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'Feedback Successfully Added.', 'type' => 'message'));
		redirect($this->uri->segment(1));
	}

	public function edit()
	{
		$data['student_id'] = $this->custom->student_user_id();
		$data['class_id'] = $this->uri->segment(4);
		$data['class_document'] = $this->db->where('timeslot', $data['class_id'])
			->where('student_id', $data['student_id'])
			->where('is_canceled', 0)
			->where('slotstatus', 2)
			->get('student_booking');
		$this->load->view('student/notice/edit', $data);
	}



	public function cancel()
	{
		$data['student_id'] = $this->custom->student_user_id();
		$data['class_id'] = $this->uri->segment(4);
		$bookingDetail = $this->db->where('timeslot', $data['class_id'])
								  ->where('student_id', $data['student_id'])
								  ->get('student_booking')->row();

		$currentDateTime = explode(" ", date('Y-m-d H:i:s'));
		$today_date = $currentDateTime[0];
		$today_time = $currentDateTime[1];
		$date1 = date_create(date('Y-m-d'));
		$date2 =  date_create(date('Y-m-d', strtotime($bookingDetail->slotdate)));
		$diff = date_diff($date2, $date1)->d;
		if ($today_date === date('Y-m-d', strtotime($bookingDetail->slotdate))) {
			$this->session->set_flashdata('message', array('title' => 'error', 'content' => 'You cannot cancel the class.', 'type' => 'message'));
			redirect($this->uri->segment(1));
		}
		if ($diff > 0) {
			if ($diff == 1) {
				if ($today_time < date('H:i:s', strtotime('18:00:00'))) {
					$this->db->where('timeslot', $data['class_id'])
						->where('student_id', $data['student_id'])
						->update('student_booking', array(
							'is_canceled' => 1,
							'canceled_at' => date('Y-m-d H:i:s'),
							'canceled_ip' => $this->input->ip_address()
						));
					$this->custom->addActivity($data['class_id'],'student',1,$this->custom->student_user_id(),get_current_user());
				$student_name = $this->students_model->getFnameById($bookingDetail->student_id);
				$subject_name = $this->subjects_model->getNameById($bookingDetail->subject_id);
				$teacher_name = $this->teachers_model->getNameById($bookingDetail->teacher_id);
				$school_name = $this->school_model->getSchoolById($bookingDetail->school_id);


				$data['message'] = '<table>
								<tr>
									<td>Student Name: </td>
									<td>'.$student_name.'</td>
								</tr>
								<tr>
									<td>Subject Name: </td>
									<td>'.$subject_name.'</td>
								</tr>
								<tr>
									<td>Teacher Name: </td>
									<td>'.$teacher_name.'</td>
								</tr>
								<tr>
									<td>School Name: </td>
									<td>'.$school_name.'</td>
								</tr>
								<tr>
									<td>Cancel Date</td>
									<td>'.date('Y-m-d H:i:s').'</td>
								</tr>
								<tr>
									<td>Slot date: </td>
									<td>'.$bookingDetail->slotdate.'</td>
								</tr>
								<tr>
									<td>Slot Time: </td>
									<td>'.date('H:i:s A', strtotime( $bookingDetail->timefrom ) ).'</td>
								</tr>
								<tr>
									<td></td>
									<td>Student cancel this slot</td>
								</tr>
							</table>';

					$st_email = $this->students_model->getEmailById($data['student_id']);
					$this->custom->SendMail($st_email,'info@columbia-academy.jp','Student Cancel Booking',$data,'cancel');
					$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'Class has been canceled successfuly.', 'type' => 'message'));
					redirect($this->uri->segment(1));
				} else {
					$this->session->set_flashdata('message', array('title' => 'error', 'content' => 'You cannot cancel the class.', 'type' => 'message'));
					redirect($this->uri->segment(1));
				}
			} else {
				$this->db->where('timeslot', $data['class_id'])
					->where('student_id', $data['student_id'])
					->update('student_booking', array(
						'is_canceled' => 1,
						'canceled_at' => date('Y-m-d H:i:s'),
						'canceled_ip' => $this->input->ip_address()
					));
					$this->custom->addActivity($data['class_id'],'student',1,$this->custom->student_user_id(),get_current_user());
				$student_name = $this->students_model->getFnameById($bookingDetail->student_id);
				$subject_name = $this->subjects_model->getNameById($bookingDetail->subject_id);
				$teacher_name = $this->teachers_model->getNameById($bookingDetail->teacher_id);
				$school_name = $this->school_model->getSchoolById($bookingDetail->school_id);

				$data['message'] = '<table>
								<tr>
									<td>Student Name: </td>
									<td>'.$student_name.'</td>
								</tr>
								<tr>
									<td>Subject Name: </td>
									<td>'.$subject_name.'</td>
								</tr>
								<tr>
									<td>Teacher Name: </td>
									<td>'.$teacher_name.'</td>
								</tr>
								<tr>
									<td>School Name: </td>
									<td>'.$school_name.'</td>
								</tr>
								<tr>
									<td></td>
									<td>Student cancel this slot</td>
								</tr>
								<tr>
									<td>Slot date: </td>
									<td>'.$bookingDetail->slotdate.'</td>
								</tr>
								<tr>
									<td>Slot Time: </td>
									<td>'.date('H:i:s A', strtotime( $bookingDetail->timefrom ) ).'</td>
								</tr>
							</table>';
	
				$st_email = $this->students_model->getEmailById($data['student_id']);
				$this->custom->SendMail($st_email,'info@columbia-academy.jp','Student Cancel Booking',$data,'cancel');
				$this->session->set_flashdata('message', array('title' => 'success', 'content' => 'Class has been canceled successfuly.', 'type' => 'message'));
				redirect($this->uri->segment(1));
			}
		} else {
			$this->session->set_flashdata('message', array('title' => 'error', 'content' => 'You cannot cancel the class.', 'type' => 'message'));
			redirect($this->uri->segment(1));
		}
	}
} 
