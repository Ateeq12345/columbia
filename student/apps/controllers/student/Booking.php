<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if ( $this->custom->student_user_id() > 0 ) {
			
		} else {
			redirect('login');
		}
		$this->load->model('schdule_model');
		$this->load->model('students_model');
		$this->load->model('subjects_model');
		$this->load->model('teachers_model');
		$this->load->model('school_model');
	}
	
	public function index()
	{
		$data['calendar'] = $this->db->limit(7)
									 ->get('schdule_time');
		$this->load->view('student/booking/index',$data);
		// print date('h:i A');									 
	}

	public function timeslot()
	{
		$slotdate = $this->input->get('booking');
		$data['schdule_time'] = $this->db->where('date_id',$slotdate)
						   				 ->get('schdule_time');
		$this->load->view('student/booking/timeslot',$data);
	}

	public function slotbook()
	{
		$student_id = $this->custom->student_user_id();
		$timeslot = $this->input->post('timeslot');
		$slottype = trim(strip_tags($this->input->post('slottype')));
		$slotid = trim(strip_tags($this->input->post('slotid')));
		$slotdate = trim(strip_tags($this->input->post('slotdate')));
		$school_id = trim(strip_tags($this->input->post('school_id')));
		for($i=0;$i < count($timeslot); $i++ ){
			$data = array(
							'student_id' => $student_id,
							'slotdate' => $slotdate,
							'school_id' => $school_id,
							'slotid' => $slotid,
							'subject_id' => $this->custom->getSubjectById($slotid),
							'teacher_id' => $this->custom->getTeacherById($slotid),
							'slottype' => $slottype,
							'timeslot' => $timeslot[$i],
							'timefrom' => $this->getFromtime($timeslot[$i],$slotid),
							'timeto' => $this->getTotime($timeslot[$i],$slotid),
							'slotstatus' => 1,
							'created' => date('Y-m-d H:i:s')
						);
			$this->db->insert('student_booking',$data);
		}

		$student_name = $this->students_model->getFnameById($student_id);
		$subject_name = $this->subjects_model->getNameById($this->custom->getSubjectById($slotid));
		$teacher_name = $this->teachers_model->getNameById($this->custom->getTeacherById($slotid));
		$school_name = $this->school_model->getSchoolById($school_id);
		$postdata['message'] = '<table>
								<tr>
									<td>Student Name: </td>
									<td>'.$student_name.'</td>
								</tr>
								<tr>
									<td>Subject Name: </td>
									<td>'.$subject_name.'</td>
								</tr>
								<tr>
									<td>Teacher Name: </td>
									<td>'.$teacher_name.'</td>
								</tr>
								<tr>
									<td>School Name: </td>
									<td>'.$school_name.'</td>
								</tr>
								<tr>
									<td>Date</td>
									<td>'.date('Y-m-d H:i:s').'</td>
								</tr>
								<tr>
									<td>Slot date: </td>
									<td>'.$slotdate.'</td>
								</tr>
								<tr>
									<td></td>
									<td>Student book this slot</td>
								</tr>
							</table>';
		$st_email = $this->students_model->getEmailById($student_id);
		$this->custom->SendMail($st_email,'info@columbia-academy.jp','Student Booking',$postdata,'booking');
		$this->session->set_flashdata( 'message', array( 'title' => 'success', 'content' => 'Timeslot successfully booked.', 'type' => 'message' )); 
		redirect(base_url().'student');
	}

	private function getFromtime($id,$date_id)
	{
		$sql = $this->db->select('*')
						->from('schdule_time')
						->where('id',$id)
						->where('date_id',$date_id)
						->limit(1)
						->get();
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return date('H:i:s', strtotime($row->timepickerfrom));
		}
		return false;
	}

	private function getTotime($id,$date_id)
	{
		$sql = $this->db->select('*')
						->from('schdule_time')
						->where('id',$id)
						->where('date_id',$date_id)
						->limit(1)
						->get();
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return date('H:i:s', strtotime($row->timepickerto));
		}
		return false;
	}

	public function addrequest()
	{
		$data['subj'] = $this->subjects_model->get();
		$data['school'] = $this->school_model->get();
		$this->load->view('student/booking/addrequest',$data);
	}

	public function sendrequest()
	{
		$filter = $this->custom->filter_all();
		$student_id = $this->custom->student_user_id();
		$data = [
					'student_id' => $student_id,
					'slotdate'	 => $filter['post']['slotdate'],
					'subject'	 => $filter['post']['subject'],
					'school'	 => $filter['post']['school'],
					'slottime'	 => $filter['post']['slottime'],
					'created'	 => date('Y-m-d')
				];
		$this->db->insert('student_request',$data);
		$st_email = $this->students_model->getEmailById($student_id);
		$this->custom->SendMail($st_email,'info@columbia-academy.jp','Class Request','','request');
		redirect(base_url().'student/booking/message');
	}

	public function message()
	{
		$data['subj'] = $this->subjects_model->get();
		$data['school'] = $this->school_model->get();
		$this->load->view('student/booking/message',$data);
	}


}
