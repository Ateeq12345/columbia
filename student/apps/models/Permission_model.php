<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission_model extends CI_Model {

	protected $permission    = 'users_permission';// User database Table
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}	
	
	public function getAddPerm($pright,$id){
		// $sql = 'SELECT * FROM
		// 			cms_users_profile,
		// 			cms_users_permission  
		// 		WHERE cms_users_profile.id = cms_users_permission.pid
		// 		AND cms_users_profile.id='.$id.' 
		// 		AND cms_users_profile.id='.$id.' ';
		$query = $this->db->select('*')
						  ->from('users_profile')
						  ->join($this->permission,'users_profile.id = users_permission.pid','left')
		                  ->where('users_profile.id',$id)
						  ->where('users_permission.pright',$pright)
						  // ->group_by('users_permission.pid')
						  ->limit(1)
						  ->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->addp;
		}
		return false;
	}
	
	public function getEditPerm($pright,$id){
		$query = $this->db->select('*')
						  ->from('users_profile')
						  ->join($this->permission,'users_profile.id = users_permission.pid','left')
		                  ->where('users_profile.id',$id)
						  ->where('users_permission.pright',$pright)
						  // ->group_by('users_permission.pid')
						  ->limit(1)
						  ->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->editp;
		}
		return false;
	}
	public function getViewPerm($pright,$id){
		$query = $this->db->select('*')
						  ->from('users_profile')
						  ->join($this->permission,'users_profile.id = users_permission.pid','left')
		                  ->where('users_profile.id',$id)
						  ->where('users_permission.pright',$pright)
						  // ->group_by('users_permission.pid')
						  ->limit(1)
						  ->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->view;
		}
		return false;
	}
	public function getDelPerm($pright,$id){
		$query = $this->db->select('*')
						  ->from('users_profile')
						  ->join($this->permission,'users_profile.id = users_permission.pid','left')
		                  ->where('users_profile.id',$id)
						  ->where('users_permission.pright',$pright)
						  // ->group_by('users_permission.pid')
						  ->limit(1)
						  ->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->deletep;
		}
		return false;
	}
	public function getAllPerm($pright,$id){
		$query = $this->db->select('*')
						  ->from('users_profile')
						  ->join($this->permission,'users_profile.id = users_permission.pid','left')
		                  ->where('users_profile.id',$id)
						  ->where('users_permission.pright',$pright)
						  // ->group_by('users_permission.pid')
						  ->limit(1)
						  ->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->allview;
		}
		return false;
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */