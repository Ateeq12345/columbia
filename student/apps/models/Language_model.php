<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language_model extends CI_Model {

	public $id;
	public $eng;
	public $jap;
	public $created;
    
	public $no_primary_key = false;
	protected $validate_field_existence = FALSE;
	
	protected $table    = 'lang';// User database Table
	protected $fields = array(
								'id',
								'eng',
								'jap',
								'created'
							);
	protected $required_fields = array('eng');
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}	
	
	
	function get($options = array()){
		// set an array for field querys and values
		// This allows gets with operators
		// $options = array('status >' => 5)
		$option_fields = array();
		foreach($options as $key => $value){
			$parts = explode(' ', $key, 2);
			$field = isset($parts[0]) ? $parts[0] : '';
			$operator = isset($parts[1]) ? $parts[1] : '';
			$option_fields[$field]['query'] = $key;
			$option_fields[$field]['value'] = $value;
		}
		$defaults = array(
			'sort_direction' => 'asc'
		);
		$options = $this->_default($defaults, $options);		
		$this->_set_editable_fields($this->table);
		foreach ($this->fields as $field){
			if (isset($option_fields[$field])){
				$this->db->where($option_fields[$field]['query'], $option_fields[$field]['value']);
			}
		}
		if (isset($options['limit']) && isset($options['offset'])){
			$this->db->limit($options['limit'], $options['offset']);
		}else{
			if (isset($options['limit'])){
			    $this->db->limit($options['limit']);
			}
		}
		if (isset($options['sort_by'])){
			$this->db->order_by($options['sort_by'], $options['sort_direction']);
		}
		$query = $this->db->get($this->table);		
		if (isset($options[$this->id])){
			return $query->row();
		}else{
			return $query;
		}
	}
	
	
	function add($options = array()){
		if ( ! $this->_required($this->required_fields, $options)){
			return FALSE;
		}
		$this->_set_editable_fields($this->table);
		$this->_validate_options_exist($options);
		$default = array(
			'created'  => date('Y-m-d H:i:s')
		);
		$options = $this->_default($default, $options);		
		foreach ($this->fields as $field) {
			if (isset($options[$field])){
				$this->db->set($field, $options[$field]);
			}
		}
        
		$query = $this->db->insert($this->table);
		if ($query){
			if ($this->no_primary_key == FALSE){
				return $this->db->insert_id();
			}else{
				return TRUE;
			}
		}
	}
	  
	function update($options = array()){
		$required = array('id');
		if ( ! $this->_required($required, $options)){
			return FALSE;
		}
		$this->_set_editable_fields($this->table);
		$this->_validate_options_exist($options);
		$default = array(
			'created' => date($this->config->item('log_date_format'))
		);
		$options = $this->_default($default, $options);
		foreach ($this->fields as $field) 
		{
			if (isset($options[$field]))
			{
				$this->db->set($field, $options[$field]);
			}
		}		
		$this->db->where('id', $options['id']);
		$this->db->update($this->table);
		return $this->db->affected_rows();
	}
	
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete($this->table);
	}
	
	function _validate_options_exist($options){
		if ($this->validate_field_existence == TRUE){
			foreach ($options as $key => $value){
				$parts = explode(' ', $key);
				$field = $parts[1];
				if ( ! $this->db->field_exists($field, $this->table)){
					show_error('You are trying to insert data into a field that does not exist.  The field "'. $field .'" does not exist in the "'. $this->table .'" table.');
				}
			}
		}
	}
	
	function _set_editable_fields(){
		if (empty($this->fields)){
			$this->db->cache_on();
			$this->fields = $this->db->list_fields($this->table);
			$this->db->cache_off();
		}
	}
	
	function _required($required, $data){
		foreach ($required as $field){
			if ( ! isset($data[$field])){
				return FALSE;
			}
		}
		return TRUE;
	}
	
	function _default($defaults, $options){
		return array_merge($defaults, $options);
	}
	
	
}
