<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students_model extends CI_Model {

	public $id;
	public $subject_id;
	public $fname;
	public $lname;
	public $fathername;
	public $email;
	public $password;
	public $address;
	public $phone;
	public $homephone;
	public $counsellor;
	public $fimg;
	public $active;
	public $admission;
	public $total_class;
	public $total_free;
	public $demo_date;
	public $demo_time;
	public $joining_date;
	public $contract_expiry_date;
	public $source;
	public $level;
	public $student_info;
	public $student_comments;
	public $entry_user;
	public $created;
	public $no_primary_key = false;
	protected $validate_field_existence = FALSE;
	
	protected $table    = 'students';// User database Table
	protected $fields = array(
								'id',
								'subject_id',
								'profile_id',
								'fname',
								'lname',
								'fathername',
								'email',
								'password',
								'address',
								'phone',
								'homephone',
								'counsellor',
								'fimg',
								'active',
								'admission',
								'total_class',
								'total_free',
                                'demo_date',
                                'demo_time',
                                'joining_date',
                                'contract_expiry_date',
                                'source',
                                'level',
								'student_info',
								'student_comments',
								'entry_user',
								'created'
							);
	protected $required_fields = array('fname');
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}	
	
	
	function get($options = array()){
		$option_fields = array();
		foreach($options as $key => $value){
			$parts = explode(' ', $key, 2);
			$field = isset($parts[0]) ? $parts[0] : '';
			$operator = isset($parts[1]) ? $parts[1] : '';
			$option_fields[$field]['query'] = $key;
			$option_fields[$field]['value'] = $value;
		}
		$defaults = array(
			'sort_direction' => 'desc'
		);
		$options = $this->_default($defaults, $options);		
		$this->_set_editable_fields($this->table);
		foreach ($this->fields as $field){
			if (isset($option_fields[$field])){
				$this->db->where($option_fields[$field]['query'], $option_fields[$field]['value']);
			}
		}
		if (isset($options['limit']) && isset($options['offset'])){
			$this->db->limit($options['limit'], $options['offset']);
		}else{
			if (isset($options['limit'])){
			    $this->db->limit($options['limit']);
			}
		}
		if (isset($options['sort_by'])){
			$this->db->order_by($options['sort_by'], $options['sort_direction']);
		}
		
		$query = $this->db->get($this->table);		
		if (isset($options[$this->id])){
			return $query->row();
		}else{
			return $query;
		}
	}
	
	function getlike($options = array()){
		$option_fields = array();
		foreach($options as $key => $value){
			$parts = explode(' ', $key, 2);
			$field = isset($parts[0]) ? $parts[0] : '';
			$operator = isset($parts[1]) ? $parts[1] : '';
			$option_fields[$field]['query'] = $key;
			$option_fields[$field]['value'] = $value;
		}
		$defaults = array(
			'sort_direction' => 'asc'
		);
		$options = $this->_default($defaults, $options);		
		$this->_set_editable_fields($this->table);
		foreach ($this->fields as $field){
			if (isset($option_fields[$field])){
				$this->db->like($option_fields[$field]['query'], $option_fields[$field]['value']);
			}
		}
		if (isset($options['limit']) && isset($options['offset'])){
			$this->db->limit($options['limit'], $options['offset']);
		}else{
			if (isset($options['limit'])){
			    $this->db->limit($options['limit']);
			}
		}
		if (isset($options['sort_by'])){
			$this->db->order_by($options['sort_by'], $options['sort_direction']);
		}
		if ( $this->custom->getUserProfile() == 2 ) {

		}  else {
			$this->db->where('entry_user',$this->custom->user_id());
		}	
		$query = $this->db->get($this->table);		
		if (isset($options[$this->id])){
			return $query->row();
		}else{
			return $query;
		}
	}
	
	function add($options = array()){
		if ( ! $this->_required($this->required_fields, $options)){
			return FALSE;
		}
		$this->_set_editable_fields($this->table);
		$this->_validate_options_exist($options);
		$default = array(
			'entry_user' => $this->custom->user_id(),
			'created'  => date('Y-m-d H:i:s')
		);
		$options = $this->_default($default, $options);		
		foreach ($this->fields as $field) {
			if (isset($options[$field])){
				$this->db->set($field, $options[$field]);
			}
		}
    	if(isset($options['password'])) $this->db->set('password',$this->hash_password($options['password']));
		$query = $this->db->insert($this->table);
		if ($query){
			if ($this->no_primary_key == FALSE){
				return $this->db->insert_id();
			}else{
				return TRUE;
			}
		}
	}
	  
	function update($options = array()){
		$required = array('id');
		if ( ! $this->_required($required, $options)){
			return FALSE;
		}
		$this->_set_editable_fields($this->table);
		$this->_validate_options_exist($options);
		$default = array(
			'created' => date($this->config->item('log_date_format'))
		);
		$options = $this->_default($default, $options);
		foreach ($this->fields as $field) 
		{
			if (isset($options[$field]))
			{
				$this->db->set($field, $options[$field]);
			}
		}		
		if(isset($options['password'])) $this->db->set('password',$this->hash_password($options['password']));
		$this->db->where('id', $options['id']);
		$this->db->update($this->table);
		return $this->db->affected_rows();
	}
	
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete($this->table);
	}
	
	function _validate_options_exist($options){
		if ($this->validate_field_existence == TRUE){
			foreach ($options as $key => $value){
				$parts = explode(' ', $key);
				$field = $parts[1];
				if ( ! $this->db->field_exists($field, $this->table)){
					show_error('You are trying to insert data into a field that does not exist.  The field "'. $field .'" does not exist in the "'. $this->table .'" table.');
				}
			}
		}
	}
	
	function _set_editable_fields(){
		if (empty($this->fields)){
			$this->db->cache_on();
			$this->fields = $this->db->list_fields($this->table);
			$this->db->cache_off();
		}
	}
	
	function _required($required, $data){
		foreach ($required as $field){
			if ( ! isset($data[$field])){
				return FALSE;
			}
		}
		return TRUE;
	}
	
	function _default($defaults, $options){
		return array_merge($defaults, $options);
	}
	
	function hash_password($password){
		$salt = sha1('@#$DSFT%T%YY%^U&U&@#$C');
		$pass = hash('sha512',$salt.$password);
		return $pass;	
	}
	
	function login_user($username,$password){
		$hash_p = $this->hash_password($password);
		$query  = $this->get(array('email'=>$username,'password'=>$hash_p,'active'=>'1'));
		if($query->num_rows() > 0){
			$row = $query->row();
			$this->session->set_userdata('logged_in',array('user_id' => $row->id,'ip'=>$this->input->ip_address()));
			return true;
		}
		return false;
	}
	
	function getEmailById($id)
	{
		$sql = $this->get(array('id'=>$id));
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->email;
		}
		return false;
	}

	function getNameById($id)
	{
		$sql = $this->get(array('id'=>$id));
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return ucfirst($row->fname. ' ' .$row->lname);
		}
		return false;
	}
	
	function getFnameById($id)
	{
		$sql = $this->get(array('id'=>$id));
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return ucfirst($row->fname);
		}
		return false;
	}

	function getlnameById($id)
	{
		$sql = $this->get(array('id'=>$id));
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return ucfirst($row->lname);
		}
		return false;
	}
	
	function getTotalAttendClass($student_id)
	{
		return $this->db->where('student_id',$student_id)->count_all_results('student_booking');
	}
	function count_all_confirm_student()
	{
		return $this->db->where('admission','0')->count_all_results('students');
	}
	function count_all_confirm_student_emp()
	{
		return $this->db->where('admission','0')->where('entry_user',$this->custom->user_id())->count_all_results('students');
	}

	function getTotalFeedClass($student_id)
	{
		return $this->db->where('student_id',$student_id)
						->where('feeback !=', "")
						->count_all_results('student_booking');
	}

}
