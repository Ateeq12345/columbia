<?php defined('BASEPATH') or exit('No direct script access allowed');

class Custom extends CI_Model
{

	private function filter_post($post)
	{
		$data = trim($post);
		$data = strip_tags($post);
		$data = htmlentities($post);
		return $data;
	}

	public function filter_all()
	{
		$get  = array();
		$post = array();
		// filter get vay
		foreach ($_GET as $key => $value) {
			$get[$key] = $this->filter_post($value);
		}
		// filter post var
		foreach ($_POST as $key => $value) {
			$post[$key] = $this->filter_post($value);
		}
		return array('get' => $get, 'post' => $post);
	}

	public function logo_admin()
	{
	}

	public function channelUrl()
	{
	}

	public function user_id()
	{
		$session_id = $this->session->userdata('logged_in');
		return $session_id['user_id'];
	}

	public function user_id_ip()
	{
		$session_id = $this->session->userdata('logged_in');
		return $session_id['ip'];
	}

	public function log_user_id()
	{
		$session_id = $this->session->userdata('logged_in');
		return $session_id['login_user_id'];
	}

	public function getUserProfile(){
		$id = $this->log_user_id();
		$sql = $this->db->where('id',$id)->limit(1)->get('users');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->profile_id;
		}
		return false;		
	}
	
	
	public function user_red()
	{
		$session_id = $this->session->userdata('red_url');
		return $session_id['red_url_user'];
	}

	public function student_user_id()
	{
		$session_id = $this->session->userdata('student_logged_in');
		return $session_id['login_user_id'];
	}

	public function teacher_user_id()
	{
		$session_id = $this->session->userdata('teacher_logged_in');
		return $session_id['login_user_id'];
	}

	public function teacher_login_user_id()
	{
		$session_id = $this->session->userdata('teacher_logged_in');
		return $session_id['login_user_id'];
	}

	public function getAdminUser()
	{
		$user_id = $this->user_id();
		$sql = $this->db->where('id', $user_id)
			->get('login');
		// print_r();
		// die;
		if ($sql->num_rows() > 0) {
			$row = $this->db->where('id',$sql->row()->user_id)->get('users')->row();
			return $row->fname . ' ' . $row->lname;
		}
		return false;
	}

	public function getAdminUserImage()
	{
		$user_id = $this->user_id();
		$sql = $this->db->where('id', $user_id)
			->get('login');
		// print_r();
		// die;
		if ($sql->num_rows() > 0) {
			$row = $this->db->where('id',$sql->row()->user_id)->get('users')->row();
			return $row->fimg;
		}
		return false;
	}


	public function getStudentUser()
	{
		$user_id = $this->student_user_id();
		$sql = $this->db->where('id', $user_id)
			->get('students');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname;
		}
		return false;
	}

	public function getStudentUserImage()
	{
		$user_id = $this->student_user_id();
		$sql = $this->db->where('id', $user_id)
			->get('students');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fimg;
		}
		return false;
	}

	public function getTeacherUser()
	{
		$user_id = $this->teacher_login_user_id();
		$sql = $this->db->where('id', $user_id)
			->get('teachers');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname;
		}
		return false;
	}

	public function getTeacherUserImage()
	{
		$user_id = $this->teacher_login_user_id();
		$sql = $this->db->where('id', $user_id)
			->get('teachers');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fimg;
		}
		return false;
	}

	public function getAdminUserById($user_id)
	{
		$sql = $this->db->where('id', $user_id)
			->get('users');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname;
		}
		return false;
	}

	public function getStudentUserById($user_id)
	{
		$sql = $this->db->where('id', $user_id)
			->get('students');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname;
		}
		return false;
	}

	public function upload_image($field, $path)
	{
		$dir = chmod($path, 0777);
		$config['image_library'] = 'gd2';
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		$config['maintain_ratio'] = TRUE;
		$config['encrypt_name'] = TRUE;
		$config['max_size']    = '1024000';
		$this->load->library('upload');
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($field)) {
			$error = array('error' => $this->upload->display_errors());
		} else {
			$image_data =  $this->upload->data();
			$config['source_image'] = $image_data['full_path'];
			$config['create_thumb'] = false;
			$config['new_image'] = $path . 'thumb/thumb_' . $image_data['file_name'];
			$config['width'] = 200;
			$config['height'] = 200;
			$config['maintain_ratio'] = TRUE;
			$this->load->library('image_lib');
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			return  $image_data;
		}
	}

	public function hash_password($password)
	{
		$salt = sha1('@#$DSFT%T%YY%^U&U&@#$C');
		$pass = hash('sha512', $salt . $password);
		return $pass;
	}

	public function checkloginemail($email)
	{
		$sql = $this->db->where('email', $email)
			->get('login');
		if ($sql->num_rows() > 0) {
			return true;
		}
		return false;
	}

	public function login($email, $password)
	{
		// print_r($password);
		// die;
		// $pass = $this->hash_password($password);
		$pass = $password;
		$sql = $this->db->where('email', $email)
			->where('password', $pass)
			->where('status', 1)
			->get('login');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			if ($row->user_type == 1) {
				$this->session->set_userdata('logged_in', array('user_id' => $row->id, 'login_user_id' => $row->user_id, 'ip' => $this->input->ip_address(), 'user_type' => $row->user_type));
				$this->session->set_userdata('red_url', array('red_url_user' => 1));
			} elseif ($row->user_type == 2) {
				$this->session->set_userdata('teacher_logged_in', array('user_id' => $row->id, 'login_user_id' => $row->user_id, 'ip' => $this->input->ip_address(), 'user_type' => $row->user_type));
				$this->session->set_userdata('red_url', array('red_url_user' => 2));
			} else {
				$this->session->set_userdata('student_logged_in', array('user_id' => $row->id, 'login_user_id' => $row->user_id, 'ip' => $this->input->ip_address(), 'user_type' => $row->user_type));
				$this->session->set_userdata('red_url', array('red_url_user' => 3));
			}
			return true;
		}
		return false;
	}


	function getDate($user_id)
	{
		$sql = $this->db->select('*')
			->from('schdule_date')
			->join('schdule_time', 'schdule_time.date_id=schdule_date.id')
			->where('schdule_date.teacher_id', $user_id)
			->get();
		return $sql;
	}

	function getBookingDate()
	{
		$today = date('m/d/Y');
		$sql = $this->db->select('*')
			->from('schdule_date')
			->where('dates >=', $today)
			->get();
		return $sql;
	}

	function getBookingDateAndTime()
	{
		$today = date('m/d/Y');
		$time = date('h:i A');
		$sql = $this->db->select('*')
						->from('schdule_time')
			// ->join('schdule_time','schdule_time.date_id=schdule_date.id')
						->where('schdule_date >=', $today)
			// ->where('timepickerfrom >=',$time)
						->get();
		return $sql;
	}

	function getStudentBookingDate()
	{
		$user_id = $this->student_user_id();
		$sql = $this->db->select('*')
			->from('student_booking')
			->where('student_id', $user_id)
			->get();
		return $sql;
	}
	function getAllStudent(){
        $result = array();
        $sql = $this->db->select('*')
            ->from('students')->get();
        if($sql->num_rows() > 0){
            foreach($sql->result() as $row){
                $result[] = array('code'=>$row->id,'name'=>$row->fname .' '. $row->lname);
            }
            }
        return $result;
    }
	function getAllSchool(){
        $result = array();
        $sql = $this->db->select('*')
            ->from('school')->get();
        if($sql->num_rows() > 0){
            foreach($sql->result() as $row){
                $result[] = array('id'=>$row->id,'name'=>$row->school_name);
            }
            }
        return $result;
    }
	function getAllTeachers(){
        $result = array();
        $sql = $this->db->select('*')
            ->from('teachers')->get();
        if($sql->num_rows() > 0){
            foreach($sql->result() as $row){
                $result[] = array('code'=>$row->id,'name'=>$row->fname .' '. $row->lname);
            }
            }
        return $result;
    }

    function getAllSubjects(){
        $sql = $this->db->select('*')
            ->from('subjects')->get();
		return 	$sql->result();
    }
    function getAllConfirmedStudents(){
        $sql = $this->db->select('*')
            ->from('students')->where('admission','0')->get();
		return 	$sql->result();
    }


	function getStudentBookingDateForStudent()
	{
		$user_id = $this->student_user_id();
		$sql = $this->db->select('*')
						->from('student_booking')
						->where('student_id', $user_id)
						->where('is_canceled', 0)
						->get();
		return $sql;
	}

	function getSbColor($subject_id)
	{
		$sql = $this->db->where('id',$subject_id)
						->get('subjects');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->color_code;
		}
		return false;
	}

	function getSbBColor($subject_id)
	{
		$sql = $this->db->where('id',$subject_id)
						->get('subjects');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->colors;
		}
		return false;
	}

	function countSlot($slot_id)
	{
		return $this->db->select('*')
			->from('student_booking')
			->where('timeslot', $slot_id)
			->count_all_results();
	}

	function TotalcountSlot($slot_id)
	{
		return $this->db->select('*')
			->from('student_booking')
			->where('timeslot', $slot_id)
			->count_all_results();
	}

	function TotalStudentName($slot_id)
	{
		$sql = $this->db->select('*')
						->from('student_booking')
						->where('timeslot', $slot_id)
						->where('is_canceled', 0)
						->get();
		if ($sql->num_rows() > 0) {
			foreach ($sql->result() as $row) {
				return $this->getStudentName($row->student_id);
			}
		}
		return false;
	}

	function GetStudentSubjectName($slot_id)
	{
		$sql = $this->db->select('*')
			->from('student_booking')
			->where('timeslot', $slot_id)
			->get();
		if ($sql->num_rows() > 0) {
			foreach ($sql->result() as $row) {
				return $this->getSubjectName($row->subject_id);
			}
		}
		return false;
	}


	function TotalTeacherName($slot_id)
	{
		$sql = $this->db->select('*')
			->from('student_booking')
			->where('timeslot', $slot_id)
			->get();
		if ($sql->num_rows() > 0) {
			foreach ($sql->result() as $row) {
				return $this->getTeacherName($row->teacher_id);
			}
		}
		return false;
	}

	function getTeacherName($id)
	{
		$sql = $this->db->select('*')
			->from('teachers')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname;
		}
		return false;
	}
	function getTeacherImage($id)
	{
		$sql = $this->db->select('*')
			->from('teachers')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			     return base_url() . 'upload/teacher/' . $row->fimg;

		}
		return false;
	}
	function getTeacherNote($id,$bookingInd)
	{
		$sql = $this->db->select('*')
            ->from('note')
            ->where('teacher_id', $id)
            ->where('booking_id', $bookingInd)
			->get();
        if ($sql->num_rows() > 0) {
            $row = $sql->row();
            return $row->note;
        }
		return false;
	}
	function getTeacherNoteId($id,$bookingInd)
	{
		$sql = $this->db->select('*')
            ->from('note')
            ->where('teacher_id', $id)
            ->where('booking_id', $bookingInd)
			->get();
        if ($sql->num_rows() > 0) {
            $row = $sql->row();
            return $row->id;
        }
		return false;
	}
	function getStudentLesson($id){
        $sql = $this->db->select('*')
            ->from('students')
            ->where('id', $id)
            ->get();

        if ($sql->num_rows() > 0) {
            $row = $sql->row();
            return $row->total_class;
        }
        return  false;

    }
    function getStudentLessonTaken($id){
        $sql = $this->db->select('*')
            ->from('student_booking')
            ->where('student_id', $id)
            ->where('class_status', '1')
            ->get();

        return  $sql->num_rows();

    }
	function getStudentNote($id,$bookingInd)
	{
		$sql = $this->db->select('*')
			->from('note')
			->where('student_id', $id)
            ->where('booking_id', $bookingInd)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->note;
		}
		return false;
	}
	function getStudentNoteId($id,$bookingInd)
	{
		$sql = $this->db->select('*')
			->from('note')
			->where('student_id', $id)
            ->where('booking_id', $bookingInd)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->id;
		}
		return false;
	}

	function getStudentName($id)
	{
		$sql = $this->db->select('*')
			->from('students')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->fname . ' ' . $row->lname;
		}
		return false;
	}

	function getStudentPhone($id)
	{
		$sql = $this->db->select('*')
			->from('students')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->phone ;
		}
		return false;
	}
	function getStudentInfo($id)
	{
		$sql = $this->db->select('*')
			->from('students')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->student_info ;
		}
		return false;
	}
	function getStudentImage($id)
	{
		$sql = $this->db->select('*')
			->from('students')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return base_url().'upload/student/'.$row->fimg ;
		}
		return false;
	}
	function getStudentLevel($id)
	{
		$sql = $this->db->select('*')
			->from('students')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->level ;
		}
		return false;
	}

	function getStudentAdmission($id)
	{
		$sql = $this->db->select('*')
			->from('students')
			->where('id', $id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->admission;
		}
		return false;
	}

	function getAllSlot()
	{
		$sql = $this->db->select('*')
						->from('student_booking')
						->where('is_canceled', 0)
						->get();
		return $sql;
	}

	function getSubjectById($slot_id)
	{
		$sql = $this->db->select('*')
			->from('schdule_date')
			->where('id', $slot_id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->subject_id;
		}
		return false;
	}

	function getTeacherById($slot_id)
	{
		$sql = $this->db->select('*')
			->from('schdule_date')
			->where('id', $slot_id)
			->get();
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->teacher_id;
		}
		return false;
	}

	function getStudentCountByTeacher($timeslot)
	{
		$sql = $this->db->select('*')
			->from('student_booking')
			->where('timeslot', $timeslot)
			->count_all_results();
		return $sql;
	}

	function getStudentCountByClass($timeslot)
	{
		$sql = $this->db->select('*')
			->from('student_booking')
			// ->join('schdule_time','schdule_time.id=student_booking.timeslot')
			->where('student_booking.slotid', $timeslot)
			->get();
		return $sql;
	}


	function getLang($id)
	{
		$sql = $this->db->where('id', $id)
			->get('lang');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			if ($this->session->userdata('lang') == 'eng') {
				return $row->eng;
			} else {
				return $row->jap;
			}
		}
		return false;
	}

	function getSubjectName($id)
	{
		$sql = $this->db->where('id', $id)
			->get('subjects');
		if ($sql->num_rows() > 0) {
			$row = $sql->row();
			return $row->name;
		}
		return false;
	}

	function getSlotById($id)
	{
		$sql = $this->db->where('timeslot', $id)
		                ->where('is_canceled',0)
			            ->get('student_booking');
		if ($sql->num_rows() > 0) {
			return true;
		}
		return false;
	}
	
	function getClassById($id)
	{
		$sql = $this->db->where('student_id',$id)
						->count_all_results('student_booking');
		return $sql;
	}

	function getSchoolName($id)
	{
		$sql = $this->db->where('id',$id)->get('school');
		if($sql->num_rows() > 0){
			$row = $sql->row();
			return $row->school_name;
		}
		return false;
	}

	function getSchoolColor($id)
	{
		$sql = $this->db->where('id',$id)->get('school');
		if($sql->num_rows() > 0){
			$row = $sql->row();

			return $row->color;
		}
		return false;
	}
	function upComingLesson($id)
	{
		$sql = $this->db->where('student_id',$id)->get('student_booking');
		if($sql->num_rows() > 0){
          $total = 0;
            foreach($sql->result() as $row){
                if(date('Y-m-d',strtotime($row->slotdate)) > date('Y-m-d')){
                    $total++;
                }
            }

			return $total;
		}
		return false;
	}

	public function addActivity($class_id,$user_type,$types,$user_id,$user_system)
	{
		$data = array(
						'class_id' => $class_id,
						'user_type' => $user_type,
						'types' => $types,
						'user_id' => $user_id,
						'user_system' => $user_system,
						'pc_info' => serialize($_SERVER),
						'ip_id' => $this->input->ip_address(),
						'created' => date('Y-m-d H:i:s')
					);
		$this->db->insert('activity',$data);
	}

	
	public function SendMail($from,$to,$title,$data,$template){
		$config = Array(
					        'protocol' => 'ssmtp',
					        'smtp_host' => 'mail13.onamae.ne.jp',
					        'smtp_port' => '587',
					        'smtp_user' => 'info@columbia-academy.jp',
					        'smtp_pass' => 'Nanaakiba#0498',
					        'mailtype'  => 'html', 
					        'charset'   => 'iso-8859-1',
					        'validation' => TRUE,
  							'wordwrap' => TRUE
					    );
    	$this->load->library('email', $config);
		$this->email->clear(TRUE);
		$this->email->from($from);
		$this->email->to($to);
		$this->email->subject($title);
		$this->email->message($this->load->view('email/'.$template, $data,TRUE));
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->send();

		// $this->load->library('email');
		// $this->email->clear(TRUE);
		// $this->email->from($from);
		// $this->email->to($to);
		// $this->email->subject($title);
		// $this->email->message($this->load->view('email/'.$template, $data,TRUE));
		// $this->email->set_mailtype("html");
		// $this->email->set_newline("\r\n");
		// $this->email->send();
		return TRUE;
	}
	
	public function get_StudentRequests()
	{
		$sql = $this->db->get('student_request');

		return $sql->num_rows();
		
	} 

	public function get_Today_annoucement()
	{
		$date = new DateTime("now");

 		$curr_date = $date->format('Y-m-d');

		$sql = $this->db->select('*')->from('annoucements')->where('DATE(Date)',$curr_date)->get();
		// print_r($sql->num_rows());
		// die;
		return $sql->result();
	}

	public function get_Today_schdule_time()
	{
		$date = new DateTime("now");
 		$curr_date = $date->format('m/d/Y');

		$sql =  $this->db->select('*')->from('schdule_time')->where('schdule_date',$curr_date)->get();
		return $sql->result();
	}

	public function get_Today_demo_classes()
	{
		$date = new DateTime("now");
 		$curr_date = $date->format('m/d/Y');

		$sql =  $this->db->select('*')->from('students')->where('demo_date',$curr_date)->get();
		return $sql->result();
	}
	
	public function get_Today_Teacher()
	{

		$date = new DateTime("now");
 		$curr_date = $date->format('m/d/Y');

		$sql =  $this->db->select('DISTINCT(teacher_id)')->from('schdule_time')->where('schdule_date',$curr_date)->get();
		return $sql->result();


	} 

	public function get_Today_Ginza_class()
	{

		$date = new DateTime("now");
 		$curr_date = $date->format('m/d/Y');

		$sql =  $this->db->select('*')->from('schdule_time')->where('school_id','2')->where('schdule_date',$curr_date)->get();
		return $sql->result();


	} 

	public function get_Today_OmotoSandho_class()
	{

		$date = new DateTime("now");
 		$curr_date = $date->format('m/d/Y');

		$sql =  $this->db->select('*')->from('schdule_time')->where('school_id','3')->where('schdule_date',$curr_date)->get();
		return $sql->result();


	} 

}
