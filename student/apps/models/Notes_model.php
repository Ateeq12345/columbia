<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notes_model extends CI_Model {

	public $id;
	public $student_id;
	public $teacher_id;
	public $note;
	public $no_primary_key = false;
	protected $validate_field_existence = FALSE;
	
	protected $table    = 'note';// User database Table
	protected $fields = array(
								'id',
								'student_id',
								'teacher_id',
								'date',
								'booking_id',
								'note',
							);
	protected $required_fields = array('note');
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}	
	

    function insert($options = array()){
        if ( ! $this->_required($this->required_fields, $options)){
            return FALSE;
        }
        if($options['id'] > 0){

            $array= array(
                'id'=>$options['id'],
                'note' => $options['note'],
                'student_id' => $options['student_id'],
                'teacher_id' => $options['teacher_id'],
            );
            $this->db->where('id', $options['id']);
            $query = $this->db->update('note', $array);
            $this->updateBooking($options);
            return $this->db->affected_rows();

        }
    else{

        $array= array(
            'note' => $options['note'],
            'student_id' => $options['student_id'],
            'teacher_id' => $options['teacher_id'],
            'date' => $options['date'],
            'booking_id' => $options['booking_id'],
        );
        $this->_set_editable_fields($this->table);
        $this->_validate_options_exist($array);
        $default = array(
            'created' => date('Y-m-d H:i:s')
        );
        $array = $this->_default($default, $array);
        foreach ($this->fields as $field) {
            if (isset($array[$field])) {
                $this->db->set($field, $array[$field]);
            }
        }
        $query = $this->db->insert($this->table);
        $this->updateBooking($array);
        if ($query){
            if ($this->no_primary_key == FALSE){
                return $this->db->insert_id();

            }else{
                return TRUE;
            }
        }

    }
    }

	function updateBooking($options = array()){
        $this->db->where('booking_id',$options['booking_id']);
        if($options['student_id'] > 0){
            $array= array(
                'booking_id'=>$options['booking_id'],
                'student_id'=>$options['student_id'],
            );
        }elseif ($options['teacher_id'] > 0){
            $array= array(
                'booking_id'=>$options['booking_id'],
                'teacher_id'=>$options['teacher_id'],
            );
          
        }else{
            $array= array(
                'booking_id'=>$options['booking_id'],
              );
        }

        // print_r($options);
        // die;
        $this->db->update('student_booking', $array);

        if($options['teacher_id'] > 0)
        {
        	  $array_2= array(
                'id'=> $this->db->where('booking_id',$options['booking_id'])->get('student_booking')->row()->timeslot,
                'teacher_id'=>$options['teacher_id'],
            );
           
            $schdule = new Schdule_model;
            $schdule->updateTeacherinSchedule($array_2);
        }

        return $this->db->affected_rows();
    }
	function _validate_options_exist($options){
		if ($this->validate_field_existence == TRUE){
			foreach ($options as $key => $value){
				$parts = explode(' ', $key);
				$field = $parts[1];
				if ( ! $this->db->field_exists($field, $this->table)){
					show_error('You are trying to insert data into a field that does not exist.  The field "'. $field .'" does not exist in the "'. $this->table .'" table.');
				}
			}
		}
	}
	
	function _set_editable_fields(){
		if (empty($this->fields)){
			$this->db->cache_on();
			$this->fields = $this->db->list_fields($this->table);
			$this->db->cache_off();
		}
	}
	
	function _required($required, $data){
		foreach ($required as $field){
			if ( ! isset($data[$field])){
				return FALSE;
			}
		}
		return TRUE;
	}
	
	function _default($defaults, $options){
		return array_merge($defaults, $options);
	}
	
	function hash_password($password){
		$salt = sha1('@#$DSFT%T%YY%^U&U&@#$C');
		$pass = hash('sha512',$salt.$password);
		return $pass;	
	}
	
	function login_user($username,$password){
		$hash_p = $this->hash_password($password);
		$query  = $this->get(array('email'=>$username,'password'=>$hash_p,'active'=>'1'));
		if($query->num_rows() > 0){
			$row = $query->row();
			$this->session->set_userdata('logged_in',array('user_id' => $row->id,'ip'=>$this->input->ip_address()));
			return true;
		}
		return false;
	}
	
	
}
