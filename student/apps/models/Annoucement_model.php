<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Annoucement_model extends CI_Model {

	public $date;
	public $title;
	public $no_primary_key = false;
	protected $validate_field_existence = FALSE;
	
	protected $fields = array(
								'date',
								'title',
								
							);

	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}	
	
	public function insert($data)
	{
		$this->db->insert('annoucements',$data);
		return true;
	}
  

	
}
